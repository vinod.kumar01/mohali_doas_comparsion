# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 17:29:09 2019
Script to create monthly mean netCDF4 file containing MODIS MAIAC AOD data
@author: Vinod
"""
# %% imports and functions
from pyhdf.SD import SD, SDC
import numpy as np
import os
import glob
from date_converter import doy_2_datetime
from get_MODIS_grid import getgrid
from datetime import date
import netCDF4
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def AOD_loc(hdf_file):
    year = int(hdf_file.split('.')[-5][1:5])
    frac_day = int(hdf_file.split('.')[-5][-3:])
    f = SD(hdf_file, SDC.READ)
    AOD470_obj = f.select('Optical_Depth_047')
    AOD550_obj = f.select('Optical_Depth_055')
    QA_obj = f.select('AOD_QA')
    AOD470 = AOD470_obj.get()
    AOD550 = AOD550_obj.get()
    QA = QA_obj.get()
    for key, value in AOD470_obj.attributes().items():
        if key == 'add_offset':
            add_offset = value
        if key == 'scale_factor':
            scale_factor = value
        if key == '_FillValue':
            fillval = value
#    mask = QA & 24   # only land pixels
#    mask = (QA & 24) == 0  # only water pixel
    mask = QA & 3840 # Best quality data
    AOD470 = np.ma.masked_where(AOD470 == fillval, AOD470)
    AOD470 = np.ma.masked_where(mask, AOD470)
    AOD470 = np.ma.masked_where(QA==0, AOD470)
    AOD470 = (AOD470 - add_offset) * scale_factor
    AOD550 = np.ma.masked_where(AOD550 == fillval, AOD550)
    AOD550 = np.ma.masked_where(QA == 0, AOD550)
    AOD550 = np.ma.masked_where(mask, AOD550)
    AOD550 = (AOD550 - add_offset) * scale_factor
    f.end()
    AOD_470_mean = np.ma.mean(AOD470, 0)
    AOD_550_mean = np.ma.mean(AOD550, 0)
    curr_date = doy_2_datetime(frac_day, year)
    print(np.ma.mean(AOD_470_mean), np.ma.mean(AOD_550_mean))
    return curr_date, AOD_470_mean, AOD_550_mean
 # %% saving monthly output as netCDF
def write_month_out(filename_out):
    f_out = netCDF4.Dataset(os.path.join(dirname, 'month_mean', filename_out),
                            'w', format='NETCDF4')
#    f_out.createDimension('time', None)
    f_out.createDimension('xdim', 1200)
    f_out.createDimension('ydim', 1200)
#    time_out = f_out.createVariable('time', 'f4', ('time'))
    xdim_out = f_out.createVariable('xdim', 'int32', ('xdim'))
    ydim_out = f_out.createVariable('ydim', 'int32', ('ydim'))
    lon_out = f_out.createVariable('lon', 'f4', ('xdim','ydim'))
    lat_out = f_out.createVariable('lat', 'f4', ('xdim','ydim'))
#    aod_470_out = f_out.createVariable('AOD_470',   'f4', ('time', 'xdim', 'ydim'))
#    aod_550_out = f_out.createVariable('AOD_550',   'f4', ('time', 'xdim', 'ydim'))
    aod_470_out = f_out.createVariable('AOD_470',   'f4', ('xdim', 'ydim'))
    aod_550_out = f_out.createVariable('AOD_550',   'f4', ('xdim', 'ydim'))
    
    xdim_out[:] = np.arange(0, 1200)
    ydim_out[:] = np.arange(0, 1200)
    lon_out[:] = lons
    lat_out[:] = lats
    aod_470_out[:] = np.ma.mean(np.ma.masked_array(AOD470), 0)
    aod_550_out[:] = np.ma.mean(np.ma.masked_array(AOD550), 0)
#    time_out[:] = np.array(daynum)
#    time_out.long_name='time';time_out.units='days since 2013-01-01 00:00:00'
    f_out.close()

# %% getting file names and sorting
start_date = date(2013, 1, 1)
end_date = date(2017, 9, 1)

daterange = pd.date_range(start_date, end_date, freq='MS')

dirname = r'M:\nobackup\vinod\MODIS_AOD\raw\l2_1km\Mohali\2013_17'
f_names = glob.glob(os.path.join(dirname, 'MCD19A2*.hdf'))
f_names.sort(key=os.path.basename)
lons, lats = getgrid(f_names[0])
AOD470 = []
AOD550 = []
Date_time = []
daynum = []
count_month = 0
for files in f_names:
    tempdate, tempAOD470, tempAOD550 = AOD_loc(files)[:]
    count_day = (tempdate.date()-start_date).days
    if tempdate.date() >= daterange[count_month+1].date():
        print('Saving merge data for '+ daterange[count_month].strftime('%Y_%m'))
        filename_out = 'MODIS_AOD_'+daterange[count_month].strftime('%Y_%m')+'_cf.nc'
        write_month_out(filename_out)
        AOD470 = []
        AOD550 = []
        Date_time = []
        daynum = []
        count_month += 1
    Date_time.append(tempdate)
    daynum.append(count_day)
    AOD470.append(tempAOD470)
    AOD550.append(tempAOD550)
    print(tempdate)
