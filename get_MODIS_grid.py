# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 17:36:13 2019

@author: Vinod
"""

from pyhdf.SD import SD, SDC
import numpy as np
import re
import pyproj


def getgrid(filename):

    hdf = SD(filename, SDC.READ)
    DATAFIELD_NAME = 'Optical_Depth_047'
    # Read dataset.
    data3D = hdf.select(DATAFIELD_NAME)
    data = data3D[1, :, :].astype(np.double)

    # Construct the grid.  The needed information is in a global attribute
    # called 'StructMetadata.0'.  Use regular expressions to tease out the
    # extents of the grid.
    fattrs = hdf.attributes(full=1)
    ga = fattrs["StructMetadata.0"]
    gridmeta = ga[0]
    ul_regex = re.compile(r'''UpperLeftPointMtrs=\(
                              (?P<upper_left_x>[+-]?\d+\.\d+)
                              ,
                              (?P<upper_left_y>[+-]?\d+\.\d+)
                              \)''', re.VERBOSE)
    match = ul_regex.search(gridmeta)
    x0 = np.float(match.group('upper_left_x'))
    y0 = np.float(match.group('upper_left_y'))

    lr_regex = re.compile(r'''LowerRightMtrs=\(
                              (?P<lower_right_x>[+-]?\d+\.\d+)
                              ,
                              (?P<lower_right_y>[+-]?\d+\.\d+)
                              \)''', re.VERBOSE)
    match = lr_regex.search(gridmeta)
    x1 = np.float(match.group('lower_right_x'))
    y1 = np.float(match.group('lower_right_y'))

    nx, ny = data.shape
    x = np.linspace(x0, x1, nx)
    y = np.linspace(y0, y1, ny)
    xv, yv = np.meshgrid(x, y)

    sinu = pyproj.Proj("+proj=sinu +R=6371007.181 +nadgrids=@null +wktext")
    wgs84 = pyproj.Proj("+init=EPSG:4326")
    lon, lat = pyproj.transform(sinu, wgs84, xv, yv)

    # There is a wrap-around issue to deal with, as some of the grid extends
    # eastward over the international dateline.  Adjust the longitude to avoid
    # a smearing effect.
    lon[lon < 0] += 360
    return(lon, lat)
    