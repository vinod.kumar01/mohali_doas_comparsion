# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 00:30:43 2019

@author: Vinod
"""

import os
import netCDF4
import cartopy as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
import numpy as np
from mod_colormap import man_cm
new_cm=man_cm("cmap_diff", 5)
dirname = r'M:\home\beirle\ProjectsA\2018_Divergence\data\Mean_Fluxes\Europe\hires'
filename = 'means_layer12_allmonths.hdf'
#filename = 'means_layer8_AprToOct.hdf'
f = netCDF4.Dataset(os.path.join(dirname, filename))
lons = f.variables['lon'][:]
lats = f.variables['lat'][:]
lats = np.flip(lats, 0)
VCD = f.variables['VCD'][:]
emis = f.variables['E'][:]
new_cm=man_cm("cmap_diff", 6)
fig,ax = plt.subplots(subplot_kw=dict(projection=ccrs.crs.PlateCarree()))
#ax.set_extent([75.5, 77.5, 30, 32])
ax.set_extent([0, 19, 43, 56])
gl = ax.gridlines(crs=ccrs.crs.PlateCarree(), draw_labels=True,
                    linewidth=1, color='gray', alpha=0.5, linestyle='--')
gl.xlabels_top = False
gl.ylabels_right = False
ax.coastlines(color='black', linewidth=1, resolution='50m')
ax.add_feature(cfeature.BORDERS.with_scale('50m'),
                 linestyle='-', alpha=.5)


llons,llats = np.meshgrid(lons, lats)
#cs = ax.pcolormesh(llons, llats, VCD*1E-15, cmap=new_cm,
#                    vmin = -2, vmax = 4,
##                   vmin=-8E-10, vmax=2E-9,
#                    transform=ccrs.crs.PlateCarree())

cs = ax.pcolormesh(llons, llats, emis*1E3*1E6, cmap=new_cm,
                    vmin = -1.2, vmax = 3,
#                   vmin=-8E-10, vmax=2E-9,
                    transform=ccrs.crs.PlateCarree())
#        ax.add_patch(Rectangle((76.4,30.4), 0.6, 0.6, fill=False, color = 'k'))
#        ax.add_patch(Rectangle((76.63,30.57), 0.2, 0.2, fill=False, color = 'k'))

#loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'},
#       {'lon': 75.86, 'lat': 30.90, 'site': 'Ludhiana'},
#       {'lon': 76.93, 'lat': 30.83, 'site': 'Kalka'},
#       {'lon': 76.79, 'lat': 30.96, 'site': 'Baddi'},
#       {'lon': 76.37, 'lat': 31.38, 'site': 'Nangal'},
#       {'lon': 77.173, 'lat': 31.105, 'site': 'Shimla'},
#       {'lon': 76.687, 'lat': 31.34, 'site': 'Bilaspur'}]
#for point in loc:
#    lo, la = point['lon'], point['lat']
#    if point['site']=='Mohali':
#        ax.plot(lo, la, 'ko')
#    elif point['site']=='Ludhiana':
#        ax.plot(lo, la, 'k*')
#    else:
#        ax.plot(lo, la, 'kx')
#    ax.annotate(point['site'], xy=(lo, la),  xycoords='data',
#                 xytext=(lo, la), textcoords='data', color='k',
#                 horizontalalignment='right', verticalalignment='bottom',
#                 arrowprops=dict(facecolor='black', shrink=0.05))

cbar = fig.colorbar(cs, ax=ax, orientation='vertical', extend='max',
             fraction=0.05, shrink=0.8, pad=0.05)

          
#fig.subplots_adjust(right=0.88, wspace = 0.02)
#cbar_ax=fig.add_axes([0.8, 0.15, 0.02, 0.7])
#cbar=fig.colorbar(cs, cax = cbar_ax, extend='max')
#cbar.set_label('NO$_{2}$ VCD (* 10$^{15}$ molecules cm $^{-2}$)', fontsize=12)
cbar.set_label('NOx'+ ' emission flux (${\mu}g$ $m^{-2}$ $s^{-1}$)', fontsize=11)