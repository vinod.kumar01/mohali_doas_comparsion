# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 17:04:24 2020

@author: Vinod
"""

import os
import shutil
from datetime import datetime, timedelta
import handleMFC


def corr_dc_off(spectra, dc, offset):
    Offset_data = offset['spectrum']/offset['iNoScans']
    DC_data = ((dc['spectrum']-Offset_data)/dc['fExposureTime'])
    spec_data = spectra['spectrum']
    spec_data = spec_data - (spectra['fExposureTime'] * DC_data)
    spec_data = spec_data - (spectra['iNoScans'] * Offset_data)
    corr_spectra = spectra.copy()
    corr_spectra['spectrum'] = spec_data
    return(corr_spectra)


src = r'D:\wat\minimax\MOhali_2013\spechori'
dest = r'D:\wat\minimax\MOhali_2013\horizon_scan'
UTC_diff = 330

corr_dc_offset = False
osdc_path = r'D:\wat\minimax\MOhali_2013\horizon_scan'
dc_file = 'osdc001070_dc'
offset_file = 'osdc001069_offset'
dc = handleMFC.readMFC(os.path.join(osdc_path, dc_file))
offset = handleMFC.readMFC(os.path.join(osdc_path, offset_file))

for path, subdirs, files in os.walk(src):
    for name in files:
        filename = os.path.join(path, name)
        spectra = handleMFC.readMFC(filename)
        creation_datetime = spectra['szDate'] + spectra['szStartTime']
        creation_datetime = datetime.strptime(creation_datetime,
                                              '%d.%m.%y%H:%M:%S')
        creation_datetime += timedelta(minutes=UTC_diff)  # account for UTC+5:30
        dirname = creation_datetime.strftime(format='%Y_%m_%d')

        try:
            os.makedirs(os.path.join(dest, dirname))
            if corr_dc_offset:
                print("Sorting and correcting files for "+ dirname )
            else:
                print("Sorting files for "+ dirname )
        except FileExistsError:
            pass
        # case when stop time exceeds 24 hours
        if (spectra['szStopTime'][:5]=="00:00") & (spectra['szStartTime'][:5]=="23:59"):
            spectra['szStopTime']="23:59:59"
            
            if corr_dc_offset:
                corr_spectra = corr_dc_off(spectra, dc, offset)
                handleMFC.saveMFC(os.path.join(dest, dirname,
                                               filename.split('\\')[-1]),
                                  corr_spectra)
            else:
                handleMFC.saveMFC(os.path.join(dest, dirname,
                                  filename.split('\\')[-1]), spectra)
        # case normal        
        else:
            if corr_dc_offset:
                corr_spectra = corr_dc_off(spectra, dc, offset)
                handleMFC.saveMFC(os.path.join(dest, dirname,
                                               filename.split('\\')[-1]),
                                  corr_spectra)
            else:
                shutil.copy2(filename, os.path.join(dest, dirname))
