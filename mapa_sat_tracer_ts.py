# -*- coding: utf-8 -*-
'''
Created on 01-Jul-2019

@author: Vinod
'''

import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import import_merge_mapa_out as mapa
from scipy import stats
from decimal import Decimal
from lin_ODR import orthoregress as odr
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# cloud classification is performed separately. Need to bring it on 
# MAPA retrieveal timeline
def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])

# %% load DOAS mapa data
#NO2
#Inp = mapa.load_plot('no2', 'no2', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 2e16, 'OMI', 'OMNO2', 'vcd_weighted_mean', 1e15, sf='0.8')
## #NO2_VIS
#Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
#                    -0.25e16, 2e16, 'OMI', 'OMNO2', 'vcd_weighted_mean', 1e15, sf='0.8')
# #HCHO 
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 6e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 6e16, 'OMI', 'OMHCHO', 'vcd_weighted_mean', sf='0.8')
# O4/AOD
#Inp = mapa.load_plot('aod', 'o4', 'AOD (360 nm)', 0, 2.5, 'OMI', 'OMAERUV', 'c_weighted_mean', sf='0.8')
Inp = mapa.load_plot('aod', 'o4', 'AOD (360 nm)', 0, 2.5, 'MODIS', 'MAIAC', 'c_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4_alt', 'AOD', 0, 2.5, 'MODIS', 'MYD/MOD', 'c_weighted_mean', sf='0.8')
concur_day = True    # set Trueto ignore DOAS measurement on days when satellite data is absent
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
#time2 = np.array(time2)
time2 = pd.to_datetime(time2)
# vcd_mean = np.ma.masked_where(flag == 2, vcd_mean)
# has issues while converting into dataframe
vcd_mean[flag == 2] = np.nan

# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
vcd_mean[OT_cloud==1] = np.nan
vcd_mean[fog==1] = np.nan
#vcd_mean[cloud_type>=3] = np.nan
# %% load satellite data
dirname_sat = r'D:\postdoc\Sat'
if Inp.inst in ['TERRA', 'AQUA']:
    sat='MODIS'
else:
    sat = Inp.inst
# filename of extracted data around measurement location
#f_name = Inp.tracer+'_'+Inp.inst+'_QA4ECV_50_cf_all.csv'  #QA4ECV NO2
#f_name = Inp.tracer+'_'+Inp.inst+'_DOM_TS50_coloc.csv' #QA4ECV colocated
#f_name = Inp.tracer+'_'+Inp.inst+'_NASA_50.csv'  #OMNO2 NO2
#f_name = Inp.tracer+'_'+Inp.inst+'_OMNO2.csv'  #OMNO2 NO2 with correct cf
#f_name = Inp.tracer+'_'+Inp.inst+'_DOM_TS50_sf08glue_corr.csv'  #DOMINO NO2
#f_name = Inp.tracer+'_'+Inp.inst+'_DOM_TS50_coloc.csv'  #DOMINO Colocated NO2
f_name = Inp.tracer+'_'+Inp.inst+'_360_TS_dm_5km_Mohali_cf.csv'   #MAIAC 2km
#f_name = Inp.tracer+'_'+Inp.inst+'_ts.csv'   #OMI AERUV
#f_name = Inp.tracer+'_'+Inp.inst+'_ts_l2g10km.csv'
#f_name = Inp.tracer+'_'+Inp.inst+'_QA4ECV_l2.csv'  # hcho QA4ECV 25km 
#f_name = Inp.tracer+'_'+Inp.inst+'_QA4ECV_coloc.csv'  # hcho QA4ECV colocated
#f_name = Inp.tracer+'_'+Inp.inst+'_NASA_l2_new.csv'
#f_name = Inp.tracer+'_'+Inp.inst+'_BIRA.csv'

#f_name = 'test1.csv'
df_sat = pd.read_csv(os.path.join(dirname_sat, sat, 'TS_mohali', f_name),
                     delimiter=',', header=0, escapechar='#')
# remove excess space from headers
df_sat.columns = df_sat.columns.str.strip()
# get the data formats right
df_sat['date_time'] = pd.to_datetime(df_sat['date_time'])
for cols in df_sat.columns[1:]:
    df_sat[cols] = pd.to_numeric(df_sat[cols], errors='coerce')
# Resample at different frequencies : Daily/Monthly
df_sat = df_sat.set_index('date_time')
df_sat['vcd_cf'] = df_sat['vcd_cf']*Inp.wf
#df_sat['vcd_cf'] = df_sat['vcd_cf_40'][df_sat['vcd_cf_30'].isnull()]*Inp.wf
## for choosing data for a interval of clod filter
df_sat = df_sat.resample('D').mean()
print(df_sat['vcd_cf'].count())

# %% filter DOAS data around satellite overpass 
#dates = [dt.astype(datetime).date() for dt in time2]
#unique_dates = list(set(dates))
if Inp.inst == 'MODIS':
    idx_sat = ((time2.hour>=7) & (time2.hour<9)) | ((time2.hour>=4) & (time2.hour<6))
elif Inp.inst == 'TERRA':
    idx_sat = ((time2.hour>=4) & (time2.hour<6))
else:
    idx_sat = ((time2.hour>=7) & (time2.hour<9))
# filter around satellite overpass
time2 = time2[idx_sat]
vcd_mean = vcd_mean[idx_sat]
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64')]).T
df.columns = ['Date_time', Inp.tracer+'_VCD']
df['Date_time'] = pd.to_datetime(df['Date_time'])
df[Inp.tracer+'_VCD'] = pd.to_numeric(df[Inp.tracer+'_VCD'], errors='coerce')
# Add satellite VCD to the common dataframe
sat_vcd_dict = {}
for days in df_sat.index:
    sat_vcd_dict[days.strftime('%d-%m-%Y')] = df_sat.loc[days.strftime('%Y-%m-%d')]['vcd_cf']
## Create satellite VCD of length same as DOAS measurements. 
##eaning repeating the same value for all overlap period
vcd_sat = np.zeros(len(time2)) 
for idx in range(len(time2)):
    try:
        vcd_sat[idx] = sat_vcd_dict[time2[idx].strftime('%d-%m-%Y')]
        # 200m is the width of box and 1m = 100cm
    except KeyError:
        vcd_sat[idx] = np.nan
        
df['vcd_cf'] = vcd_sat


# %% Filter out doas data on days when satellite measurements are also absent
if concur_day:
    mask = df[Inp.tracer+'_VCD'].isnull() | df['vcd_cf'].isnull()
    df = df[~mask]

# %% Daily and mOnthly means
df = df.set_index('Date_time')
df_day = df.resample('D').mean()
df_mnth = df.resample('MS').mean()
df_sem = df.resample('MS').sem()
df_std = df.resample('MS').std()
df.reset_index(inplace=True)

# %% plotting time series
def plot_ts(ax):
    ax.scatter(df_day.index, df_day[Inp.tracer+'_VCD'], s=2,
               c='r', label='MAX-DOAS (daily)', alpha=0.15)
    markers, caps, bars = ax.errorbar(df_mnth.index,
                                      df_mnth[Inp.tracer + '_VCD'],
                                      df_sem[Inp.tracer+'_VCD'],
                                      c='r', label='MAX-DOAS (monthly)',
                                      fmt='-x', capsize=4)
    ax.scatter(df_sat.index, df_sat['vcd_cf'], s=2, c='b',
               label=Inp.inst+' '+Inp.prod+' (daily)', alpha=0.15)
    [bar.set_alpha(0.5) for bar in bars]
    markers, caps, bars = ax.errorbar(df_mnth.index, df_mnth['vcd_cf'],
                                      df_sem['vcd_cf'], c='b',
                                      label=Inp.inst+' '+Inp.prod+' (monthly)',
                                      fmt='-x', capsize=4)
    [bar.set_alpha(0.5) for bar in bars]
    ax.legend(loc=[0.5, 0.78], ncol=2)
    ax.set_ylabel(Inp.spec_str)
    ax.set_xlim(pd.to_datetime('11-15-2012'), pd.to_datetime('06-15-2017'))
    ax.set_ylim(Inp.y_min, Inp.y_max)
    ax.minorticks_on()
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
    ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    ax.grid(axis='y', alpha=0.4)
fig, ax = plt.subplots(figsize=[11, 4])
plot_ts(ax)
fig.tight_layout()

#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf, 'Overlap',
#                         Inp.mapa_spec+'_'+Inp.inst+'_'+Inp.prod+'_TS_l2cf_v5_coloc.png'),
#                         format='png', dpi=300)
#plt.show()

# %% scatter plot
# Scatter plot

def plot_scatter(ax2):
    mask_day = df_day[Inp.tracer+'_VCD'].isnull() | df_day['vcd_cf'].isnull()
    ax2.scatter(df_day[Inp.tracer+'_VCD'][~mask_day],
                df_day['vcd_cf'][~mask_day], s=2, c='b', alpha=0.5, label='Daily')
    ax2.set_ylabel(Inp.inst + ' (' +Inp.prod+ ') ' + Inp.spec_str[:-22])
    ax2.set_xlabel('DOAS (MAPA) ' + Inp.spec_str)
    ax2.set_xlim(Inp.y_min, Inp.y_max)
    ax2.set_ylim(Inp.y_min, Inp.y_max)
    # # add linear regression line
    fit_param = stats.linregress(df_day[Inp.tracer+'_VCD'][~mask_day],
                df_day['vcd_cf'][~mask_day])[0:3]
    fit_func = np.poly1d(fit_param[0:2])
    ax2.plot(df_day[Inp.tracer+'_VCD'][~mask_day],
             fit_func(df_day[Inp.tracer+'_VCD'][~mask_day]), color="b",
             ls="--", linewidth=1)    
    ax2.plot(ax2.get_xlim(), ax2.get_ylim(), ls=":", c=".3")
    add_fit_param(fit_param, pos_x=0.02, pos_y=0.85, ax=ax2,
                  color='b', showr=True)   
    # add monthly values
    mask_mnth = df_mnth[Inp.tracer+'_VCD'].isnull() | df_mnth['vcd_cf'].isnull()
    ax2.scatter(df_mnth[Inp.tracer+'_VCD'][~mask_mnth], df_mnth['vcd_cf'][~mask_mnth],
                s=20, marker='s', facecolors='none', edgecolors='k', label='Monthly')
    fit_param2 = stats.linregress(df_mnth[Inp.tracer+'_VCD'][~mask_mnth],
                                  df_mnth['vcd_cf'][~mask_mnth])[0:3]
    fit_func2 = np.poly1d(fit_param2[0:2])
    ax2.plot(df_mnth[Inp.tracer+'_VCD'][~mask_mnth],
             fit_func2(df_mnth[Inp.tracer+'_VCD'][~mask_mnth]),
             color="k", linewidth=2)
    add_fit_param(fit_param2, pos_x=0.02, pos_y=0.7, ax=ax2,
                  color='k', showr=True)
    mask_mnth |= (df_std[Inp.tracer+'_VCD']== 0) | (df_std['vcd_cf']==0)
    mask_mnth |= df_std[Inp.tracer+'_VCD'].isnull() | df_std['vcd_cf'].isnull()
    fit_param_odr = odr(df_mnth[Inp.tracer+'_VCD'][~mask_mnth],
                        df_mnth['vcd_cf'][~mask_mnth],
                        df_sem[Inp.tracer+'_VCD'][~mask_mnth],
                        df_sem['vcd_cf'][~mask_mnth])[0:3]
    print(fit_param_odr)
    fit_func_odr = np.poly1d(fit_param_odr[0:2])  
    ax2.plot(df_mnth[Inp.tracer+'_VCD'][~mask_mnth],
             fit_func_odr(df_mnth[Inp.tracer+'_VCD'][~mask_mnth]),
             color="r", linewidth=2, label='ODR fit')
    add_fit_param(fit_param_odr, pos_x=0.02, pos_y=0.6, ax=ax2,
                  color='r', showr=False)
    ax2.legend(handletextpad=0.2, loc=[0.63,0.62])
    ax2.minorticks_on()
def add_fit_param(fit_param, pos_x, pos_y, ax='ax', color='k', showr=True):
    corr_coeff = "\n r = " + str('%.2f' % fit_param[2]) if showr else ''
    if fit_param[1] >= 0:
       ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x + " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color = color)
    else:
      ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x - " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color = color)
    
fig2, ax2 = plt.subplots()
plot_scatter(ax2)
plt.tight_layout(w_pad=0.5)
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf, 'Overlap',
#                         Inp.mapa_spec+Inp.inst+'_'+Inp.prod+'_scatter_l2v5.png'),
#                         format='png', dpi=300)
##plt.show()

#ax1.annotate("A", xy=(0.9, 0.9), xycoords="axes fraction")
#ax2.annotate("B", xy=(0.9, 0.9), xycoords="axes fraction")