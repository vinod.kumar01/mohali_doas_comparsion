# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 22:17:45 2019

@author: Vinod
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import import_merge_mapa_out as mapa
import calendar
import matplotlib.patches as mpatches

# cloud classification is performed separately. Need to bring it on 
# MAPA retrieveal timeline
def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])

# %% load DOAS mapa data
# #NO2_VIS


# O4/AOD
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v3'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
'''
VCD
'''
#Inp = mapa.load_plot('aod', 'o4', 'AOD', 0, 2.5, 'MODIS', 'MAIAC', 'c_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 2e16, 'OMI', 'OMNO2', 'vcd_weighted_mean', 1e15, sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 6e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
'''
surface concentration
'''
#Inp = mapa.load_plot('aod', 'o4', 'Aerosol extinction (1/Km)', 0, 2.5,'OMI',
#                     'NASA', 'c_weighted_mean', sf='0.8')
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ concentration (molecules cm$^{-3}$)',
                     5e9, 7e11, 'Chemiluminescence', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO concentration (molecules cm$^{-3}$)',
#                     5e9, 7e11, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
profile_mean = np.array(profile_mean)
conc_surface = profile_mean[:, 0]
vcd_mean[flag == 2] = np.nan
conc_surface[flag == 2] = np.nan

#vcd_mean, z, flag, profile_mean, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
#vcd_mean_hcho, flag = np.array(vcd_mean), np.array(flag)
time2 = pd.to_datetime(time2)
#vcd_mean_hcho[flag == 2] = np.nan

print('loaded MAPA data')
# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
flag_remove = (OT_cloud==1) | (fog==1)
vcd_mean[flag_remove] = np.nan
conc_surface[flag_remove] = np.nan
#vcd_mean_no2[flag_remove] = np.nan
#vcd_mean_hcho[flag_remove] = np.nan
#%% create merge data and subgroup
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64'), conc_surface.astype('float64')]).T
df.columns = ['Date_time', Inp.tracer+'vcd', Inp.tracer+'conc_surface']

#df = pd.DataFrame(data=[time2, vcd_mean_aod.astype('float64'),
#                        vcd_mean_no2.astype('float64'), vcd_mean_hcho.astype('float64')]).T
#df.columns = ['Date_time', 'aod', 'no2', 'hcho']
df['Date_time'] = pd.to_datetime(df['Date_time'])
for colnames in df.columns[1:]:
    df[colnames] = pd.to_numeric(df[colnames], errors='coerce')
df['weekday'] = df['Date_time'].dt.weekday
df['month'] = df['Date_time'].dt.month


# %% plotting
## everything together
#def color_bp(bp, c):
#    [item.set_color(c) for item in bp[0][1]['boxes']]
#    [item.set_color(c) for item in bp[0][1]['fliers']]
#    [item.set_color(c) for item in bp[0][1]['medians']]
#    [item.set_markerfacecolor(c) for item in bp[0][1]['means']]
#    [item.set_color(c) for item in bp[0][1]['whiskers']]
#    [item.set_color(c) for item in bp[0][1]['caps']]
#fig,ax = plt.subplots()
#    
#boxplot_no2 = df.boxplot(column='no2', by='weekday',ax=ax,
#                     showfliers=False, return_type = 'both')
#color_bp(boxplot_no2, 'r')
#
#boxplot_hcho = df.boxplot(column='hcho', by='weekday',ax=boxplot_no2[0][0],
#                     showfliers=False, return_type = 'both')
#color_bp(boxplot_hcho, 'g')
#ax2 = ax.twinx()
#boxplot_aod = df.boxplot(column='aod', by='weekday',ax=ax2,
#                     showfliers=False, return_type = 'both')
#k_patch = mpatches.Patch(color='k', label='AOD')
#red_patch = mpatches.Patch(color='r', label='NO$_2$')
#g_patch = mpatches.Patch(color='g', label='HCHO')
# separate
#weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
#fig,ax = plt.subplots(3,4, figsize=[22,13], sharex=True, sharey=True)
#for i in range(0,3):
#    for j in range(0,4):
#        month = (4*i)+j+1    
#        boxplot_no2 = df[df['month']==month].boxplot(column=Inp.tracer,
#                        by='weekday',ax=ax[i,j], showfliers=False, whis=[5, 95],
#                        showmeans=True, return_type = 'dict', grid=False)
#        fig.suptitle(' ')
#        ax[i,j].set_title('')
#        ax[i,j].grid(alpha=0.4)
#        ax[i,j].set_xticklabels(weekdays)
#        ax[i,j].set_xlabel(' ')
#        if j==0:
#            ax[i,j].set_ylabel(Inp.spec_str, size=12)
#        ax[i,j].text(0.8, 0.9, calendar.month_abbr[month],
#                 transform=ax[i][j].transAxes)
#        plt.tight_layout()
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 
#                         Inp.tracer+'_weekdayv5.png'), format='png', dpi=300)    
# separate
#weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
# %% all months together
def box_plot_week(Inp, ax, plotable="vcd"):
    '''
    plotable = "vcd" / "conc_surface"
    '''
    boxplot_tracer = df.boxplot(column=Inp.tracer+plotable, by='weekday',ax=ax,
                             showfliers=False, whis=[5, 95], showmeans=True,
                             return_type = 'dict', grid=False)
    weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    ax.set_title('')
    ax.grid(alpha=0.4)
    ax.set_xticklabels(weekdays)
    ax.set_xlabel(' ')
    ax.set_ylabel(Inp.spec_str, size=12)
    return boxplot_tracer
#
#fig,ax = plt.subplots()
#boxplot_tracer = box_plot_week(Inp, ax)
#fig.suptitle(' ')
#plt.tight_layout()
##plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 
##                         Inp.tracer+'_weekdayv5.png'), format='png', dpi=300)       

