# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 17:15:22 2019

@author: Vinod
"""

import pandas as pd
import os


def interpolate(ts, datetime_index):
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    return x.groupby(x.index).first().sort_index().fillna(
        method="ffill", limit=5)[datetime_index]
    
    
insitu_path = r'D:\postdoc\DOAS\Mohali_2013_17'
insitu_file = os.path.join(insitu_path, 'in_situ.csv')
colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
            'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
            'abs_humidity', 'hcho']
insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=colnames)
insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                         format='%d-%m-%Y %H:%M:%S')
for cols in colnames[1:]:
    insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')

rain_file = os.path.join(insitu_path, 'rain.csv')

rain_data = pd.read_csv(rain_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=['met_time', 'rain', 'rain_corr'])
rain_data['met_time'] = pd.to_datetime(rain_data['met_time'],
                                         format='%d-%m-%Y %H:%M:%S')
rain_data['rain'] = pd.to_numeric(rain_data['rain'], errors='coerce')
rain_data['rain_corr'] = pd.to_numeric(rain_data['rain_corr'], errors='coerce')
rain_data = rain_data.set_index('met_time')
insitu_data = insitu_data.set_index('desttime')

rain_corr_intp = interpolate(rain_data['rain_corr'], insitu_data.index)
insitu_data['rain'] = rain_corr_intp
insitu_data.reset_index(inplace=True)
insitu_data.to_csv(r'D:\postdoc\DOAS\Mohali_2013_17\insitu_all.csv', sep=',', index=False)