# -*- coding: utf-8 -*-
"""
Created on Mon May  4 13:31:50 2020

@author: Vinod
"""

import netCDF4
import numpy as np
from scipy.interpolate import interp2d
import os
from datetime import datetime, timedelta
import pandas as pd
from tools import plot_diurnal
import matplotlib.pyplot as plt

def era2dict(era_file, params):
    data_out = {}
    with netCDF4.Dataset(era_file) as f:
        lat = f.variables['latitude']
        lon = f.variables['longitude']
        time = f.variables['time']
        data_out['lat'] = {'unit': lat.units,
                           'data': lat[:]}
        data_out['lon'] = {'unit': lon.units,
                           'data': lon[:]}
        data_out['time'] = {'unit': time.units,
                            'data': time[:]}
        for key, param in params.items():
            if param not in f.variables.keys():
                continue
            param_var = f.variables[param]
# =============================================================================
# By default, scale factors and offset are already applied in netCDF4.variables
# Similary missing values and fill values are masked by default.
# Howevevr, masked values arenot considered later for interpolation
# and might result unrealistic values. Hence, I'll convert them to nan
# http://unidata.github.io/netcdf4-python/netCDF4/index.html
# if not applied, uncomment 8 limes below
# =============================================================================
            fv = param_var._FillValue
            mv = param_var.missing_value
#            sf = param_var.scale_factor
#            offset = param_var.add_offset
#            param_data *= sf
#            param_data += offset
            unit = param_var.units
            param_data = np.array(param_var[:])
            param_data[param_data == fv] = np.nan
            param_data[param_data == mv] = np.nan
            data_out[key] = {'unit': unit,
                             'data': param_data}
        return data_out


def grid2point_ts(era_file, params, loc):
    griddata = era2dict(era_file, params)
    ts = {}
    ts['time'] = griddata['time']
    ts_len = len(ts['time']['data'])
    for point in loc:
        ts[point['site']] = {}
        for param in params.keys():
            if param not in griddata.keys():
                continue
            ts[point['site']][param] = {'data': np.full(ts_len, np.nan),
                                        'unit': griddata[param]['unit']}
            ts[point['site']][param]['data'][:] = np.nan

    for param in params.keys():
        if param not in griddata.keys():
            continue
        for i in range(ts_len):
            fx = interp2d(griddata['lat']['data'], griddata['lon']['data'],
                          griddata[param]['data'][i, :].T)

            for point in loc:
                data_now = fx(point['lat'], point['lon'])
                ts[point['site']][param]['data'][i] = data_now
    return ts

# %% load data
dirname = r'D:\postdoc\DOAS\Mohali_2013_17'
filename = 'ERA5_blh_temp_hourly_2013_17.nc'
era_file = os.path.join(dirname, filename)
params = {'pblh': 'blh', 't2m': 't2m'}
loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'}]

ts = grid2point_ts(era_file, params, loc)
date_display = datetime.strptime(ts['time']['unit'][12:31],
                                 '%Y-%m-%d %H:%M:%S')
date_time = [date_display+timedelta(hours=int(dt))
             for dt in ts['time']['data']]
ts_blh = pd.DataFrame(data={'Date_time': np.array(date_time),
                            'blh': ts['Mohali']['pblh']['data'],
                            't2m': ts['Mohali']['t2m']['data']})
ts_blh = ts_blh.set_index('Date_time')
ts_blh = ts_blh.set_index(ts_blh.index+timedelta(minutes=330))

# %% blh climatology
#df2 = 1e-3*ts_blh.resample('3H').mean()
#df2['month'] = df2.index.month
#df2['hour'] = df2.index.hour
#df3 = df2.groupby(['month', 'hour']).mean()
#df3['std'] = df2.groupby(['month', 'hour']).std()['blh']
#df3.reset_index(inplace=True)
#df3.to_csv(r'M:\nobackup\vinod\Sdonner\AMF_LUT_v1.0\BLH_climatology_Mohali.dat',
#           index=False, sep='\t', float_format='%.3E')
# %% further calculations
# monthly profiles
#month = ts_blh.index.month
#pblh_day = []
#percentile_25_diff = []
#percentile_75_diff = []
#for i in range(1, 13):
#    idx = (month == i)
#    pblh_mnth = ts_blh[idx]['blh'].mean()
#    pblh_day.append(pblh_mnth)
#    percentile_25 = ts_blh[idx]['blh'].quantile(0.25)
#    percentile_25_diff.append(pblh_mnth - percentile_25)
#    percentile_75 = ts_blh[idx]['blh'].quantile(0.75)
#    percentile_75_diff.append(percentile_75 - pblh_mnth)
# %% diurnal profiles
fig, ax = plt.subplots(figsize=[6, 4.5])
month = ts_blh.index.month
seasons = {'Winter': [11, 12, 1, 2], 'Summer': [3, 4, 5, 6],
           'Monsoon': [7, 8], 'Post-Monsoon': [9, 10]}
color = ['slategrey', 'red', 'blue', 'green']
for c, season in zip(color, seasons):
    df_sel = ts_blh[ts_blh.index.month.isin(seasons[season])]
    a = plot_diurnal(1e-3*df_sel['blh'], df_sel.index, ax=ax,
                     label=season, color=c)
ax.set_ylabel('Boundary layer height (Km)')
ax.set_xlabel('Local time (UTC+5:30)')
ax.grid(alpha=0.4)
ax.legend(loc='upper left')
ax.set_xlim(a[1].index[6], a[1].index[20])
ax.annotate('A)', xy=(0.9, 0.9), xycoords="axes fraction")
plt.tight_layout()
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_0.8',
#                         'blh_seasonality.png'), format='png', dpi=300)

