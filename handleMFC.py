'''
Created on 15.03.2017

@author: donner
'''
import numpy as np
import struct


def convert_to_byte(convertstr, maxlen):
    '''
    Convert a string to a bytecode with fixed length.
    '''
    return (convertstr + (maxlen - len(convertstr)) * '\x00').encode()


def readMFC(fileName):
    '''
    This function reads .mfc files generated with the DOASIS software.
    @fileName is the path and file name of the .mfc data file
    @mfcFile is a dictionary containing all the information of the file
    RETURNS file content as dictionary (mfcFile)
    Some important translations:
    Character does not need translation: 1 character = 1 byte
    Integer needs struct.unpack('i', ...): 1 int32 = 1 byte
    Double needs struct.unpack('d', ...): 1 double = 8 byte
    By Steffen Doerner and Sebastian Donner based on a matlab script by Julia
    Remmers. The documentation of the .mfc file header is from Stefan
    Kraus, 2006.
    Latest Version: 1.0, 15th November 2016 (first implementation)
    Latest Version: 1.1, 16th March 2017 (Pep8, Python3)
    '''
    # create empty dictionary for all the information
    mfcFile = {}
    # open the mfc file for reading it
    fid = open(fileName, 'rb')
    # read the different information of the file header
    mfcFile['szVersion'] = fid.read(20).decode().replace('\x00', '')
    mfcFile['iNChannel'] = struct.unpack('i', fid.read(4))[0] + 1
#    mfcFile['pData'] = fid.read(4).decode().replace('\x00', '')
    dump = fid.read(4)
    mfcFile['pData'] = ' '
    mfcFile['szSpecName'] = fid.read(20).decode().replace('\x00', '')
    mfcFile['szSite'] = fid.read(20).decode().replace('\x00', '')
    mfcFile['szSpectrometer'] = fid.read(20).decode().replace('\x00', '')
    mfcFile['szDevice'] = fid.read(20).decode().replace('\x00', '')
    mfcFile['szFirstLine'] = fid.read(80).decode().replace('\x00', '')
    mfcFile['szLater'] = fid.read(18).decode().replace('\x00', '')
    mfcFile['iSpecInBlock'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iNumSpecOfBlock'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['fDispersionHighDensity'] = struct.unpack('ddd', fid.read(24))
    mfcFile['fWavelength'] = struct.unpack('d', fid.read(8))[0]
    mfcFile['fAverage'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['iODLeft'] = struct.unpack('h', fid.read(2))[0]
    mfcFile['iODCenter'] = struct.unpack('h', fid.read(2))[0]
    mfcFile['iODRight'] = struct.unpack('h', fid.read(2))[0]
    mfcFile['fOptDens'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fOptWavelength'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['iType'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['szDate'] = fid.read(9).decode().replace('\x00', '')
    mfcFile['szStartTime'] = fid.read(9).decode().replace('\x00', '')
    mfcFile['szStopTime'] = fid.read(9).decode().replace('\x00', '')
    mfcFile['cFillDummy1'] = fid.read(1).decode().replace('\x00', '')
    mfcFile['iMathLow'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iMathHigh'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iMinChannel'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iMaxChannel'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iMarker'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iNoScans'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['fExposureTime'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fLatitude'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fLongitude'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['iNoPeaks'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iNoBands'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['fMin'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fMax'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fYScale'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fOffsetScale'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fWavelength1'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fAverageOld'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['fDispersion'] = [struct.unpack('f', fid.read(4))[0]
                              for index in range(3)]
    mfcFile['fOptDen'] = struct.unpack('f', fid.read(4))[0]
    mfcFile['iMode'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iSmooth'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['iDegreeRegression'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['cNull'] = fid.read(8).decode().replace('\x00', '')
    mfcFile['cRef'] = fid.read(8).decode().replace('\x00', '')
    mfcFile['szFileName'] = fid.read(8).decode()
    mfcFile['szBackgroundFile'] = fid.read(8).decode().replace('\x00', '')
    mfcFile['szGapList'] = [struct.unpack('i', fid.read(4))[0]
                            for index in range(40)]
    mfcFile['szComment'] = fid.read(4).decode().replace('\x00', '')
    mfcFile['iRegionNo'] = struct.unpack('i', fid.read(4))[0]
    mfcFile['pNextRegion'] = fid.read(4).decode().replace('\x00', '')
    mfcFile['pPrevRegion'] = fid.read(4).decode().replace('\x00', '')
    # read the spectrum and store it
    spectrum = np.zeros([mfcFile['iNChannel']], dtype=np.float)
    for pixelid in range(mfcFile['iNChannel']):
        spectrum[pixelid] = struct.unpack('f', fid.read(4))[0]
    mfcFile['spectrum'] = spectrum
    # test if the full file was read
    finalid = fid.tell()
    fid.read()
    testid = fid.tell()
    if testid != finalid:
        print('CAUTION: File was not read completely!!!')
    # close the file
    fid.close()
    # return the dictionary mfcFile
    return mfcFile


def saveMFC(fileName_outMFC, mfcFile_content):
    '''
    This function writes .mfc files readable with the DOASIS or WinDOAS
    software.
    @fileName_outMFC is the path and file name of the written .mfc data file
    @mfcFile_content is a dictionary containing all the information of the file
    which will be written to the .mfc file. The dictionary must have the right
    format (e.g. the output from the function above)
    SAVES a .mfc file
    Some important translations:
    Character does not need translation: 1 character = 1 byte
    Integer needs struct.pack('i', ...): 1 int32 = 1 byte
    Double needs struct.pack('d', ...): 1 double = 8 byte
    By Steffen Doerner and Sebastian Donner based on a matlab script by Julia
    Remmers. The documentation of the .mfc file header is from Stefan
    Kraus, 2006.
    Latest Version: 1.0, 22nd November 2016 (first implementation)
    Latest Version: 1.1, 17th March 2017 (Python3 compatibility)
    '''
    # open a file for writing into it
    fid = open(fileName_outMFC, 'wb')
    # write the different information to the file
    fid.write(convert_to_byte(mfcFile_content['szVersion'], 20))
    fid.write(struct.pack('i', mfcFile_content['iNChannel'] - 1))
    fid.write(convert_to_byte(mfcFile_content['pData'], 4))
    fid.write(convert_to_byte(mfcFile_content['szSpecName'], 20))
    fid.write(convert_to_byte(mfcFile_content['szSite'], 20))
    fid.write(convert_to_byte(mfcFile_content['szSpectrometer'], 20))
    fid.write(convert_to_byte(mfcFile_content['szDevice'], 20))
    fid.write(convert_to_byte(mfcFile_content['szFirstLine'], 80))
    fid.write(convert_to_byte(mfcFile_content['szLater'], 18))
    fid.write(struct.pack('i', mfcFile_content['iSpecInBlock']))
    fid.write(struct.pack('i', mfcFile_content['iNumSpecOfBlock']))
    fid.write(struct.pack('ddd', mfcFile_content['fDispersionHighDensity'][0],
                          mfcFile_content['fDispersionHighDensity'][1],
                          mfcFile_content['fDispersionHighDensity'][2]))
    fid.write(struct.pack('d', mfcFile_content['fWavelength']))
    fid.write(struct.pack('f', mfcFile_content['fAverage']))
    fid.write(struct.pack('h', mfcFile_content['iODLeft']))
    fid.write(struct.pack('h', mfcFile_content['iODCenter']))
    fid.write(struct.pack('h', mfcFile_content['iODRight']))
    fid.write(struct.pack('f', mfcFile_content['fOptDens']))
    fid.write(struct.pack('f', mfcFile_content['fOptWavelength']))
    fid.write(struct.pack('i', mfcFile_content['iType']))
    fid.write(convert_to_byte(mfcFile_content['szDate'], 9))
    fid.write(convert_to_byte(mfcFile_content['szStartTime'], 9))
    fid.write(convert_to_byte(mfcFile_content['szStopTime'], 9))
    fid.write(convert_to_byte(mfcFile_content['cFillDummy1'], 1))
    fid.write(struct.pack('i', mfcFile_content['iMathLow']))
    fid.write(struct.pack('i', mfcFile_content['iMathHigh']))
    fid.write(struct.pack('i', mfcFile_content['iMinChannel']))
    fid.write(struct.pack('i', mfcFile_content['iMaxChannel']))
    fid.write(struct.pack('i', mfcFile_content['iMarker']))
    fid.write(struct.pack('i', mfcFile_content['iNoScans']))
    fid.write(struct.pack('f', mfcFile_content['fExposureTime']))
    fid.write(struct.pack('f', mfcFile_content['fLatitude']))
    fid.write(struct.pack('f', mfcFile_content['fLongitude']))
    fid.write(struct.pack('i', mfcFile_content['iNoPeaks']))
    fid.write(struct.pack('i', mfcFile_content['iNoBands']))
    fid.write(struct.pack('f', mfcFile_content['fMin']))
    fid.write(struct.pack('f', mfcFile_content['fMax']))
    fid.write(struct.pack('f', mfcFile_content['fYScale']))
    fid.write(struct.pack('f', mfcFile_content['fOffsetScale']))
    fid.write(struct.pack('f', mfcFile_content['fWavelength1']))
    fid.write(struct.pack('f', mfcFile_content['fAverageOld']))
    fid.write(struct.pack('fff', mfcFile_content['fDispersion'][0],
                          mfcFile_content['fDispersion'][1],
                          mfcFile_content['fDispersion'][2]))
    fid.write(struct.pack('f', mfcFile_content['fOptDen']))
    fid.write(struct.pack('i', mfcFile_content['iMode']))
    fid.write(struct.pack('i', mfcFile_content['iSmooth']))
    fid.write(struct.pack('i', mfcFile_content['iDegreeRegression']))
    fid.write(convert_to_byte(mfcFile_content['cNull'], 8))
    fid.write(convert_to_byte(mfcFile_content['cRef'], 8))
    fid.write(mfcFile_content['szFileName'].encode())
    fid.write(convert_to_byte(mfcFile_content['szBackgroundFile'], 8))
    for index in range(40):
        fid.write(struct.pack('i', mfcFile_content['szGapList'][index]))
    fid.write(convert_to_byte(mfcFile_content['szComment'], 4))
    fid.write(struct.pack('i', mfcFile_content['iRegionNo']))
    fid.write(convert_to_byte(mfcFile_content['pNextRegion'], 4))
    fid.write(convert_to_byte(mfcFile_content['pPrevRegion'], 4))
    # write the spectrum to mfc file
    for pixelid in range(len(mfcFile_content['spectrum'])):
        fid.write(struct.pack('f', mfcFile_content['spectrum'][pixelid]))
    # finally, close the file
    fid.close()


def convertMFC(MFCfileName, outFileName):
    '''
    This function converts .mfc files readable with the DOASIS or WinDOAS
    software to .txt ASCII files.
    @MFCfileName is the path and file name of the input .mfc data file
    @outFileName is the path and file name of the outout .txt file
    SAVES a .txt ASCII file
    By Steffen Doerner and Sebastian Donner based on a matlab script by Julia
    Remmers. The documentation of the .mfc file header is from Stefan
    Kraus, 2006.
    Latest Version: 1.0, 14th December 2016 (first implementation)
    '''
    # read the mfc file to dictionary mfcFile
    mfcFile = readMFC(MFCfileName)
    # open the output txt file in write mode
    with open(outFileName, 'w') as f:
        f.write('%s:%s\n' % ('szVersion', mfcFile['szVersion']))
        f.write('%s:%s\n' % ('iNChannel', mfcFile['iNChannel']))
        f.write('%s:%s\n' % ('pData', mfcFile['pData']))
        f.write('%s:%s\n' % ('szSpecName', mfcFile['szSpecName']))
        f.write('%s:%s\n' % ('szSite', mfcFile['szSite']))
        f.write('%s:%s\n' % ('szSpectrometer', mfcFile['szSpectrometer']))
        f.write('%s:%s\n' % ('szDevice', mfcFile['szDevice']))
        f.write('%s:%s\n' % ('szFirstLine', mfcFile['szFirstLine']))
        f.write('%s:%s\n' % ('szLater', mfcFile['szLater']))
        f.write('%s:%s\n' % ('iSpecInBlock', mfcFile['iSpecInBlock']))
        f.write('%s:%s\n' % ('iNumSpecOfBlock', mfcFile['iNumSpecOfBlock']))
        f.write('%s:%s\n' % ('fDispersionHighDensity',
                             mfcFile['fDispersionHighDensity']))
        f.write('%s:%s\n' % ('fWavelength', mfcFile['fWavelength']))
        f.write('%s:%s\n' % ('fAverage', mfcFile['fAverage']))
        f.write('%s:%s\n' % ('iODLeft', mfcFile['iODLeft']))
        f.write('%s:%s\n' % ('iODCenter', mfcFile['iODCenter']))
        f.write('%s:%s\n' % ('iODRight', mfcFile['iODRight']))
        f.write('%s:%s\n' % ('fOptDens', mfcFile['fOptDens']))
        f.write('%s:%s\n' % ('fOptWavelength', mfcFile['fOptWavelength']))
        f.write('%s:%s\n' % ('iType', mfcFile['iType']))
        f.write('%s:%s\n' % ('szDate', mfcFile['szDate']))
        f.write('%s:%s\n' % ('szStartTime', mfcFile['szStartTime']))
        f.write('%s:%s\n' % ('szStopTime', mfcFile['szStopTime']))
        f.write('%s:%s\n' % ('cFillDummy1', mfcFile['cFillDummy1']))
        f.write('%s:%s\n' % ('iMathLow', mfcFile['iMathLow']))
        f.write('%s:%s\n' % ('iMathHigh', mfcFile['iMathHigh']))
        f.write('%s:%s\n' % ('iMinChannel', mfcFile['iMinChannel']))
        f.write('%s:%s\n' % ('iMaxChannel', mfcFile['iMaxChannel']))
        f.write('%s:%s\n' % ('iMarker', mfcFile['iMarker']))
        f.write('%s:%s\n' % ('iNoScans', mfcFile['iNoScans']))
        f.write('%s:%s\n' % ('fExposureTime', mfcFile['fExposureTime']))
        f.write('%s:%s\n' % ('fLatitude', mfcFile['fLatitude']))
        f.write('%s:%s\n' % ('fLongitude', mfcFile['fLongitude']))
        f.write('%s:%s\n' % ('iNoPeaks', mfcFile['iNoPeaks']))
        f.write('%s:%s\n' % ('iNoBands', mfcFile['iNoBands']))
        f.write('%s:%s\n' % ('fMin', mfcFile['fMin']))
        f.write('%s:%s\n' % ('fMax', mfcFile['fMax']))
        f.write('%s:%s\n' % ('fYScale', mfcFile['fYScale']))
        f.write('%s:%s\n' % ('fOffsetScale', mfcFile['fOffsetScale']))
        f.write('%s:%s\n' % ('fWavelength1', mfcFile['fWavelength1']))
        f.write('%s:%s\n' % ('fAverageOld', mfcFile['fAverageOld']))
        f.write('%s:%s\n' % ('fDispersion', mfcFile['fDispersion']))
        f.write('%s:%s\n' % ('fOptDen', mfcFile['fOptDen']))
        f.write('%s:%s\n' % ('iMode', mfcFile['iMode']))
        f.write('%s:%s\n' % ('iSmooth', mfcFile['iSmooth']))
        f.write('%s:%s\n' % ('iDegreeRegression',
                             mfcFile['iDegreeRegression']))
        f.write('%s:%s\n' % ('cNull', mfcFile['cNull']))
        f.write('%s:%s\n' % ('cRef', mfcFile['cRef']))
        f.write('%s:%s\n' % ('szFileName', mfcFile['szFileName']))
        f.write('%s:%s\n' % ('szBackgroundFile', mfcFile['szBackgroundFile']))
        f.write('%s:%s\n' % ('szGapList', mfcFile['szGapList']))
        f.write('%s:%s\n' % ('szComment', mfcFile['szComment']))
        f.write('%s:%s\n' % ('iRegionNo', mfcFile['iRegionNo']))
        f.write('%s:%s\n' % ('pNextRegion', mfcFile['pNextRegion']))
        f.write('%s:%s\n' % ('pPrevRegion', mfcFile['pPrevRegion']))
        f.write('spectrum:\n')
        for index in range(len(mfcFile['spectrum'])):
            f.write('%s\n' % mfcFile['spectrum'][index])
        f.close()
