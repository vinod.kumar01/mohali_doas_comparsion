# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 09:22:51 2019

@author: Vinod
"""
# %% imports
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pyorbital import astronomy
#import seaborn as sns
import import_merge_mapa_out as mapa
import os

def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
#Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 2e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', 1e15, sf='0.8')
Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
                     -0.25e16, 6e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4', 'AOD (360nm)', 0, 3, 'MODIS', 'MAIAC', 'c_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')

vcd_mean, flag = np.array(vcd_mean), np.array(flag)
vcd_mean[flag == 2] = np.nan
sza = np.array([astronomy.sun_zenith_angle(i, 76.729, 30.667) for i in time2])
if Inp.tracer == 'hcho':
    keep = sza > 60
    vcd_mean[keep] = np.nan
# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
vcd_mean[OT_cloud==1] = np.nan
vcd_mean[fog==1] = np.nan
#vcd_mean[cloud_type>=3] = np.nan
# %% arranging data
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64')]).T
df.columns = ['Date_time', Inp.tracer+'_VCD']
df['Date_time'] = pd.to_datetime(df['Date_time'])
df[Inp.tracer+'_VCD'] = pd.to_numeric(df[Inp.tracer+'_VCD'], errors='coerce')
df = df.set_index('Date_time')
df[Inp.tracer+'_VCD'] = pd.to_numeric(df[Inp.tracer+'_VCD'], errors='coerce')
month_var  = df.rolling('30D').mean().describe()
month_var_per = 100*(month_var[Inp.tracer+'_VCD']['max']-
                     month_var[Inp.tracer+'_VCD']['min'])
month_var_per /= month_var[Inp.tracer+'_VCD']['mean']
print ("Variability is " + str(month_var_per) + "%")


df_mnth = df.resample('MS').mean()
df_std = df.resample('MS').std()
df_25_diff = df.resample('MS').quantile(0.25)
df_25_diff[Inp.tracer+'_VCD'] = df_mnth[Inp.tracer+'_VCD']-df_25_diff[Inp.tracer+'_VCD']
df_75_diff = df.resample('MS').quantile(0.75)
df_75_diff[Inp.tracer+'_VCD'] = df_75_diff[Inp.tracer+'_VCD']-df_mnth[Inp.tracer+'_VCD']
df_sem = df.resample('MS').sem()
df_mnth.reset_index(inplace=True)
df_std.reset_index(inplace=True)
df_sem.reset_index(inplace=True)
df_25_diff.reset_index(inplace=True)
df_75_diff.reset_index(inplace=True)

df_mnth['year'] = [d.year for d in df_mnth['Date_time']]
df_std['year'] = [d.year for d in df_std['Date_time']]
df_sem['year'] = [d.year for d in df_sem['Date_time']]
df_mnth['month'] = df_mnth['Date_time'].dt.month
df_25_diff['month'] = df_25_diff['Date_time'].dt.month
df_75_diff['month'] = df_75_diff['Date_time'].dt.month
#df_mnth['month'] = [d.strftime('%b') for d in df_mnth['Date_time']]
#df_25_diff['month'] = [d.strftime('%b') for d in df_25_diff['Date_time']]
#df_75_diff['month'] = [d.strftime('%b') for d in df_75_diff['Date_time']]
years = df_mnth['year'].unique()


# %% Seasonal plot showing monthly mean of different years
def plot_trend(ax):
    j=-0.06
    for i, y in enumerate(years):
        if i > 0:
#            error_bar =df_sem[Inp.tracer+'_VCD'][df_mnth.year == y]
            error_bar = [df_25_diff[Inp.tracer+'_VCD'][df_mnth.year == y],
                         df_75_diff[Inp.tracer+'_VCD'][df_mnth.year == y]]
            ax.errorbar(df_mnth['month'][df_mnth.year == y]+j-1,
                        df_mnth[Inp.tracer+'_VCD'][df_mnth.year == y],
                        yerr=error_bar, label=y,
                        ls='--', marker='*', capsize=4)
            j+=0.03
    ax.legend(loc=[0.15, 0.55])
    ax.set_ylabel(Inp.spec_str)
    plt.yticks(fontsize=12, alpha=.7)
    plt.xticks(np.arange(12), ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'))
#    ax.minorticks_on()
    ax.grid(axis='y', alpha=0.4)
#fig,ax = plt.subplots(figsize=[10,4])
#plot_trend(ax)
#    ax.tick_params(axis='x', which='minor', bottom=False)
#fig, ax = plt.subplots(3,1,figsize=[10,9], sharex=True)
#plot_trend(ax[2])
##plt.tight_layout()
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf,
#                         Inp.mapa_spec+'_'+'DOAS_seasonal_yearly_trend_cf.png'),
#                         format='png', dpi=300)
#plt.title('Seasonal Plot of ' + Inp.tracer + ' Time Series', fontsize=20)
#plt.show()

## %% Annual and seasonal trends as box whiskers
#fig, axes = plt.subplots(1, 2, figsize=(20, 7))
#sns.boxplot(x='year', y=Inp.tracer+'_VCD',
#            data=df_mnth.loc[~df_mnth.year.isin([2012]), :], ax=axes[0])
#sns.boxplot(x='month', y=Inp.tracer+'_VCD',
#            data=df_mnth.loc[~df_mnth.year.isin([2012]), :])
#axes[0].set_title('Year-wise Box Plot\n(The Trend)', fontsize=18)
#plt.show()
#axes[1].set_title('Month-wise Box Plot\n(The Seasonality)', fontsize=18)