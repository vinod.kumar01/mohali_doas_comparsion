# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 23:48:42 2020

@author: Vinod
"""
import numpy as np
import pandas as pd
from matplotlib import dates


def nc():
    def universal_gas_constant():
        return 8.3144621    # J / (K * mol)

    def Avogadros_number():
        return 6.022140857E23

    def msl_pressure():
        return 1.01325E5  # Pa

    def scale_height():
        return 7400   # meters for P0 = 101325 Pa and 250K


def O4_conc(P, T):
    '''
    calculate O4 concentration
    if temperature and pressure are provided
    P: Pressure (Pa)
    T: temperature in K
    '''
    P = np.asarray([P], dtype=float) if np.isscalar(P) else np.asarray(P)
    T = np.asarray([T], dtype=float) if np.isscalar(T) else np.asarray(T)
    # check temperatire units
    if np.nanmean(T < 60):
        T += 273.15
    R = nc.universal_gas_constant()
    N_A = nc.Avogadros_number()
    n_air = (P*N_A)/(T*R)  # molecules m-3
    n_air *= 1E-6
    c_o2 = 0.20942
    n_o4 = (c_o2*n_air)**2
    n_o4 = np.asscalar(n_o4) if len(n_o4) == 1 else n_o4
    # unit molecules2cm-5
    return n_o4


def conc2vmr(conc, P, T):
    # returns VMR in ppb from concentration in molecules cm-3
    P = np.asarray([P], dtype=float) if np.isscalar(P) else np.asarray(P)
    T = np.asarray([T], dtype=float) if np.isscalar(T) else np.asarray(T)
    N_A = nc.Avogadros_number()
    if np.nanmean(T < 60):
        T += 273.15
    vmr = conc*(8.314*T*1E6)
    vmr /= N_A*1E-9*97000
    vmr = np.asscalar(vmr) if len(vmr) == 1 else vmr
    return vmr


def vmr2conc(vmr, P, T):
    P = np.asarray([P], dtype=float) if np.isscalar(P) else np.asarray(P)
    T = np.asarray([T], dtype=float) if np.isscalar(T) else np.asarray(T)
    N_A = nc.Avogadros_number()
    if np.nanmean(T < 60):
        T += 273.15
    conc = vmr*1E-9*N_A*P*1E-6/nc.universal_gas_constant()/T
    return conc


def plot_diurnal(data, date_time, ax, label='Mean', freq='H', color='r'):
    df2 = pd.DataFrame(data={'dt': date_time, 'data': data})
    df2 = df2.set_index('dt')
    df2 = df2.resample(freq).mean()
    df2['Time'] = df2.index.map(lambda x: x.strftime("%H:%M"))
    g_data = df2.groupby('Time').describe()
    g_data.index = pd.to_datetime(g_data.index.astype(str))
    dft = g_data.index.to_pydatetime()
    ax.plot(dft, g_data['data']['mean'], color, marker='o',
            linewidth=2.0, label=label)
    ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
    ax.plot(dft, g_data['data']['75%'], color=color, alpha=0.6,
            label='_nolegend_')
    ax.plot(dft, g_data['data']['25%'], color=color, alpha=0.6,
            label='_nolegend_')
    ax.fill_between(dft, g_data['data']['mean'], g_data['data']['75%'],
                    alpha=.4, facecolor=color)
    ax.fill_between(dft, g_data['data']['mean'], g_data['data']['25%'],
                    alpha=.4, facecolor=color)
    ax.legend(loc='upper left')
    return(ax, g_data)


def O4_VCD_sfc(sfc_temp, sfc_height):   ##sfc temp in K, SFC_height in meters
    '''
    calculate O4 VCD assuming lapse rate of -0.65k per 100m
    if surface temp and sfc height is provided
    '''
    h = np.arange(sfc_height, 20000, 100)
    temp_profile = [sfc_temp+i*(-0.65) if j <= 12000 else 215
                    for i, j in enumerate(h)]
    pressure_profile = nc.msl_pressure()*np.exp((-h)*temp_profile/nc.scale_height()/250)
    O2_conc = vmr2conc(0.20946*1E9, pressure_profile, temp_profile)
    O4_VCD = np.sum(100*100*O2_conc**2)  # box height 100m= 100*100cm
    return O4_VCD
