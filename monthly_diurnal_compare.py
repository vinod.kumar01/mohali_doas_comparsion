# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 22:45:47 2019

@author: Vinod
"""
# %% imports and function definitons
import os
import pandas as pd
import matplotlib.pyplot as plt
import calendar
import numpy as np
from datetime import datetime, timedelta
from matplotlib import dates
from pyorbital import astronomy
import import_merge_mapa_out as mapa


def get_insitu_data(ts, insitu_file):

    colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
                'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
                'abs_humidity', 'hcho']
    insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                              keep_default_na=False, names=colnames)
    insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                             format='%d-%m-%Y %H:%M:%S')
    insitu_data['desttime'] -= timedelta(minutes=330)  # offset for IST
    for cols in colnames[1:]:
        insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')
    insitu_data = insitu_data.set_index('desttime')
    insitu_data_intp = insitu_data.reindex(ts, method='nearest', limit=5)
    return(insitu_data_intp['no2'], insitu_data_intp['hcho'],
           insitu_data_intp['Wind_speed'], insitu_data_intp['Amb_temp'])


def interpolate(ts, datetime_index):
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    return x.groupby(x.index).first().sort_index().fillna(
        method="ffill", limit=5)[datetime_index]

def interpolate_cloud(ref_time, cloud_inp):

    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])

def diurnal(df, param_col, dtcol='Date_Time', mean_freq='30T'):
    cols = [dtcol, param_col]
    tempdf = df[cols]
    tempdf = tempdf.set_index(dtcol)
    tempdf = tempdf.resample(mean_freq).mean()
    tempdf['Time'] = tempdf.index.map(lambda x: x.strftime("%H:%M"))
    tempdf = tempdf.groupby('Time').describe()
    tempdf.index = pd.to_datetime(tempdf.index.astype(str))
    return tempdf

# %% load MAX DOAS data
# Max DOAS(mapa) Data
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ mixing ratio (ppb)',
                     0, 50, 'Chemiluminescence', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO concentration (ppb)',
#                0, 21, 'PTR-MS', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4', 'Aerosol extinction (1/km)', 0, 4, 'OMI', 'NASA', 'c_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v3'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
profile_mean, flag = np.array(profile_mean), np.array(flag)
vcd_mean = np.array(vcd_mean)
time2  = np.array(time2)
sza = np.array([astronomy.sun_zenith_angle(i, 76.729, 30.667) for i in time2])
# %% cloud classification data
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')

cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
# %% Data filter
vcd_mean[(flag[:] != 0) | (fog==1) | (OT_cloud==1)] = np.nan
mask = np.empty(profile_mean.shape, dtype=bool)
mask[:, :] = ((flag[:] != 0) | (fog==1) | (OT_cloud==1))[:, np.newaxis]
# profile_mean = np.ma.MaskedArray(profile_mean, mask=mask)
profile_mean[mask] = np.nan  # masked values have problem in pandas
if Inp.tracer == 'hcho':
    keep = sza > 60
else:
    keep = sza > 85
profile_mean[keep, :] = np.nan

conc_surf = profile_mean[:, 0]

time3 = time2.astype(datetime)
df_mapa = pd.DataFrame(np.column_stack((time3, conc_surf, vcd_mean, flag)),
                   columns=['Date_time', 'conc_surface',
                            'vcd_mean', 'flag'])
df_mapa['Date_time'] = pd.to_datetime(df_mapa['Date_time'],
                                  format='%Y-%m-%d %H:%M:%S')
for cols in df_mapa.columns[1:]:
    df_mapa[cols] = pd.to_numeric(df_mapa[cols])
df_mapa['Date_time'] = df_mapa['Date_time'] + timedelta(minutes=330)
df_mapa['month'] = [d.month for d in df_mapa['Date_time']]
df_mapa = df_mapa.set_index('Date_time')


# %%in situ data
# spec = 'no2'
insitu_path = r'D:\postdoc\DOAS\Mohali_2013_17'
insitu_file = os.path.join(insitu_path, 'in_situ.csv')
colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
            'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
            'abs_humidity', 'hcho']
insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=colnames)
insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                         format='%d-%m-%Y %H:%M:%S')
for cols in colnames[1:]:
    insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')
insitu_data = insitu_data.set_index('desttime')
df_mapa['ws'] = interpolate(insitu_data['Wind_Speed'], df_mapa.index)
df_mapa['amb_temp'] = interpolate(insitu_data['Amb_temp'], df_mapa.index)
if Inp.tracer=="aod":
    df_mapa[Inp.tracer+'_insitu'] = np.nan
    df_mapa[Inp.tracer+'_doasmr'] = df_mapa['conc_surface']
else:
    df_mapa[Inp.tracer+'_insitu'] = interpolate(insitu_data[Inp.tracer], df_mapa.index)
    df_mapa[Inp.tracer+'_doasmr'] = df_mapa['conc_surface']*(8.314*(df_mapa['amb_temp']+273.16)*1E6)
    df_mapa[Inp.tracer+'_doasmr'] /= 6.023E23*1E-9*97000

df_mapa = df_mapa.reset_index(inplace=None)
#df_mapa_hour = df_mapa['Date_time'].dt.hour

insitu_data = insitu_data.reset_index(inplace=None)
insitu_data['month'] = [d.month for d in insitu_data['desttime']]


# %%plotting
df_month = {}
fig, ax = plt.subplots(3, 4, figsize=[22, 13], sharex=True, sharey=True)
for i in range(0, 3):
    for j in range(0, 4):
        month = (4*i)+j+1
        df_month['insitu'+str(month)] = diurnal(insitu_data[insitu_data['month']==month],
                                                Inp.tracer, 'desttime', '30T')
        df_month['mapa'+str(month)]=diurnal(df_mapa[df_mapa['month'] == month],
                                            Inp.tracer+'_doasmr', 'Date_time', '30T')
        df_month['vcd'+str(month)]=diurnal(df_mapa[df_mapa['month'] == month],
                                            'vcd_mean', 'Date_time', '30T')
        df_month['mapa'+str(month)].loc[df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['count']<=5, :]=np.nan
        df_month['vcd'+str(month)].loc[df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['count']<=5, :]=np.nan
        ax[i, j].plot(df_month['insitu'+str(month)].index,
                      df_month['insitu'+str(month)][Inp.tracer]['mean'],
                      'k', linewidth=2.0, label='in situ')
        ax[i, j].plot(df_month['insitu'+str(month)].index,
                      df_month['insitu'+str(month)][Inp.tracer]['75%'],
                      color='k', label='_nolegend_')
        ax[i, j].plot(df_month['insitu'+str(month)].index,
                      df_month['insitu'+str(month)][Inp.tracer]['25%'],
                      color='k', label='_nolegend_')
        ax[i, j].fill_between(df_month['insitu'+str(month)].index,
                              df_month['insitu'+str(month)][Inp.tracer]['mean'],
                              df_month['insitu'+str(month)][Inp.tracer]['75%'],
                              alpha=.5, facecolor='k')
        ax[i, j].fill_between(df_month['insitu'+str(month)].index,
                              df_month['insitu'+str(month)][Inp.tracer]['mean'],
                              df_month['insitu'+str(month)][Inp.tracer]['25%'],
                              alpha=.5, facecolor='k')
        ax[i, j].xaxis.set_major_formatter(dates.DateFormatter('%H'))
        ax[i, j].minorticks_on()
        ax[i, j].set_ylim(Inp.y_min, Inp.y_max)
        ax[i, j].plot(df_month['mapa'+str(month)].index,
                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
                      'r', linewidth=2.0, label = 'MAX-DOAS')
        ax[i, j].plot(df_month['mapa'+str(month)].index,
                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['75%'],
                      color='r', label='_nolegend_')
        ax[i, j].plot(df_month['mapa'+str(month)].index,
                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['25%'],
                      color='r', label='_nolegend_')
        ax[i, j].fill_between(df_month['mapa'+str(month)].index,
                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['75%'],
                              alpha=.5, facecolor='r')
        ax[i, j].fill_between(df_month['mapa'+str(month)].index,
                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['25%'],
                              alpha=.5, facecolor='r')
        ax[i, j].text(0.85, 0.9, calendar.month_abbr[(4*i)+j+1],
                      transform=ax[i][j].transAxes, size=12)
        ax[i, j].grid(axis='x', alpha=0.5)
        if i == 2:
            ax[i, j].set_xlabel('Local time (UTC+5:30))', size=12)


#        ax[i, j].plot(np.nan, '-r', label = 'MAX-DOAS')
        #mapa
#        ax2 = ax[i, j].twinx()
#        ax2.plot(df_month['vcd'+str(month)].index,
#                      df_month['vcd'+str(month)]['vcd_mean']['mean'],
#                      'g', linewidth=2.0, label = 'VCD')
#        ax2.plot(df_month['vcd'+str(month)].index,
#                      df_month['vcd'+str(month)]['vcd_mean']['75%'],
#                      color='g', label='_nolegend_')
#        ax2.plot(df_month['vcd'+str(month)].index,
#                      df_month['vcd'+str(month)]['vcd_mean']['25%'],
#                      color='g', label='_nolegend_')
#        ax2.fill_between(df_month['vcd'+str(month)].index,
#                              df_month['vcd'+str(month)]['vcd_mean']['mean'],
#                              df_month['vcd'+str(month)]['vcd_mean']['75%'],
#                              alpha=.5, facecolor='g')
#        ax2.fill_between(df_month['vcd'+str(month)].index,
#                              df_month['vcd'+str(month)]['vcd_mean']['mean'],
#                              df_month['vcd'+str(month)]['vcd_mean']['25%'],
#                              alpha=.5, facecolor='g')
#        ax2.spines['right'].set_color('g')
#        ax2.minorticks_on()
#        if j != 3:
#            ax2.set_yticklabels([])
#        if i == 0:
#            ax2.legend(loc=[0.85, 0.8])
        
#        ax2.plot(df_month['mapa'+str(month)].index,
#                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
#                      'r', linewidth=2.0, label = 'DOAS')
#        ax2.plot(df_month['mapa'+str(month)].index,
#                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['75%'],
#                      color='r', label='_nolegend_')
#        ax2.plot(df_month['mapa'+str(month)].index,
#                      df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['25%'],
#                      color='r', label='_nolegend_')
#        ax2.fill_between(df_month['mapa'+str(month)].index,
#                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
#                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['75%'],
#                              alpha=.5, facecolor='r')
#        ax2.fill_between(df_month['mapa'+str(month)].index,
#                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['mean'],
#                              df_month['mapa'+str(month)][Inp.tracer+'_doasmr']['25%'],
#                              alpha=.5, facecolor='r')
#        ax2.set_ylim(Inp.y_min, Inp.y_max)
#        ax2.xaxis.set_major_formatter(dates.DateFormatter('%H'))
#        ax2.spines['right'].set_color('r')
#
#        ax2.minorticks_on()
#        if j != 3:
#            ax2.set_yticklabels([])
    ax[i, 0].set_ylabel(Inp.spec_str, size=12)
ax[0, 0].legend(loc='upper left', fontsize=12)
#ax[i, j].set_xlim(df_month['mapa'+str(month)].index[9], df_month['mapa'+str(month)].index[38])
#plt.tight_layout()
plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 'valid',
                         Inp.tracer+'_diurnal_validv5.png'), format='png', dpi=300)
