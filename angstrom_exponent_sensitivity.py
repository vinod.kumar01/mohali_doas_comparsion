# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 12:14:47 2019

@author: Vinod
"""

import netCDF4
f_12 = netCDF4.Dataset(r'M:\nobackup\vinod\MAPA\Results\Vinod\mapa_v098_cnf_098\analysis_2013_4_auto\no2_vis\no2_vis_o4sf0.8.hdf')
vcd_12 = f_12.variables['vcd_weighted_mean'][:]
flag_12 = f_12.variables['flag_total'][:]

f_10 = netCDF4.Dataset(r'M:\nobackup\vinod\MAPA\Results\AEtest\Vinod\mapa_v098_cnf_098\analysis_2013_4_auto\no2_vis\no2_vis_o4sf0.8.hdf')
vcd_10 = f_10.variables['vcd_weighted_mean'][:]
flag_10 = f_10.variables['flag_total'][:]