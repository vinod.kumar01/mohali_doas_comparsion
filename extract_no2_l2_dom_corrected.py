# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 23:19:35 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import pandas as pd
import sys
from datetime import date, datetime
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# %% define limits and path
lat_bound = [30.4, 30.9]
lon_bound = [76.4, 77.0]

#lat_bound = [30.57, 30.77]
#lon_bound = [76.63, 76.83]
profile_dir = 'D:\postdoc\Sat\OMI\TS_mohali'
filename_profile = r'DOMINO_profile_mohali_2015_corrsf_1_glue2km.nc'
f_profile = netCDF4.Dataset(os.path.join(profile_dir, filename_profile), 'r')
meas_profile = f_profile.variables['no2_sub_col_mod'][:]
meas_rel_profile = meas_profile[:, 0:16]
for days in range(meas_profile.shape[0]):
    meas_rel_profile[days, :] = meas_rel_profile[days, :]/np.ma.sum(meas_rel_profile[days, :])
    
dirname = r"M:\DATASETS\www.temis.nl\airpollution\no2col\data\omi\data_v2"
vcd_ts = []
ch_ts = []
vcd_corr_ts = []
vcd_ts_std = []
vcd_ts_cf_30 = []
vcd_corr_ts_cf_30 = []
vcd_ts_cf_30_std = []
date_time = []
vcd_ts_cf_40 = []
vcd_ts_cf_35 = []
vcd_ts_cf_25 = []
vcd_ts_cf_20 = []
vcd_ts_cf_15 = []
vcd_ts_cf_10 = []
vcd_corr_ts_cf_40 = []
vcd_corr_ts_cf_35 = []
vcd_corr_ts_cf_25 = []
vcd_corr_ts_cf_20 = []
vcd_corr_ts_cf_15 = []
vcd_corr_ts_cf_10 = []
start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(dirname,
                                        single_date.strftime("%Y\%m\%d"),
                                        'OMI-Aura_L2-OMDOMINO_' + '*.he5'))
    vcd_day = []
    vcd_day_corr = []
    vcd_day_cf_30 = []
    vcd_day_cf_40, vcd_day_cf_35, vcd_day_cf_25 = [], [], []
    vcd_day_cf_20, vcd_day_cf_15, vcd_day_cf_10 = [], [], []
    vcd_day_corr_cf_30 = []
    vcd_day_corr_cf_40, vcd_day_corr_cf_35, vcd_day_corr_cf_25 = [], [], []
    vcd_day_corr_cf_20, vcd_day_corr_cf_15, vcd_day_corr_cf_10 = [], [], []
    cp_day = []
    for path_sat in f_name_sat:
        AK_orb = []
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        tempdate = path_sat.split('\\')[-1].split('_')[2]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[5:7])
        temp_day = int(tempdate[7:9])
        temp_hour = int(tempdate[10:12])
        temp_min = int(tempdate[12:14])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        daynum = min(temp_dt.timetuple().tm_yday, 365)
        swath = f.groups['HDFEOS'].groups['SWATHS']
        Geoloc_fields = swath.groups['DominoNO2'].groups['Geolocation Fields']
        lats = Geoloc_fields.variables['Latitude'][:]
        lons = Geoloc_fields.variables['Longitude'][:]
        Data_fields = swath.groups['DominoNO2'].groups['Data Fields']
        NO2_tropcol = Data_fields.variables['TroposphericVerticalColumn']
        AMF_trop = Data_fields.variables['AirMassFactorTropospheric']
        AMF_tot = Data_fields.variables['AirMassFactor']
        AK = Data_fields.variables['AveragingKernel']
        cf = Data_fields.variables['CloudFraction'][:]*0.001
#        cf = Data_fields.variables['CloudRadianceFraction'][:]*0.01*0.01
        cp = Data_fields.variables['CloudPressure']
        cp_fv = cp._FillValue
#             flag = Data_fields.variables['MeasurementQualityFlags'][:]
        mv = NO2_tropcol.MissingValue
        fv = NO2_tropcol._FillValue
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        NO2_tropcol = NO2_tropcol[:]
        NO2_tropcol = NO2_tropcol[keep]
        NO2_tropcol[NO2_tropcol==fv] = np.nan
        cf = cf[keep]
        NO2_tropcol_cf_30 = NO2_tropcol[(cf < 0.3)]
        NO2_tropcol_cf_40 = NO2_tropcol[(cf < 0.4)]
        NO2_tropcol_cf_35 = NO2_tropcol[(cf < 0.35)]
        NO2_tropcol_cf_25 = NO2_tropcol[(cf < 0.25)]
        NO2_tropcol_cf_20 = NO2_tropcol[(cf < 0.2)]
        NO2_tropcol_cf_15 = NO2_tropcol[(cf < 0.15)]
        NO2_tropcol_cf_10 = NO2_tropcol[(cf < 0.10)]
        # further processing of AMF correction on the relevant orbits
        if len(NO2_tropcol) > 0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                + ' : ' + str(orbit_num))
            cp = cp[:][keep].astype('float32')
            cp[cp<0] = np.nan
            trop_SCD = NO2_tropcol/AMF_trop[:][keep]
            AMF_tot = AMF_tot[:][keep]
            trop_amf_scale = np.zeros(len(AMF_tot), dtype='float')
            # filter averaging kernels for pixels within spatial boundary
            for lev in range(34):
                AK_orb.append(AK[lev][keep]*0.001)
            AK_orb = np.array(AK_orb)
            for i in range(len(AMF_tot)):
                trop_amf_scale[i] = np.ma.divide(np.ma.sum(meas_rel_profile[daynum-1]*AK_orb[0:16,i]), np.ma.sum(meas_rel_profile[daynum-1]))
            trop_amf_corr = AMF_tot*trop_amf_scale
            NO2_tropcol_corr = trop_SCD/trop_amf_corr        
          
            vcd_day_corr.extend(NO2_tropcol_corr)
            vcd_day.extend(NO2_tropcol)
            cp_day.extend(cp)
            NO2_tropcol_corr_cf_30 = NO2_tropcol_corr[(cf < 0.3)]
            NO2_tropcol_corr_cf_40 = NO2_tropcol_corr[(cf < 0.4)]
            NO2_tropcol_corr_cf_35 = NO2_tropcol_corr[(cf < 0.35)]
            NO2_tropcol_corr_cf_25 = NO2_tropcol_corr[(cf < 0.25)]
            NO2_tropcol_corr_cf_20 = NO2_tropcol_corr[(cf < 0.2)]
            NO2_tropcol_corr_cf_15 = NO2_tropcol_corr[(cf < 0.15)]
            NO2_tropcol_corr_cf_10 = NO2_tropcol_corr[(cf < 0.1)]
        if len(NO2_tropcol_cf_40 > 0):
            vcd_day_cf_30.extend(NO2_tropcol_cf_30)
            vcd_day_corr_cf_30.extend(NO2_tropcol_corr_cf_30)
            vcd_day_cf_40.extend(NO2_tropcol_cf_40)
            vcd_day_corr_cf_40.extend(NO2_tropcol_corr_cf_40)
            vcd_day_cf_35.extend(NO2_tropcol_cf_35)
            vcd_day_corr_cf_35.extend(NO2_tropcol_corr_cf_35)
            vcd_day_cf_25.extend(NO2_tropcol_cf_25)
            vcd_day_corr_cf_25.extend(NO2_tropcol_corr_cf_25)
            vcd_day_cf_20.extend(NO2_tropcol_cf_20)
            vcd_day_corr_cf_20.extend(NO2_tropcol_corr_cf_20)
            vcd_day_cf_15.extend(NO2_tropcol_cf_15)
            vcd_day_corr_cf_15.extend(NO2_tropcol_corr_cf_15)
            vcd_day_cf_10.extend(NO2_tropcol_cf_10)
            vcd_day_corr_cf_10.extend(NO2_tropcol_corr_cf_10)
        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_corr_daymean = np.nanmean(vcd_day_corr)
        vcd_daystd = np.nanstd(vcd_day)
        cp_daymean = np.nanmean(cp_day)
        # calculation of cloud height assuming scale height of 8.5km and 
        # surface pressure of 970 hpa
        ch_daymean = 8.5*np.log(970/cp_daymean)
    else:
        vcd_daymean = np.nan
        vcd_corr_daymean = np.nan
        vcd_daystd = np.nan
        ch_daymean = np.nan
    if len(vcd_day_cf_40) > 0:
        vcd_daymean_cf_30 = np.nanmean(vcd_day_cf_30)
        vcd_corr_daymean_cf_30 = np.nanmean(vcd_day_corr_cf_30)
        vcd_daystd_cf_30 = np.nanstd(vcd_day_cf_30)
        vcd_daymean_cf_40 = np.nanmean(vcd_day_cf_40)
        vcd_corr_daymean_cf_40 = np.nanmean(vcd_day_corr_cf_40)
        vcd_daymean_cf_35 = np.nanmean(vcd_day_cf_35)
        vcd_corr_daymean_cf_35 = np.nanmean(vcd_day_corr_cf_35)
        vcd_daymean_cf_25 = np.nanmean(vcd_day_cf_25)
        vcd_corr_daymean_cf_25 = np.nanmean(vcd_day_corr_cf_25)
        vcd_daymean_cf_20 = np.nanmean(vcd_day_cf_20)
        vcd_corr_daymean_cf_20 = np.nanmean(vcd_day_corr_cf_20)
        vcd_daymean_cf_15 = np.nanmean(vcd_day_cf_15)
        vcd_corr_daymean_cf_15 = np.nanmean(vcd_day_corr_cf_15)
        vcd_daymean_cf_10 = np.nanmean(vcd_day_cf_10)
        vcd_corr_daymean_cf_10 = np.nanmean(vcd_day_corr_cf_10)
    else:
        vcd_daymean_cf_30 = np.nan
        vcd_corr_daymean_cf_30 = np.nan
        vcd_daystd_cf_30 = np.nan
        vcd_daymean_cf_40 = np.nan
        vcd_corr_daymean_cf_40 = np.nan
        vcd_daymean_cf_35 = np.nan
        vcd_corr_daymean_cf_35 = np.nan
        vcd_daymean_cf_25 = np.nan
        vcd_corr_daymean_cf_25 = np.nan
        vcd_daymean_cf_20 = np.nan
        vcd_corr_daymean_cf_20 = np.nan
        vcd_daymean_cf_15 = np.nan
        vcd_corr_daymean_cf_15 = np.nan
        vcd_daymean_cf_10 = np.nan
        vcd_corr_daymean_cf_10 = np.nan
    vcd_ts.append(vcd_daymean)
    vcd_corr_ts.append(vcd_corr_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf_30.append(vcd_daymean_cf_30)
    vcd_corr_ts_cf_30.append(vcd_corr_daymean_cf_30)
    vcd_ts_cf_30_std.append(vcd_daystd_cf_30)
    
    vcd_ts_cf_40.append(vcd_daymean_cf_40)
    vcd_corr_ts_cf_40.append(vcd_corr_daymean_cf_40)
    vcd_ts_cf_35.append(vcd_daymean_cf_35)
    vcd_corr_ts_cf_35.append(vcd_corr_daymean_cf_35)
    vcd_ts_cf_25.append(vcd_daymean_cf_25)
    vcd_corr_ts_cf_25.append(vcd_corr_daymean_cf_25)
    vcd_ts_cf_20.append(vcd_daymean_cf_20)
    vcd_corr_ts_cf_20.append(vcd_corr_daymean_cf_20)
    vcd_ts_cf_15.append(vcd_daymean_cf_15)
    vcd_corr_ts_cf_15.append(vcd_corr_daymean_cf_15)
    vcd_ts_cf_10.append(vcd_daymean_cf_10)
    vcd_corr_ts_cf_10.append(vcd_corr_daymean_cf_10)

    ch_ts.append(ch_daymean)
    date_time.append(single_date.strftime("%Y-%m-%d"))
# %% create time series and save output
omi_ts = np.vstack((date_time, vcd_ts, vcd_corr_ts, vcd_ts_std, vcd_ts_cf_30,
                    vcd_corr_ts_cf_30, vcd_ts_cf_30_std, ch_ts, 
                    vcd_ts_cf_40, vcd_corr_ts_cf_40,
                    vcd_ts_cf_35, vcd_corr_ts_cf_35,
                    vcd_ts_cf_25, vcd_corr_ts_cf_25,
                    vcd_ts_cf_20, vcd_corr_ts_cf_20,
                    vcd_ts_cf_15, vcd_corr_ts_cf_15,
                    vcd_ts_cf_10, vcd_corr_ts_cf_10)).T
np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\no2_OMI_DOM_TS50_sf08glue_2km_corr.csv", omi_ts,
           delimiter=",",
           header = ('date_time, vcd, vcd_corr, vcd_std,'
                     'vcd_cf,vcd_corr_cf, vcd_cf_std, cloud_heigt,'
                     'vcd_cf_40,vcd_corr_cf_40,'
                     'vcd_cf_35,vcd_corr_cf_35,'
                     'vcd_cf_25,vcd_corr_cf_25,'
                     'vcd_cf_20,vcd_corr_cf_20,'
                     'vcd_cf_15,vcd_corr_cf_15,'
                     'vcd_ts_cf_10, vcd_corr_cf_10'),
           fmt="%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log1.txt', 'w')