# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 19:54:50 2019

@author: Vinod
"""
# %% Imports and declarations
import numpy as np
import import_merge_mapa_out as mapa
import os
import pandas as pd
import matplotlib.pyplot as plt
import calendar
from scipy.interpolate import interp1d
import netCDF4
from datetime import date, timedelta


def interpolate_cloud(ref_time, cloud_inp):

    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])


def fill_nan_mean(a):
    col_mean = np.nanmean(a, axis=0)
    inds = np.where(np.isnan(a))
    a[inds] = np.take(col_mean, inds[1])
    return(a)

# %% load data
#Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ concentration (molecules cm$^{-3}$)',
#                     5e9, 9e11, 'Chemiluminescence', 'IISERM', 'vcd_weighted_mean', sf='0.8')
Inp = mapa.load_plot('hcho', 'hcho', 'HCHO concentration (molecules cm$^{-3}$)',
                5e9, 7e11, 'PTR-MS', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4', 'AOD', 0, 2.5, 'OMI', 'NASA', 'c_weighted_mean', sf='0.8')
save_bAMF = False
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v3'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
time2 = np.array(time2)
profile_mean = np.ma.masked_array(profile_mean)

# cloud classification data
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')


cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)

#%% data filter
# cloud filter and mapa valid
keep = (cloud_type<=2) & (fog==0) & (OT_cloud==0) & (flag==0)
profile_clear = profile_mean[keep, :]
flag_clear = flag[keep]
time2_clear = time2[keep]
hour_clear = np.array([int(str(i).split('T')[1][:2]) for i in time2_clear])

# Filter in the values around OMI overpass
keep_omi = (hour_clear>6)&(hour_clear<9)
profile_clear_omi = profile_clear[keep_omi, :]
time2_clear_omi = time2_clear[keep_omi]
month_clear_omi = np.array([int(str(i).split('T')[0][5:7]) for i in time2_clear_omi])
z_c = np.arange(100, 20000, 200)
# %% plot and save bAMF around OMI overpass
if save_bAMF:
    fig,ax = plt.subplots()
    ax.set_prop_cycle(color=['darkgrey', 'k', 'lawngreen', 'gold', 'violet',
                             'r', 'b', 'cyan', 'g', 'maroon', 'indigo',
                             'rosybrown'])   
    profile_month = np.zeros((100, 12))
    for i in range(1,13):
        idx = (month_clear_omi==i)
        profile_month[:, i-1] = np.nanmean(profile_clear_omi[idx, :], axis=0)
        ax.plot(profile_month[:, i-1], z_c[:], '-+',
                label=calendar.month_abbr[i])
        ax.legend(loc='upper right', ncol=2)
        ax.set_ylim(0, 4000)
        ax.set_xlim(-0.01, 3)
        ax.minorticks_on()
        ax.grid(alpha=0.4)
        ax.set_ylabel('Altitude (meters)')
        ax.set_xlabel('Aerosol extinction (1/km)')
        plt.tight_layout()
    plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf,
                         Inp.tracer+'_mean_month_profile_abs_omi.png'),
                         format='png', dpi=300)
    np.savetxt(r"D:\postdoc\Sat\OMI\AOD_noon_profile_new.csv", profile_month,
               delimiter=",")

# %% plot monthly absolute and relative profiles
fig,ax = plt.subplots()
def plot_profile(ax):
    ax.set_prop_cycle(color=['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
                        'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])
    for i in range(1,13):
        idx = month_clear_omi==i
        abs_profile = np.nanmean(profile_clear_omi[idx, :], axis=0)
        rel_profile = abs_profile/np.sum(abs_profile)
        ax.plot(rel_profile, z_c[:], '-+',
                label=calendar.month_abbr[i])
    ax.set_ylabel('Altitude (meters)')
    ax.set_ylim(0, 4000)
    ax.set_xlim(-0.01, 1)
    ax.set_xlabel(Inp.spec_str)
#    ax.set_xlabel('Fraction of '+Inp.spec_str.split('(')[0])
    ax.legend(loc='upper right', ncol=2)
    ax.minorticks_on()
    ax.grid(alpha=0.4)

plot_profile(ax)
plt.tight_layout()
plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf,
                         Inp.tracer+'_mean_month_profile_relv5.png'),
                         format='png', dpi=300)

# %%correct OMI relative profiles
#filename_omi = 'DOMINO_profile_mohali_2015_corrsf_1_glue2km.nc'
filename_omi = 'QA4ECV_hcho_profile_mohali_2015_dom_temp2.nc'
omi_dir = r'D:\postdoc\Sat\OMI\TS_mohali'
f_omi = netCDF4.Dataset(os.path.join(omi_dir, filename_omi), 'r')
date_time = f_omi.variables['time'][:]
date_time = [date(2015,1,1)+timedelta(days=float(i)) for i in date_time]
month = np.array([i.month for i in date_time])
# calculate pressure
prs_i = np.array([f_omi.variables['hyai'][i]+(97000*f_omi.variables['hybi'][i])
                    for i in range(0,35)])
prs_m = np.array([f_omi.variables['hyam'][i]+(97000*f_omi.variables['hybm'][i])
                for i in range(0,34)])
temp = f_omi.variables['Temperature'][:]
prs_i[prs_i<0]=np.nan
#calculate and correct height
h_interface=np.array([7.4*np.log(97000/prs_i[i]) for i in range(0,35)])*1000 
h=np.array([h_interface[i]-h_interface[i-1] for i in range(1,35)])
h=h*temp[100,:]/250
z_centre = (h_interface[1:]+h_interface[:-1])/2 
z_centre=z_centre*temp[100,:]/250
z_centre = np.array(z_centre)
if Inp.tracer == 'no2':
    no2_bvcd = f_omi.variables['NO2'][:]*1E15
    #convert subcolumn to concentartion
    tracer_conc = no2_bvcd/h/100
else:
    hcho_vmr = f_omi.variables['HCHO'][:]
    tracer_conc = hcho_vmr*6.023e23*prs_m*1e-6/8.314/temp
    tracer_conc = fill_nan_mean(tracer_conc.filled(np.nan))
tracer_conc_mod = tracer_conc.copy()
#apply monthly correction for relative profile
for i in range(1,13):
    idx_doas = month_clear_omi==i
    abs_profile_doas = np.nanmean(profile_clear_omi[idx_doas, :], axis=0)
    rel_profile_doas = abs_profile_doas/np.sum(abs_profile_doas)
    # get a function for MAX-DOAS profile. OMI profile is below 200m,
    #so I need to set same values for less than 200m 
    fx_doas = interp1d(z_c[:19], rel_profile_doas[:19],
                       fill_value=(rel_profile_doas[0], rel_profile_doas[19]),
                       bounds_error=False)
    idx_omi = np.argwhere(month==i)[:,0]
    #i will apply the correction to relative profiles as absolute values differ
    # but i can only do it upto 4km until where DOAS is sensitive
    # 16 is tropopause level until where i sum to get relative profile
    abs_profile_omi = np.nanmean(tracer_conc[idx_omi, :16], axis=0)
    rel_profile_omi = abs_profile_omi/np.sum(abs_profile_omi)
    rel_profile_omi_mod = rel_profile_omi.copy()
    # index 9 of OMI vertical levels correspond to height ~ 5km
    # index 6 of OMI vertical levels correspond to height ~ 2.5km
    rel_profile_omi_mod[:9] = fx_doas(z_centre[:9])
    # if i do not want to glue original profiles
    # rel_profile_omi_mod[9:] = 0
    # get back to absolute profile 
    abs_profile_omi_mod = np.sum(abs_profile_omi)*rel_profile_omi_mod
    # only replace until tropopause which matters
    for j in idx_omi:
        tracer_conc_mod[j, :16] = abs_profile_omi_mod
# calculate corrected subcolumn
tracer_sub_col_mod = tracer_conc_mod*h*100/1E15
f_omi.close()

# %%append corrected OMI relative profiles
f_omi = netCDF4.Dataset(os.path.join(omi_dir, filename_omi), 'r+')
if Inp.tracer == 'no2':
    NO2_mod_out = f_omi.createVariable('no2_sub_col_mod', 'f4', ('time', 'lev'))
    NO2_mod_out[:] = tracer_sub_col_mod
    NO2_mod_out.long_name='modified NO2 a priori sub Column'
    NO2_mod_out.units='molecules m-2'
    NO2_mod_out.ScaleFactor=1E15
else:
    HCHO_mod_out = f_omi.createVariable('hcho_sub_col_mod', 'f4', ('time', 'lev'))
    HCHO_mod_out[:] = tracer_sub_col_mod
    HCHO_mod_out.long_name='modified HCHO a priori sub Column'
    HCHO_mod_out.units='molecules m-2'
f_omi.close()
