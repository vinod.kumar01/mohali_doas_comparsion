# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 23:19:35 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import pandas as pd
import sys
from datetime import date, datetime
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# %% define limits and path
pos = Point(30.667, 76.729)

lat_bound = [30.57, 30.77]
lon_bound = [76.63, 76.83]

dirname = r"M:\DATASETS\www.temis.nl\airpollution\no2col\data\omi\data_v2"
vcd_ts = []
vcd_ts_std = []
vcd_ts_cf = []
cf_ts = []
date_time = []

start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(dirname,
                                        single_date.strftime("%Y\%m\%d"),
                                        'OMI-Aura_L2-OMDOMINO_' + '*.he5'))
    vcd_day = []
    vcd_day_cf = []
    cf_day = []
    for path_sat in f_name_sat:
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        tempdate = path_sat.split('\\')[-1].split('_')[2]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[5:7])
        temp_day = int(tempdate[7:9])
        temp_hour = int(tempdate[10:12])
        temp_min = int(tempdate[12:14])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        daynum = min(temp_dt.timetuple().tm_yday, 365)
        swath = f.groups['HDFEOS'].groups['SWATHS']
        Geoloc_fields = swath.groups['DominoNO2'].groups['Geolocation Fields']
        lats = Geoloc_fields.variables['Latitude'][:]
        lons = Geoloc_fields.variables['Longitude'][:]
        lat_bnds = Geoloc_fields.variables['LatitudeCornerpoints'][:]
        lon_bnds = Geoloc_fields.variables['LongitudeCornerpoints'][:]
        Data_fields = swath.groups['DominoNO2'].groups['Data Fields']
        NO2_tropcol = Data_fields.variables['TroposphericVerticalColumn']
        AMF_trop = Data_fields.variables['AirMassFactorTropospheric']
        AMF_tot = Data_fields.variables['AirMassFactor']
        AK = Data_fields.variables['AveragingKernel']
        cf = Data_fields.variables['CloudFraction'][:]*0.001
#        cf = Data_fields.variables['CloudRadianceFraction'][:]*0.01*0.01
#        flag = Data_fields.variables['MeasurementQualityFlags'][:]
        mv = NO2_tropcol.MissingValue
        fv = NO2_tropcol._FillValue
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])

        # further processing of AMF correction on the relevant orbits
        if len(lats[keep]) > 0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                + ' : ' + str(orbit_num))
            lat_bnds = lat_bnds[:, keep]
            lon_bnds = lon_bnds[:, keep]
            NO2_tropcol = NO2_tropcol[:]
            NO2_tropcol[NO2_tropcol==fv] = np.nan
            NO2_tropcol = NO2_tropcol[keep]
            cf = cf[keep]
            NO2_tropcol_cf = NO2_tropcol.copy()
            NO2_tropcol_cf[(cf >= 0.3)] = np.nan
            for idx in range(lat_bnds.shape[1]):
                # DOMINO has wrong order of points
                polygon = Polygon([(lat_bnds[0][idx], lon_bnds[0][idx]),
                                   (lat_bnds[1][idx], lon_bnds[1][idx]),
                                   (lat_bnds[3][idx], lon_bnds[3][idx]),
                                   (lat_bnds[2][idx], lon_bnds[2][idx])])
                if ~polygon.is_valid:
                    polygon = Polygon([(lat_bnds[0][idx], lon_bnds[0][idx]),
                                       (lat_bnds[1][idx], lon_bnds[1][idx]),
                                       (lat_bnds[2][idx], lon_bnds[2][idx]),
                                       (lat_bnds[3][idx], lon_bnds[3][idx])])                    
                if(polygon.contains(pos)):
                    vcd_day.append(NO2_tropcol[idx])
                    vcd_day_cf.append(NO2_tropcol_cf[idx])
                    cf_day.append(cf[idx])

        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_daymean_cf= np.nanmean(vcd_day_cf)
        vcd_daystd = np.nanstd(vcd_day)
        cf_daymean = np.nanmean(cf_day)
    else:
        vcd_daymean = np.nan
        vcd_daymean_cf = np.nan
        vcd_daystd = np.nan
        cf_daymean = np.nan

    vcd_ts.append(vcd_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf.append(vcd_daymean_cf)
  
    cf_ts.append(cf_daymean)
    date_time.append(single_date.strftime("%Y-%m-%d"))
# %% create time series and save output
omi_ts = np.vstack((date_time, vcd_ts, vcd_ts_std, vcd_ts_cf, cf_ts)).T
np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\no2_OMI_DOM_TS50_coloc.csv", omi_ts,
           delimiter=",",
           header = ('date_time, vcd, vcd_std,'
                     'vcd_cf, cloud_fraction'),
           fmt="%s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log1.txt', 'w')