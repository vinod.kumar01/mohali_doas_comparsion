# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 10:23:03 2019

@author: Vinod
"""
# %% Import and function declaration
import os
import pandas as pd
import matplotlib.pyplot as plt
import calendar
import numpy as np
from datetime import datetime, timedelta
import import_merge_mapa_out as mapa
from mapa_layerheight import calc_layerh
from tools import plot_diurnal
from collections import defaultdict
from pyorbital import astronomy
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
calc_layer_height = False

def interpolate_cloud(ref_time, cloud_inp):

    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
# %% load data
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ concentration (molecules cm$^{-3}$)',
                     5e9, 7e11, 'Chemiluminescence', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO concentration (molecules cm$^{-3}$)',
#                5e9, 5e11, 'PTR-MS', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4', 'Aerosol extinction (km$^{-1}$)', 0, 2.5,
#                     'OMI', 'NASA', 'c_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v3'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')

profile_mean, flag, z = np.array(profile_mean), np.array(flag), np.array(z)
time2 = np.array(time2)
sza = np.array([astronomy.sun_zenith_angle(i, 76.729, 30.667) for i in time2])
daynum = [str(i.astype(datetime).timetuple().tm_yday) for i in time2]

# %% profiles in ppb
with open(r'D:\wat\minimax\temperature_profile.csv') as file:
    next(file)    # ##skip header line
    inp = [line.replace('\n', '').split(',', maxsplit=3) for line in file]
    file.close()
sfc_daytemp = defaultdict(list)
for desttime, day_num, avg_temp, day_temp in inp:
    sfc_daytemp[day_num].append(day_temp)
temp_profile = {}
pressure_profile = {}
for d in range(366):
    temp_profile[d+1] = [float(sfc_daytemp[str(d+1)][0])+273.15+i*(-0.65)
                         if j <= 12000 else 215
                         for i, j in enumerate(z[:25]*1000)]
    pressure_profile[d+1] = (1.01325E5*np.exp((-z[:25]*1000)*temp_profile[d+1]/7400/250))
  
# %% cloud classification data
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')


cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)

# %% Data filter
time2 = pd.to_datetime(time2)
time2 += timedelta(minutes=330)
months = np.array(time2.month)
hours = np.array(time2.hour)
mask = np.empty(profile_mean.shape, dtype=bool)
mask1d = (flag[:] != 0) | (fog == 1) | (OT_cloud == 1)
mask[:, :] = (mask1d)[:, np.newaxis]
profile_mean[mask] = np.nan
profile_mean = profile_mean[:, :25]   # upto 5km
if Inp.tracer == 'hcho':
    keep = (sza > 60)
else:
    keep = (sza > 85)
profile_mean[keep, :] = np.nan
profile_month = {}
for month in list(set(months)):
    profile_month[str(month)] = {}
    for hour in list(set(hours)):
        idx = (hours == hour) & (months == month)
        temp_prof = profile_mean[idx, :]
        # mean profile for a given hour
        profile_month[str(month)][str(hour)] = np.nanmean(temp_prof, axis=0)
        if hour in [5, 18]:
            profile_month[str(month)][str(hour)][:] = np.nan
   

# %% profile in ppb
#profile_ppb = []
#for i, d in enumerate(daynum):
#    profile_ppb_now = profile_mean[i, :]*8.314*temp_profile[int(d)]*1e6
#    profile_ppb_now /= 6.023E23*1E-9*pressure_profile[int(d)]
#    profile_ppb.append(profile_ppb_now)
#profile_ppb = np.array(profile_ppb)

# %% plotting            
fig, ax = plt.subplots(3, 4, figsize=[22, 10], sharex=True, sharey=True)
for i in range(0, 3):
    for j in range(0, 4):
        month = (4*i)+j+1
        month_prof = []
        for hour in range(5, 20):
            month_prof.append(profile_month[str(month)][str(hour)])
        month_prof = np.array(month_prof).T
        if month in [1, 12]:
            month_prof[:, -2:] = np.nan
        pcm = ax[i, j].pcolormesh(np.arange(5, 20), z[:25], month_prof,
                                  cmap='bone_r', vmin=Inp.y_min, vmax=Inp.y_max)  # hot_r/bone_r/pink_r
        ax[i][j].set_ylim(0, 3)
        ax[i][j].set_xlim(5, 19)
        ax[i, j].minorticks_on()
        ax[i, j].text(0.85, 0.9, calendar.month_abbr[(4*i)+j+1],
                      transform=ax[i][j].transAxes, size=16)
        if i == 2:
            ax[i, j].set_xlabel('Local time (UTC+5:30)', size=16)
    ax[i, 0].set_ylabel('Altitude (km)', size=16)
plt.tight_layout()
fig.subplots_adjust(right=0.9)
cbar_ax = fig.add_axes([0.92, 0.15, 0.02, 0.7])
cbar = fig.colorbar(pcm, cax=cbar_ax, extend="max")
cbar.set_label(Inp.spec_str, labelpad=2, size=18)
plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_' +Inp.sf, 'valid',
                         Inp.tracer+'_monthly_profile_valid_v5.png'), format='png', dpi=300)
# %% calculate layer height
if calc_layer_height:
    layer_h = calc_layerh(h,s)
    layer_h[mask1d] = np.nan
#
#def plot_layerh(ax):
#    ax.set_color_cycle(['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
#                        'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])
#    layerh_day = []
#    layerh_sem = []
#    layerh_std = []
#    percentile_25_diff = []
#    percentile_75_diff = []
#    for i in range(1,13):
#        idx = (months==i)
#        layer_h_mnth = [np.nanmean(layer_h[idx][hours[idx]==hour]) for hour in range(5,19)]
#        layerh_mean = np.nanmean(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)])
#        layerh_day.append(layerh_mean)
#        percentile_25 = np.nanpercentile(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)], 25)
#        percentile_25_diff.append(layerh_mean - percentile_25)
#        percentile_75 = np.nanpercentile(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)], 75)
#        percentile_75_diff.append(percentile_75-layerh_mean)      
#        layerh_std.append(np.nanstd(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)]))
#        layerh_sem.append(np.nanstd(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)])/
#                          np.sqrt(np.count_nonzero(~np.isnan(layer_h[idx][(hours[idx] >= 12) &  (hours[idx] <= 14)]))))
#        ax.plot(np.arange(5, 19), layer_h_mnth, label=calendar.month_abbr[i])
#    ax.set_ylabel('Layer height')
#    #ax.set_xlabel('NO$_{2}$ concentration (molecules cm$^{-3}$)')
#    ax.set_xlabel('Local time (UTC+5:30)')
#    ax.legend(loc='upper right', ncol=2)
#    ax.minorticks_on()
#    ax.grid(alpha=0.4)
#    ax.set_xlim(5,19)
#    ax.set_ylim(0, 3)
#    return layerh_day, layerh_std, layerh_sem, percentile_25_diff, percentile_75_diff
#fig,ax = plt.subplots()
#layerh_day, layerh_std, layerh_sem, layerh_25_diff, layerh_75_diff= plot_layerh(ax)
#plt.tight_layout()
# %% plot diurnal profiles
#layerh_merge = pd.read_csv(r'D:\postdoc\DOAS\Mohali_2013_17\sf_0.8\layerh_v5.csv')
#layerh_merge['Date_time'] = pd.to_datetime(layerh_merge['Date_time'],
#            format='%Y-%m-%d %H:%M:%S')
#seasons = {'Winter': [11, 12, 1, 2], 'Summer': [3, 4, 5, 6],
#           'Monsoon': [7, 8], 'Post-Monsoon': [9, 10]}
#color = ['slategrey', 'red', 'blue', 'green']
#fig, ax = plt.subplots(figsize=[8, 4.5])
#for c, season in zip(color, seasons):
#    keep = (layerh_merge['Date_time'].dt.month.isin(seasons[season]))
#    if Inp.tracer == 'hcho':
#        keep &= (sza < 60)
#    else:
#        keep &= (sza < 85)
#    df_sel = layerh_merge[keep]
#    a = plot_diurnal(df_sel['layerh_'+Inp.tracer], df_sel['Date_time'], ax=ax,
#                     label=season, color=c)
#ax.set_ylabel('Profile height (Km)')
#ax.set_xlabel('Local time (UTC+5:30)')
#ax.set_xlim(a[1].index[6], a[1].index[-4])
#ax.annotate(Inp.spec_str.split(' ')[0], xy=(0.85, 0.9),
#            xycoords='axes fraction')
#ax.grid(alpha=0.3)
#plt.tight_layout()

# %% profile in ppb
#def plot_ppbprofile(idx):
#    if flag[idx] != 0:
#        print('not a valid profile')
#    else:
#        fig,ax = plt.subplots()
#        ax.plot(profile_mean[idx, :], z[:25], '-o', c='r', label='conc_pofile')
#        ax.plot(np.nan, np.nan, '-o', c='k', label='vmr_pofile')
#        ax2 = ax.twiny()
#        ax2.plot(profile_ppb[idx, :], z[:25], '-o', c='k', label='vmr_pofile')
#        ax.set_xlabel('NO$_2$ concentration (molecules cm$^{-2}$)')
#        ax2.set_xlabel('NO$_2$ VMR (ppb')
#        ax.set_ylim(0, 3)
#        ax.set_ylabel('Altitude (km)')
#        ax.annotate(time2[idx].strftime('%d-%m-%Y %H:%M'), xy=(0.3, 0.9),
#                    xycoords='axes fraction')
#        ax.legend(loc='upper right')
#        ax.grid(alpha=0.3)
#        plt.tight_layout()
#        return(fig)
