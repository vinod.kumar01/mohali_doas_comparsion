# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 21:46:23 2019

@author: Vinod
"""
from datetime import datetime, timedelta


def doy_2_datetime(doy, year):
    '''
    It converts fractional day into datetime
    '''
    year = datetime(year, 1, 1)
    dt = timedelta(days=doy-1)
    #  1 Jan is 1 and not 0
    calander_day = year + dt
    return calander_day


def datetime_2_doy(date):
    '''
    It converts datetime into day of year
    the input datetime should be in format datetime.datetime(yyyy, mm, dd)
    or datetime.datetime(yyyy, mm, dd, hh, mm, ss)
    '''
    detail_time = date.timetuple()
    doy = detail_time.tm_yday
    doy += detail_time.tm_hour/24 + detail_time.tm_min/24/60
    doy += detail_time.tm_sec/24/60/60
    return doy
