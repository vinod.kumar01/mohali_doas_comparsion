# -*- coding: utf-8 -*-

'''
Created on 12-Jul-2019

@author: Vinod
'''

import os
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import import_merge_mapa_out as mapa
from scipy import stats
from decimal import Decimal
import seaborn as sns
from pyorbital import astronomy
from mapa_layerheight import calc_layerh
from lin_ODR import orthoregress as odr
#from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from mod_colormap import add_white

def fitFunc(x, A0, A, mu, sigma):
    return A0 + A*np.exp(-((x-mu)**2/(2.*sigma**2)))


def color_flag(flag):
    if flag == 0:
        color = 'g'
    elif flag == 1:
        color = "orange"
    else:
        color = "r"
    return color


def plot_sf(date_time, scaling_fac):
    fig, ax = plt.subplots()
    ax.scatter(date_time, scaling_fac, s=1, c=color)
    ax.axhline(y=1, c='k', ls='-.')
    ax.axhline(y=0.8, c='k', ls='--')
    ax.set_ylim(0, 5)
    ax.set_ylabel('O4 scaling factor')
    ax.set_xlim(pd.to_datetime('12-09-2013'), pd.to_datetime('06-15-2017'))
    plt.show()


def interpolate_ts(ts, datetime_index):
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    return x.groupby(x.index).first().sort_index().fillna(
        method="ffill", limit=5)[datetime_index]


def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
# %% load DOAS Data

mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath=r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
calc_layer_height = False
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ mixing ratio (ppb)',
                     0.01, 50, 'Chemiluminescence', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO mixing ratio (ppb)',
#                0.01, 40, 'PTR-MS', 'IISERM', 'vcd_weighted_mean', sf='0.8')
#Inp = mapa.load_plot('aod', 'o4', 'AOD', 0, 2.5, 'MODIS', 'MAIAC', 'c_weighted_mean', sf='0.8')
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath,
                                                          mapa_outpath)

profile_mean, flag = np.array(profile_mean), np.array(flag)
h, vcd_mean, time2 = np.array(h), np.array(vcd_mean), np.array(time2)
sza = np.array([astronomy.sun_zenith_angle(i, 76.729, 30.667) for i in time2])
s = np.array(s)
color = [color_flag(i) for i in flag]
mask = np.empty(profile_mean.shape, dtype=bool)
mask[:, :] = (flag[:] == 2)[:, np.newaxis]
# profile_mean = np.ma.MaskedArray(profile_mean, mask=mask)
profile_mean[mask] = np.nan  # masked values have problem in pandas
h[flag[:] == 2] = np.nan
s[flag[:]== 2] = np.nan
'''
find layer height
Find the cumulative sum of box VCD below layers
Find a fit function for each sequence
height at which cumulative sum >=80% of total vcd
'''

#method 1
if calc_layer_height:
    layer_h = calc_layerh(h,s)
else:
    layer_h = h
#    for i in range(len(vcd_mean)):
#    vcd_cum = profile_mean*200*100
#    for lev in range(25):
#        if lev != 0:
#            vcd_cum[:, lev] += vcd_cum[:, lev-1]
#    for i in range(len(vcd_mean)):
#        try:
#            fx = interp1d(vcd_cum[i, :25], z[:25])
#            layer_h.append(fx(vcd_mean[i]*0.8))
#        except ValueError:
#            layer_h.append(h[i])

# # plot_sf(time2, sf)   #plot scaling factor

'''
flag 0 = good
flag 1 = warning
flag 2 = error
'''
'''
surface concentration
'''
conc_surf = profile_mean[:, 0]
if Inp.tracer == 'hcho':
    conc_surf[sza > 60] = np.nan

time3 = time2.astype(datetime)
# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
flag_remove = (OT_cloud==1) | (fog==1)
mask = np.empty(profile_mean.shape, dtype=bool)
mask[:, :] = (flag_remove)[:, np.newaxis]
profile_mean[mask] = np.nan
conc_surf[flag_remove] = np.nan
layer_h[flag_remove] = np.nan

df2 = pd.DataFrame(np.column_stack((time3, conc_surf, vcd_mean, layer_h, s, flag)),
                   columns=['Date_time', 'conc_surface',
                            'vcd_mean', 'layer_height', 'shape_parameter', 'flag'])
df2['Date_time'] = pd.to_datetime(df2['Date_time'],
                                  format='%Y-%m-%d %H:%M:%S')
for cols in df2.columns[1:]:
    df2[cols] = pd.to_numeric(df2[cols])
# df2['conc_surface'] = pd.to_numeric(df2['conc_surface'])
# df2['vcd_mean'] = pd.to_numeric(df2['vcd_mean'])
# df2['flag'] = pd.to_numeric(df2['flag'])
df2 = df2.set_index('Date_time')

# %% load insitu data
insitu_path = r'D:\postdoc\DOAS\Mohali_2013_17'
insitu_file = os.path.join(insitu_path, 'in_situ.csv')
colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
            'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
            'abs_humidity', 'hcho']
insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=colnames)
insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                         format='%d-%m-%Y %H:%M:%S')
insitu_data['desttime'] -= timedelta(minutes=330)  # offset for IST
for cols in colnames[1:]:
    insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')
insitu_data = insitu_data.set_index('desttime')

df2[Inp.tracer+'_insitu'] = interpolate_ts(insitu_data[Inp.tracer], df2.index)
df2['ws'] = interpolate_ts(insitu_data['Wind_Speed'], df2.index)
df2['wd'] = interpolate_ts(insitu_data['Wind_Direction'], df2.index)
df2['amb_temp'] = interpolate_ts(insitu_data['Amb_temp'], df2.index)
df2['o3'] = interpolate_ts(insitu_data['o3'], df2.index)
df2[Inp.tracer+'_doasmr'] = df2['conc_surface']*(8.314*(df2['amb_temp']+273.16)*1E6)
df2[Inp.tracer+'_doasmr'] /= 6.023E23*1E-9*97000

data_filter = (df2['wd']<=360)    #(df2['wd']>340)|(df2['wd']<10)
df2_dm = df2[data_filter].resample('D').mean()    # to resample
df2_ds = df2[data_filter].resample('D').std()    # to resample

df2_dm.reset_index(inplace=True)
df2_ds.reset_index(inplace=True)
df2_dm['month'] = df2_dm['Date_time'].dt.month
#df2 = df2.reset_index()
#dt_intp = [d.to_pydatetime() for d in df2.index]

# %% time series
'''
Surface concentration
'''
fig, ax = plt.subplots(figsize=[11, 5])

ax.scatter(df2.index, df2[Inp.tracer+'_doasmr'], c=color, s=2, alpha=0.1,
           label='MAX-DOAS')
ax.scatter(df2.index, df2[Inp.tracer+'_insitu'], c='k', s=2, alpha=0.1,
           label=Inp.inst, marker="v")
ax.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_doasmr'],
        c='r', label='MAX-DOAS (daily mean)')
ax.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_insitu'],
        c='k', label=Inp.inst+' (daily mean)')

# ax.errorbar(df2_dm['Date_time'], df2_dm['conc_surface'],
#             df2_ds['conc_surface'], c='g', label='MAX-DOAS (daily)')
# ax.errorbar(df2_dm['Date_time'], df2_dm[spec+'_insitu'],
#             df2_ds[spec+'_insitu'], c='b', label='MAX-DOAS (daily)')
ax.set_ylim(Inp.y_min, Inp.y_max)
ax.set_ylabel("Surface " + Inp.spec_str, fontsize=10)
plt.legend(loc='upper right', markerscale=2.5)
#ax.set_xlabel("Date and Time (UTC)", fontsize=10)
ax.set_xlim(pd.to_datetime('12-09-2012'), pd.to_datetime('06-15-2017'))
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
ax.annotate('A)', xy=(0.05, 0.9), xycoords="axes fraction")
plt.tight_layout()
# add inset of monthly variability
df2['month'] = [d.strftime('%b') for d in df2.index]
order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
ax_in = ax.inset_axes((0.25, 0.68, .36, .3))
sns.boxplot(x='month', y=Inp.tracer+'_doasmr', data=df2, color='r',
            showfliers=False, whis=[5, 95],
            showmeans=True, order=order, ax=ax_in)
ax_in.set_xlabel('')
ax_in.set_ylabel(Inp.spec_str.replace('mixing ratio', 'VMR'))
ax_in.grid(alpha=0.3)
ax_in.annotate("MAX-DOAS", xy=(0.05, 0.8), xycoords='axes fraction')
num_artists = len(ax_in.artists)
num_lines = len(ax_in.lines)
lines_per_artist = num_lines // num_artists
for i, artist in enumerate(ax_in.artists):
    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
    col = artist.get_facecolor()
    artist.set_edgecolor(col)
    artist.set_facecolor('None')
    # set the marker colors of the corresponding "lines" to the same color
    for j in range(lines_per_artist):
        ax_in.lines[i * lines_per_artist + j].set_markerfacecolor(col)
        ax_in.lines[i * lines_per_artist + j].set_markeredgecolor(col)
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 'valid',
#                         Inp.mapa_spec+'_insitu_vis_warningv5_inset.png'), format='png', dpi=300)
#
#%% Plot time series with gap as cross
#fig, (ax1, ax2) = plt.subplots(1, 2, sharey=True, figsize=[11, 5],
#                               gridspec_kw={'width_ratios': [4, 1]})
#ax1.scatter(df2.index, df2[Inp.tracer+'_doasmr'], c=color, s=2, alpha=0.1,
#           label='MAX-DOAS')
#ax1.scatter(df2.index, df2[Inp.tracer+'_insitu'], c='k', s=2, alpha=0.1,
#           label=Inp.inst, marker="v")
#ax1.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_doasmr'],
#        c='r', label='MAX-DOAS (daily mean)')
#ax1.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_insitu'],
#        c='k', label=Inp.inst+' (daily mean)')
#ax2.scatter(df2.index, df2[Inp.tracer+'_doasmr'], c=color, s=2, alpha=0.1,
#           label='MAX-DOAS')
#ax2.scatter(df2.index, df2[Inp.tracer+'_insitu'], c='k', s=2, alpha=0.1,
#           label=Inp.inst, marker="v")
#ax2.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_doasmr'],
#        c='r', label='MAX-DOAS (daily mean)')
#ax2.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_insitu'],
#        c='k', label=Inp.inst+' (daily mean)')
#ax1.set_ylim(Inp.y_min, Inp.y_max)
#ax1.set_xlim(pd.to_datetime('12-09-2012'), pd.to_datetime('01-15-2016'))
#ax2.set_xlim(pd.to_datetime('10-01-2016'), pd.to_datetime('06-15-2017'))
#ax1.spines['right'].set_visible(False)
#ax2.spines['left'].set_visible(False)
#ax2.yaxis.tick_right()
#ax1.yaxis.tick_left()
#ax1.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
#ax1.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
#ax1.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
#ax2.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
#ax2.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
#ax2.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
#ax1.set_ylabel("Surface " + Inp.spec_str, fontsize=10)
#ax1.annotate('A)', xy=(0.05, 0.9), xycoords="axes fraction")
#kwargs = dict(transform=ax1.transAxes, color='k', clip_on=False)
#d = 0.015
#ax1.plot((1-d,1+d), (-d,+d), **kwargs)
#ax1.plot((1-d,1+d),(1-d,1+d), **kwargs)
#ax1.plot((1.02-d,1.02+d), (-d,+d), **kwargs)
#ax1.plot((1.02-d,1.02+d),(1-d,1+d), **kwargs)
#plt.tight_layout(w_pad=0.3)
#ax2.legend(loc='upper right', markerscale=2.5)
# %% Scatter plot
def add_fit_param(fit_param, pos_x, pos_y, ax='ax', color='k', showr=True):
    corr_coeff = "\n r = " + str('%.2f' % fit_param[2]) if showr else ''
    if fit_param[1] >= 0:
       ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x + " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color = color)
    else:
      ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x - " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color = color)

fig, ax = plt.subplots()
scat1 = ax.scatter(x=Inp.tracer+'_insitu', y=Inp.tracer+'_doasmr', data=df2,
                   s=1, c='k', label='Individual measurements', alpha=0.1)
scat = ax.scatter(x=Inp.tracer+'_insitu', y=Inp.tracer+'_doasmr', data=df2_dm,
                  s=4, c='layer_height', label='Daily mean', cmap='jet', vmax=1.5)
cb=plt.colorbar(scat, ax=ax, extend='max')
ax.set_ylim(Inp.y_min, Inp.y_max)
ax.set_xlim(Inp.y_min, Inp.y_max)
idx = np.isfinite(df2_dm[Inp.tracer+'_insitu']) & np.isfinite(df2_dm[Inp.tracer+'_doasmr'])
fit_param = stats.linregress(df2_dm[Inp.tracer+'_insitu'][idx], df2_dm[Inp.tracer+'_doasmr'][idx])[0:3]
fit_func = np.poly1d(fit_param[0:2])
ax.plot(df2_dm[Inp.tracer+'_insitu'][idx], fit_func(df2_dm[Inp.tracer+'_insitu'][idx]),
        color="b", ls="--", linewidth=1, label='linear fit (Daily mean)')
idx &= np.isfinite(df2_ds[Inp.tracer+'_insitu']) & np.isfinite(df2_ds[Inp.tracer+'_doasmr'])
idx &= (df2_dm[Inp.tracer+'_insitu'] != 0) & (df2_dm[Inp.tracer+'_doasmr']!=0)
fit_param_odr = odr(df2_dm[Inp.tracer+'_insitu'][idx],
                    df2_dm[Inp.tracer+'_doasmr'][idx],
                    df2_ds[Inp.tracer+'_insitu'][idx],
                    df2_ds[Inp.tracer+'_doasmr'][idx])[0:3]
fit_func_odr = np.poly1d(fit_param_odr[0:2])
ax.plot(df2_dm[Inp.tracer+'_insitu'][idx], fit_func_odr(df2_dm[Inp.tracer+'_insitu'][idx]),
        color="r", ls="--", linewidth=1, label='ODR fit (Daily mean)')

ax.set_xlabel(Inp.spec_str+' ('+Inp.inst+')')
ax.set_ylabel(Inp.spec_str+' (MAX-DOAS)')
ax.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c=".3", label='1:1 line')
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[::-1], labels[::-1], loc='upper right')
ax.minorticks_on()
add_fit_param(fit_param, 0.05, 0.8, ax=ax, color='b', showr=True)
add_fit_param(fit_param_odr, 0.05, 0.75, ax=ax, color='r', showr=False)
ax.annotate('C)', xy=(0.05, 0.9), xycoords="axes fraction")
plt.tight_layout()
cb.set_label('Profile height (H$_{75}$) (Km)')
#
##ticks = np.arange(0,13,1)
##cb.set_ticks(ticks)
##labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
##          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
##cb.set_ticklabels(labels)
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 'valid',
#                         Inp.tracer+'_insitu_scatter_vis_layer_height_v5_odr.png'), format='png', dpi=300)
# %% histogram with bias
#bias = df2.resample('H').mean()[Inp.tracer+'_doasmr'] - df2.resample('H').mean()[Inp.tracer+'_insitu']
bias = df2[Inp.tracer+'_doasmr'] - df2[Inp.tracer+'_insitu']
n_bins = np.arange(-20,20,0.5)
count, bins = np.histogram(bias[~bias.isnull()], n_bins)
#count = count/np.sum(count) for relative frequency
fig,ax = plt.subplots()
ax.bar(bins[:-1], count, width=0.4, label='BIAS (DOAS-in situ)(ppb)')
ax.axvline(x=0, alpha=0.5, c='r')
ax.set_xlim(-20, 20)
ax.set_xlabel('BIAS (MAX-DOAS $\u2212$ in situ) in '+Inp.spec_str)
ax.set_ylabel('Frequency', size=12)
ax.minorticks_on()
ax.annotate('B)', xy=(0.03, 0.9), xycoords="axes fraction")
plt.tight_layout()
p0 = [0, np.max(count), n_bins[np.argmax(count)], 0.2]
fitParams, fitCovariances = curve_fit(fitFunc, bins[:-1], count, p0)
FWHM = 2*np.sqrt(2*np.log(2))*fitParams[3]
fit_line = fitFunc(bins[:-1], fitParams[0], fitParams[1],
                       fitParams[2], fitParams[3])
#ax.plot(bins[:-1], fit_line, '-', color='r', label='Gauss fit')
ax.annotate("Mean bias = " + str('%.2f' % fitParams[2]) + ' '+ chr(177)+' ' + 
            str('%.2f' % (FWHM/2)) + ' ppb',
            xy=(0.03, 0.8), xycoords='axes fraction', color='r')
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, 'valid',
#                         Inp.tracer+'_insitu_biasv5_hist.png'), format='png', dpi=300)
#
## %%
##doas_mean_polar = []
##insitu_mean_polar = []
##for i in np.arange(0, 360, 5):
##    fil = (df2['wd']>i) & (df2['wd']<=i+5)
##    doas_mean_polar.append(df2[fil][Inp.tracer+'_doasmr'].mean())
##    insitu_mean_polar.append(df2[fil][Inp.tracer+'_insitu'].mean())
##    
##fig,ax = plt.subplots(subplot_kw=dict(polar=True))
##ax.set_theta_direction(-1)
##ax.set_theta_zero_location('N')
##scat = ax.plot(np.arange(0, 360, 5)*np.pi/180, doas_mean_polar, label='MAX DOAS')
##scat = ax.plot(np.arange(0, 360, 5)*np.pi/180, insitu_mean_polar, label = 'in situ')
##ax.legend(loc = [0.95, 0.9])
##ax.set_ylim(0, 20)

# %% hexbin plot 
# MAX-DOAS /in situ no2 vs in situ no2
new_cm = add_white('Reds', shrink_colormap=False, replace_frac=0.02)
df2_dm['rat'] = df2_dm['no2_doasmr']/df2_dm['no2_insitu']
fig,ax = plt.subplots(1, 2, figsize=[9, 4], sharex=True, sharey=True)
scat0 = ax[0].hexbin(x=Inp.tracer+'_insitu', y='rat',
          data=df2_dm[df2_dm['Date_time'].dt.year.isin([2012, 2013, 2014])],
                  gridsize=(30,150), cmap=new_cm)
scat1 = ax[1].hexbin(x=Inp.tracer+'_insitu', y='rat',
          data=df2_dm[df2_dm['Date_time'].dt.year.isin([2015, 2016, 2017])],
          cmap=new_cm,  gridsize=(25,30))
ax[0].set_ylim(0, 5)
ax[0].set_xlim(0, 20)
ax[0].grid(alpha=0.3)
ax[1].grid(alpha=0.3)
ax[0].set_ylabel('MAX-DOAS NO$_2$ VMR/in situ NO$_2$ VMR')
ax[0].set_xlabel('in situ NO$_2$ VMR (ppb)')
ax[1].set_xlabel('in situ NO$_2$ VMR (ppb)')
plt.tight_layout()             
fig.subplots_adjust(right=0.9)
cbar_ax=fig.add_axes([0.92, 0.2, 0.02, 0.7])
cbar = fig.colorbar(scat0, cax=cbar_ax, extend='max')
cbar.set_label('Frequency', size=12)
ax[0].annotate("2013-2014", xy=(0.8, 0.9), xycoords='axes fraction')
ax[1].annotate("2015-2017", xy=(0.8, 0.9), xycoords='axes fraction')