# -*- coding: utf-8 -*-
"""
Created on Mon Jul 20 22:25:08 2020

@author: Vinod
with inputs from Lukas
"""
import numpy as np
import os
import netCDF4
from datetime import datetime
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import timedelta


def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])


def sichtweite(sza, o4dscd, rsaa=None, saa=None, vaa=None,
               o4vcd=1.19e43, wavelength='360nm'):
    """
    sza: solar zenith angle
    saa: solar azimuth angle
    vaa: viewing azimuth angle
    o4dscd: some measured o4 differential slant column
    (derived against the zenith spectrum)
    o4vcd: some typical o4 vertical column. Required to derive the O4 DAMF.
    """
    coefficients = {'360nm': np.array([-14.7437902804, 9.5812447052,
                                       -0.1333431158, -0.0647288802,
                                       14.7387915017, -1.9958988210,
                                       -3.2608469084, 0.2435139358,
                                       52.6824217963, -56.3743701223,
                                       10.1223233578, -0.3292049248,
                                       -69.6317773272, 60.1047965938,
                                       -5.9367673327, 3.6365734829]),
                    '477nm': np.array([-3.6952550619, 4.8415393893,
                                       -0.5232165487, 0.0620563212,
                                       -5.2573454376, 3.7371879081,
                                       -1.4816115658, -0.1143135161,
                                       45.6539834835, -46.0418147681,
                                       6.8455156328, -0.0419364356,
                                       -52.7605258737, 51.6757893115,
                                       -6.2183885565, 3.7890129549])}

    polycoeffs = coefficients[wavelength]
    x = sza/100
    if rsaa is None:
        rsaa = vaa - saa
        while rsaa > 180:
            rsaa -= 360
        while rsaa < -180:
            rsaa += 360
        rsaa = abs(rsaa)
    y = rsaa/100
    damf = o4dscd/o4vcd
    xpots = np.array([3, 2, 1, 0, 3, 2, 1, 0, 3, 2, 1, 0, 3, 2, 1, 0])
    ypots = np.array([3, 3, 3, 3, 2, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0])
    polynom = sum(polycoeffs * (x**xpots) * (y**ypots))
    return polynom*damf


def get_mapa_inp(mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'):
    dscd, sza, raa, o4vcd, time2 = [], [], [], [], []
    for nc_dir in ['analysis_2012_1_auto', 'analysis_2012_2_auto',
                   'analysis_2013_1_auto', 'analysis_2013_2_auto',
                   'analysis_2013_3_auto', 'analysis_2013_4_auto',
                   'analysis_2014_1_auto', 'analysis_2014_2_auto',
                   'analysis_2015_1_auto', 'analysis_2015_2_auto',
                   'analysis_2015_3_auto', 'analysis_2015_4_auto',
                   'analysis_2015_5_auto', 'analysis_2015_6_auto',
                   'analysis_2016_1_auto', 'analysis_2016_2_auto',
                   'analysis_2017_1_auto']:
        mapa_infile = os.path.join(mapa_inpath, nc_dir+".nc")
        datafile = netCDF4.Dataset(mapa_infile, 'r')
        eas = datafile.variables['ea'][:]
        tracer_dscd = datafile.variables['o4_dscd'][:]
        o4_vcd_file = datafile.variables['o4_vcd'][:]
        sza_file = datafile.variables['sza'][:]
        raa_file = datafile.variables['raa'][:]
        dat = datafile.variables['time'][:]
        timegrid = np.max(dat, axis=1)
        time2.extend(np.array(
            [np.datetime64(datetime.strptime(str(int(t)), "%Y%m%d%H%M%S"))
             for t in timegrid]))
        dscd.extend(tracer_dscd)
        sza.extend(sza_file)
        raa.extend(raa_file)
        o4vcd.extend(o4_vcd_file)
        print(nc_dir)
        outdict = {'dscd': np.array(dscd), 'sza': np.array(sza),
                   'raa': np.array(raa), 'datetime': np.array(time2),
                   'eas': np.array(eas), 'o4vcd': np.array(o4vcd)}
    return outdict


# %% load data and calculate sensitivity
data = get_mapa_inp()
df = pd.DataFrame(data={'Date_time': data['datetime'],
                        'sza': data['sza'][:, 0],
                        'raa': data['raa'][:, 0],
                        'o4dscd': data['dscd'][:, 0],
                        'o4vcd': data['o4vcd'][:, 0]})
# sichtweite is defined for raa less than 180
df['raa'][df['raa']>180] = 360-df['raa']
df = df.set_index('Date_time')
df['o4dscd'][df['o4dscd'] < 0] = np.nan
df['hor_sensitivity'] = df.apply(lambda x: sichtweite(x['sza'], x['o4dscd'],
                                 rsaa=x['raa'], o4vcd=x['o4vcd']), axis=1)
df['hor_sensitivity'][df['sza'] > 70] = np.nan

# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'],
         format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(data['datetime'], cloud_dat)
df['cloud_type'] = cloud_type
df['OT_cloud'] = OT_cloud
df['fog'] = fog
df['hor_sensitivity'][df['fog'] == 1] = np.nan
df['hor_sensitivity'][df['OT_cloud'] == 1] = np.nan
df = df.set_index(df.index+timedelta(minutes=330))
# %% plotting
df['month'] = [d.strftime('%b') for d in df.index]
order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
fig, ax = plt.subplots(figsize=[10, 4])
sns.boxplot(x='month', y='hor_sensitivity', hue='cloud_type',
            data=df[(df['cloud_type'].isin([1, 2])) &
                    (df.index.hour.isin([12, 13, 14]))],
            showfliers=False, whis=[5, 95],
            showmeans=True, order=order)
L = plt.legend(loc='upper right')
L.get_texts()[0].set_text('Clear sky\nlow aerosol')
L.get_texts()[1].set_text('Clear sky\nhigh aerosol')
ax.set_ylabel('Horizontal sensitivity at 2$^\circ$ elevation (km)')
ax.set_xlabel('')
plt.tight_layout()
ax.grid(alpha=0.3)
