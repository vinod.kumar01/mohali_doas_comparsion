# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 01:02:01 2019
script to calculate layer height if h and s are provided
We can define a threshold for definition of layer height
Height below which threshold of the total column is located.

@author: Vinod
"""
import numpy as np
#from scipy.interpolate import interp1d
#import matplotlib.pyplot as plt

def exp_dec(z, h, s):
    if z <= h:
        return s/h
    else:
        return (s/h)* np.exp(-((z-h)/h)*(s/(1-s)))


def calc_layerh(h, s, threshold=0.75):
    layer_height = []
    h = np.asarray([h]) if np.isscalar(h) else np.asarray(h)
    s = np.asarray([s]) if np.isscalar(s) else np.asarray(s)
    for h_now,s_now in zip(h,s):
        z = np.linspace(0, 4, 1000)
        dz = z[1]-z[0]
        rho = np.zeros([len(z)])
        frac = np.zeros([len(z)])
        if s_now<1:
            for i in range(1000):
                rho[i] = exp_dec(z[i],h_now,s_now)
                frac[i] = frac[i-1]+(rho[i]*dz) if i>0 else frac[i]+(rho[i]*dz)
        elif s_now==1:
            for i in range(1000):
                rho[i] = s_now/h_now if z[i]<=h_now else 0
                frac[i] = frac[i-1]+(rho[i]*dz) if i>0 else frac[i]+(rho[i]*dz)
        else:
            for i in range(1000):
                h1 = h_now*(s_now-1)
                h2 = h_now*(2-s_now)
                rho[i] = s_now/h2 if h1<z[i]<=h_now else 0
                frac[i] = frac[i-1]+(rho[i]*dz) if i>0 else frac[i]+(rho[i]*dz)
        try:
            if np.isnan(h_now) | np.isnan(s_now):
                layer_height.append(np.nan)
            else:
#                fx = interp1d(frac, z)
#                layer_height.append(fx(0.8))
                layer_height.append(z[np.searchsorted(frac, threshold)])
        except IndexError:
            layer_height.append(4.0)
    return np.array(layer_height)
#    ax.plot(rho, z, label=str(h))
    
        
