# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 19:40:53 2020

@author: Vinod
"""

import pandas as pd
import os
dirname = r'D:\wat\minimax\Mohali_2013\corr_spectra\temp3\results'
refname = 'analysis_2013_3_auto.ASC'
target_name = 'analysis_2013_3_auto_new.ASC'


ref_data = pd.read_csv(os.path.join(dirname, refname), dtype=None, delimiter='\t',
                                header=0)
ref_data = ref_data.iloc[:, :-1]
ref_data['Date_time'] = ref_data['Date (DD/MM/YYYY)'] + ' ' + ref_data['Time (hh:mm:ss)']
ref_data['Date_time'] = pd.to_datetime(ref_data['Date_time'],
                                   format='%d/%m/%Y %H:%M:%S')
ref_data = ref_data.set_index('Date_time')

target_data = pd.read_csv(os.path.join(dirname, target_name), dtype=None, delimiter='\t',
                                header=0)
target_data = target_data.iloc[:, :-1]
target_data['Date_time'] = target_data['Date (DD/MM/YYYY)'] + ' ' + target_data['Time (hh:mm:ss)']
target_data['Date_time'] = pd.to_datetime(target_data['Date_time'],
                                   format='%d/%m/%Y %H:%M:%S')
target_data = target_data.set_index('Date_time')
target_data = target_data.loc[ref_data.index]
target_data['Date & time (YYYYMMDDhhmmss)'] = ref_data['Date & time (YYYYMMDDhhmmss)']
target_data['Solar Azimuth Angle'] = ref_data['Solar Azimuth Angle']
target_data['SZA'] = ref_data['SZA']
target_data['Name'] = ref_data['Name']
target_data['Year'] = ref_data['Year']
target_data['Fractional day'] = ref_data['Fractional day']

# choose which fields to keep
target_data['O4.SlCol(O4)'] = ref_data['O4.SlCol(O4)']
target_data['O4.SlErr(O4)'] = ref_data['O4.SlErr(O4)']
target_data['NO2_UV.SlCol(NO2)'] = ref_data['NO2_UV.SlCol(NO2)']
target_data['NO2_UV.SlErr(NO2)'] = ref_data['NO2_UV.SlErr(NO2)']
target_data['HCHO.SlCol(HCHO)'] = ref_data['HCHO.SlCol(HCHO)']
target_data['HCHO.SlErr(HCHO)'] = ref_data['HCHO.SlErr(HCHO)']

target_data.to_csv(os.path.join(dirname, target_name), sep='\t', index=False)


