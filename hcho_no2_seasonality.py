# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 14:55:13 2020

@author: Vinod
"""
import seaborn as sns
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import import_merge_mapa_out as mapa
from pyorbital import astronomy
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
#%% Load NO2 data 
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
                    -0.25e16, 2e16, 'OMI', 'OMNO2', 'vcd_weighted_mean', 1e15, sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
#time2 = np.array(time2)
time2 = pd.to_datetime(time2)
sza = np.array([astronomy.sun_zenith_angle(i, 76.729, 30.667) for i in time2])
# vcd_mean = np.ma.masked_where(flag == 2, vcd_mean)
# has issues while converting into dataframe
vcd_mean[flag == 2] = np.nan
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
vcd_mean[OT_cloud == 1] = np.nan
vcd_mean[fog == 1] = np.nan
#vcd_mean[cloud_type>=3] = np.nan
#idx_sat = ((time2.hour >= 4) & (time2.hour < 6))
#idx_sat = ((time2.hour >= 7) & (time2.hour < 9))
idx_sat = ((time2.hour >= 10) & (time2.hour < 12))

time2 = time2[idx_sat]
vcd_mean = vcd_mean[idx_sat]
sza = sza[idx_sat]
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64'),
                        sza.astype('float64')]).T
df.columns = ['Date_time', Inp.tracer+'_VCD', 'sza']
df['Date_time'] = pd.to_datetime(df['Date_time'])
df[Inp.tracer+'_VCD'] = pd.to_numeric(df[Inp.tracer+'_VCD'], errors='coerce')
df['sza'] = pd.to_numeric(df['sza'], errors='coerce')
df_day = df.copy()


#%% load Hcho data
Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
                     -0.25e16, 6e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'

vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
#time2 = np.array(time2)
time2 = pd.to_datetime(time2)
# vcd_mean = np.ma.masked_where(flag == 2, vcd_mean)
# has issues while converting into dataframe
vcd_mean[flag == 2] = np.nan
vcd_mean[OT_cloud==1] = np.nan
vcd_mean[fog==1] = np.nan
#vcd_mean[cloud_type>=3] = np.nan
time2 = time2[idx_sat]
vcd_mean = vcd_mean[idx_sat]
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64')]).T
df.columns = ['Date_time', Inp.tracer+'_VCD']
df['Date_time'] = pd.to_datetime(df['Date_time'])
df[Inp.tracer+'_VCD'] = pd.to_numeric(df[Inp.tracer+'_VCD'], errors='coerce')
df_day['hcho_vcd']= df['hcho_VCD']
df_day['rat'] = df_day['hcho_vcd']/df_day['no2_VCD']
df_day['month'] = [d.strftime('%b') for d in df_day['Date_time']]


# %%Plot

#fig, axes = plt.subplots(figsize=[9,4])
#order = ['Jan','Feb','Mar','Apr', 'May','Jun','Jul','Aug','Sep', 'Oct', 'Nov', 'Dec' ]
#sns.boxplot(x='month', y='rat', data=df_day, showfliers = False, whis=[5, 95],
#            color="w", showmeans=True, order=order)
#axes.set_ylabel('HCHO VCD/NO$_{2}$ VCD')
#axes.set_xlabel('')
#plt.tight_layout()
#axes.grid(axis="y", alpha=0.4)
##plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf, 'Overlap',
##                         'HCHO_NO2_DOASv5.png'),
##                         format='png', dpi=300)
#
## %% morning, afternoon and evening together
#df_morning['kind']='09:30-11:30'
#df_noon['kind']='12:30-14:30'
#df_evening['kind']='15:30-17:30'
#df_merge = pd.concat((df_morning, df_noon, df_evening), sort=False)
#order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
#         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
#fig, axes = plt.subplots(figsize=[9,4])
#
#sns.boxplot(x='month', y='rat', hue='kind', showfliers = False, whis=[5, 95],
#            data=df_merge, palette=sns.color_palette(('r', 'k', 'b')),
#            showmeans=True, order=order)
#num_artists = len(axes.artists)
#num_lines = len(axes.lines)
#lines_per_artist = num_lines // num_artists
#for i, artist in enumerate(axes.artists):
#    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
#    col = artist.get_facecolor()
#    artist.set_edgecolor(col)
#    artist.set_facecolor('None')
#    # set the marker colors of the corresponding "lines" to the same color
#    for j in range(lines_per_artist):
#        axes.lines[i * lines_per_artist + j].set_markerfacecolor(col)
#        axes.lines[i * lines_per_artist + j].set_markeredgecolor(col)
#axes.set_ylabel('HCHO VCD/NO$_{2}$ VCD', size=12)
#axes.set_xlabel('')
#axes.grid(axis="y", alpha=0.4)
#axes.legend(loc='upper left', fontsize=12)
#for legpatch in axes.get_legend().get_patches():
#    col = legpatch.get_facecolor()
#    legpatch.set_edgecolor(col)
#    legpatch.set_facecolor('None')
#axes.tick_params(axis='x', which='minor', bottom=False)
#axes.minorticks_on()
#plt.tight_layout()
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf,
#                         'Overlap', 'HCHO_NO2_DOASv5_all.png'), format='png',
#    dpi=300)
#
