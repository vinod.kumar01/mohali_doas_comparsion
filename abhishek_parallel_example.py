# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 09:41:18 2019

@author: Vinod
"""

import numpy as np
import multiprocessing as mp
import sys
import time
#np.set_printoptions(threshold=sys.maxsize)
#Load Data
sample=np.loadtxt('sample.txt',delimiter=',',skiprows=1) #Sample data
(data1,data2,data3)=np.genfromtxt('data.txt', usecols=(0,1,2),unpack=True,delimiter=',',skip_header=0)    #orignal Grid

#start_time=time.time()
##define function##
def function_cal_data3(j):
    tmp_array=np.zeros(len(sample[0,2:]),dtype=float)
    
    index=np.argmin(np.abs(data1[j]-sample[:,0]))
    
    if(index<1000):
        index_min=index
    else:
        index_min=index-1000
        
    if(index>len(sample[:,0])):
        index_max=index
    else:
        index_max=index+1000
        
    for i in range(index_min,index_max):
        if (np.abs(data1[j]-sample[i,0])<=0.01) and (np.abs(data2[j]-sample[i,1])<=0.01):
            tmp_array[:]=sample[i,2:]
            break
    
    print(j)
    
    return tmp_array
    
##Set processor###
process = mp.Pool(processes=8)
result  = process.map(function_cal_data3,range(1,641462)) ####Range of data ##can be len(data1)
process.close()
process.join()

array_with_properties=np.array(result,dtype=int)
total_property=np.sum(array_with_properties,axis=1)


#end_time=time.time()
#print(end_time-start_time)
#----------------------------- Parallel Code Block --------------------------------------#
#####Please check before the run#######
file_name = np.array([11,14,20,30,40,50,60,70,90,100,110,120,130,140,150,160,170,180,190,200,210,220,230])
#print(len(file_name))

f=[open("data_file_{}.txt".format(file_name[m]),'w') for m in range(len(file_name))]


for i in range (len(array_with_properties)):
    for j in range (len(file_name)):
        if (abs( total_property[i] - array_with_properties[i,j]  ) <= 0.0001) and (total_property[i]!=0) :
            #print(j,file_name[j],"ok")
            f[j].write((3*"%13.6f"+"\n") %(data1[i],data2[i],data3[i]))


for i in range(len(file_name)):
    f[i].close()