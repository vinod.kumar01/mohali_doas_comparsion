# -*- coding: utf-8 -*-
"""
Created on Sun Dec 29 15:55:35 2019

@author: Vinod
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import import_merge_mapa_out as mapa


Inp = mapa.load_plot('aod', 'o4', 'AOD', 0, 2.5, 'MODIS', 'MAIAC', 'c_weighted_mean', sf='var')
concur_day = False    # set Trueto ignore DOAS measurement on days when satellite data is absent
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
o4sf = np.array(o4sf)
n_bins = np.arange(0,2,0.05)
count_v, bins = np.histogram(o4sf[flag==0], n_bins)
count_w, bins = np.histogram(o4sf[flag==1], n_bins)
count_e, bins = np.histogram(o4sf[flag==2][~np.isnan(o4sf[flag==2])], n_bins)

#def colorpic(val):
#    if (0.6<val<1.2):
#        return "green"
#    elif (0.4<val<1.4):
#        return "orange"
#    else:
#        return "red"
#c = [colorpic(i) for i in bins]
fig,ax = plt.subplots()
flag_name=['Error','Warning', 'Valid']
ax.bar(bins[:-1], count_e, width=0.04, label='Error', color='red')
ax.bar(bins[:-1], count_w, width=0.04, label='Warning', bottom = count_e, color='orange')
ax.bar(bins[:-1], count_v, width=0.04, label='Valid', 
       bottom = count_e+count_w, color='green')

ax.minorticks_on()
ax.set_ylabel ('Frequency')
ax.set_xlabel ('O$_{4}$ scaling factor')
ax.legend(loc='upper right')
plt.tight_layout()
plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf, 'scaling_factor_v5.png'),
                         format='png', dpi=300)