# -*- coding: utf-8 -*-
"""
Created on Fri Nov 22 21:18:08 2019

@author: Vinod
"""

# %% definitions and imports
import os
import netCDF4
import numpy as np
from datetime import timedelta, date
import matplotlib.pyplot as plt
import pandas as pd
import import_merge_mapa_out as mapa
from scipy.interpolate import interp1d
import matplotlib.dates as mdates
from scipy import stats
from decimal import Decimal
import calendar
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
    
# %% load DOAS Data
plot_bAMF = False
# whether to use a priori or use the method of smoothing by Gaia
use_apriori = True
mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath=r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
                     -0.25e16, 2e16, 'OMI', 'DOMINO', 'vcd_weighted_mean', 1e15, sf='0.8')
vcd_mean, z, flag, profile_mean, o4sf, h, s, time2 = Inp.load_data(mapa_inpath,
                                                          mapa_outpath)

profile_mean, flag = np.array(profile_mean), np.array(flag)
vcd_mean, time2 = np.array(vcd_mean), np.array(time2)
mask = np.empty(profile_mean.shape, dtype=bool)
qa = flag[:] == 2
vcd_mean[qa] = np.nan
mask[:, :] = (qa)[:, np.newaxis]
# profile_mean = np.ma.MaskedArray(profile_mean, mask=mask)
profile_mean[mask] = np.nan  # masked values have problem in pandas
#cfilter profiles around OMI overpass
#idx_sat = (pd.to_datetime(time2).hour>=7) & (pd.to_datetime(time2).hour<9)
z = np.array(z)*1000
vcd_mean = np.array(vcd_mean)
# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
profile_mean[OT_cloud==1, :] = np.nan
profile_mean[fog==1, :] = np.nan
vcd_mean[OT_cloud==1] = np.nan
vcd_mean[fog==1] = np.nan
# only keep the data around satellite overpass
time2 = pd.to_datetime(time2)
idx_sat = (time2.hour>=7) & (time2.hour<9)
profile_mean = profile_mean[idx_sat, :]
vcd_mean = vcd_mean[idx_sat]
# %% Load satellite VCD data in csv for comparison
dirname_sat = r'D:\postdoc\Sat'

# filename of extracted data around measurement location
if Inp.prod=="DOMINO":
    f_name = Inp.tracer+'_'+Inp.inst+'_DOM_TS50_sf08glue_corr.csv'
elif Inp.prod=="QA4ECV":
    f_name = Inp.tracer+'_'+Inp.inst+'_QA4ECV_50_cf03.csv'
#f_name = 'test1.csv'
df_sat = pd.read_csv(os.path.join(dirname_sat, Inp.inst, 'TS_mohali', f_name),
                     delimiter=',', header=0, escapechar='#')
# remove excess space from headers
df_sat.columns = df_sat.columns.str.strip()
# get the data formats right
df_sat['date_time'] = pd.to_datetime(df_sat['date_time'])
for cols in df_sat.columns[1:]:
    df_sat[cols] = pd.to_numeric(df_sat[cols], errors='coerce')
#df_sat['Date'] = [d.strftime('%d-%m-%Y') for d in df_sat['date_time']]
#df_sat['Date'] = df_sat['date_time'].dt.date
df_sat = df_sat.set_index('date_time')
# Resample at different frequencies : Daily/Monthly

# %% Account for the DOAS data when satellite measurments were not available

# %% load OMI AK
dirname_sat = r'D:\postdoc\Sat\OMI\TS_mohali'
filename_sat = Inp.prod+'_AK_amf_mohali_2013_17_cf.nc'
#filename_sat = 'DOMINO_AK_amf_mohali_2013_17_new.nc'
f_omi = netCDF4.Dataset(os.path.join(dirname_sat, filename_sat), 'r')
prs_i = np.array([f_omi.variables['hyai'][i]+(97000*f_omi.variables['hybi'][i])
                    for i in range(0,35)])
datetime_sat = f_omi.variables['time'][:]
#datetime_sat = np.arange(0, 1674, 1)
datetime_sat = [date(2013,1,1)+timedelta(days=float(i)) for i in datetime_sat]
month = np.array([i.month for i in datetime_sat])
temp = f_omi.variables['Temperature'][:]
prs_i[prs_i<0]=np.nan
#calculate and correct height
h_interface=np.array([7.4*np.log(97000/prs_i[i]) for i in range(0,35)])*1000 
h=np.array([h_interface[i]-h_interface[i-1] for i in range(1,35)])
h=h*temp[100,:]/250
z_centre = (h_interface[1:]+h_interface[:-1])/2 
z_centre=z_centre*temp[100,:]/250
z_centre = np.array(z_centre)

#AK = np.array(f_omi.variables['AveragingKernel'][:], dtype=float)
AK = f_omi.variables['AveragingKernel'][:]
if use_apriori:
    AP = f_omi.variables['NO2'][:]
cf = f_omi.variables['CloudFraction'][:]
amf_tot = f_omi.variables['AMF_TOT'][:]
amf_trop = f_omi.variables['AMF_TROP'][:]
def plot_bAMF_season(ax):
#    ax.set_color_cycle(['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
#                    'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])  
    ax.set_color_cycle(['g', 'maroon', 'indigo'])  
    for i in range(9,12):
        idx = (month==i) & (cf<0.1)
        bamf_month = np.nanmean(bamf[idx, :16], axis=0)
        ax.plot(bamf_month, z_centre[:16]/1000, '-+',
            label=calendar.month_abbr[i])
        ax.set_ylabel('Altitude (Km)')
        ax.set_xlabel('Box airmass factors')
        ax.minorticks_on()
        ax.grid(alpha=0.4)
        ax.set_xlim(-0.01, 3)
        ax.set_ylim(0, 5)
        ax.legend(loc='upper left', ncol=2)
    
if plot_bAMF:
    bamf = np.ma.masked_array([amf_tot*AK[:, i] for i in range(34)]).T
    fig, ax = plt.subplots(1,2,figsize=[10,3], sharex=True, sharey=True)
#    ax.set_color_cycle(['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
#                    'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])  
    plot_bAMF_season(ax[0])
#    df = pd.read_csv(r'D:\postdoc\Sat\OMI\TS_mohali\Mohali_box_AMF_new__thomas.csv',
#                         delimiter=',', header=0, escapechar='#')
#    ax[1].set_color_cycle(['g', 'maroon', 'indigo'])
#    ax[1].plot(df['Sept_SZA30'], df['altitude'], '-+',
#        label="Sep")
#    ax[1].plot(df['Oct_SZA30'], df['altitude'], '-+',
#        label="Oct")
#    ax[1].plot(df['Nov_SZA30'], df['altitude'], '-+',
#        label="Nov")
#    plt.savefig(r'D:\postdoc\Sat\OMI\TS_mohali\DOMINO_bmf_sep_nov.png', format='png', dpi=300)
    
'''
Tropospheric averaging kernels are calculated as
total averaging kernel*total AMF/Tropospheric AMF
'''
AK_new = AK.copy()
for i in range(len(amf_tot)):
    AK_new[i, :] = AK[i, :]*amf_tot[i]/amf_trop[i]
# for DOMINO it is cloud fraction and not the cloud radiance fraction.
# Recommended limit 0.3
#AK_intp = np.zeros([1674,19], dtype=float)
#for days in range(AK.shape[0]):
#    f_ak = interp1d(z_centre[:11], AK[days,:11], bounds_error=False)
#    AK_intp[days, :] = f_ak(z)

AK_intp = {}
cf_dict = {}
AP_intp = {}
sat_vcd_dict = {}
for days in range(AK.shape[0]):
    f_ak = interp1d(z_centre[:12], AK_new[days,:12], bounds_error=False)
    if use_apriori:
        f_ap = interp1d(z_centre[:12], AP[days,:12]*1E15, bounds_error=False)
        AP_intp[datetime_sat[days].strftime('%d-%m-%Y')] = f_ap(z[:25])
    AK_intp[datetime_sat[days].strftime('%d-%m-%Y')] = f_ak(z[:25])
    cf_dict[datetime_sat[days].strftime('%d-%m-%Y')] = cf[days]
    sat_vcd_dict[datetime_sat[days].strftime('%d-%m-%Y')] = df_sat.loc[datetime_sat[days].strftime('%Y-%m-%d')]['vcd_cf']
    
# %% apply averaging kernels    
time2 = time2[idx_sat]

vcd_mod = np.zeros(len(time2))
bvcd_mod = np.zeros([len(time2), 25])
cf_intp = np.zeros(len(time2))
vcd_sat = np.zeros(len(time2))
for idx in range(len(time2)):
    try:
        if use_apriori:
            bvcd_mod[idx, :] = AK_intp[time2[idx].strftime('%d-%m-%Y')]*(200*100*profile_mean[idx,:25]-
                     AP_intp[time2[idx].strftime('%d-%m-%Y')])+AP_intp[time2[idx].strftime('%d-%m-%Y')]
        else:
           bvcd_mod[idx, :] = 200*100*profile_mean[idx,:25]*AK_intp[time2[idx].strftime('%d-%m-%Y')] 
        vcd_mod[idx] = np.sum(bvcd_mod[idx,:])
#        vcd_mod[idx] = np.dot(200*100*profile_mean[idx,:25],AK_intp[time2[idx].strftime('%d-%m-%Y')])
        cf_intp[idx] = cf_dict[time2[idx].strftime('%d-%m-%Y')]
        vcd_sat[idx] = sat_vcd_dict[time2[idx].strftime('%d-%m-%Y')]
        # 200m is the width of box and 1m = 100cm
    except KeyError:
        bvcd_mod[idx, :] = np.nan
        vcd_mod[idx] = np.nan
        cf_intp[idx] = np.nan
        vcd_sat[idx] = np.nan
# calculate cumulate vcd until a specfic layer
vcd_mod_cum = bvcd_mod.copy()
#vcd_mod_cum[:] = 0
for lev in range(25):
#    vcd_mod_cum[:, lev] = np.sum(bvcd_mod[:, :lev], ax=1)
    if lev != 0:
        vcd_mod_cum[:, lev] += vcd_mod_cum[:, lev-1]
    
# %% Create common data of satellite and DOAS to apply cloud filter
df = pd.DataFrame(data=[time2, vcd_mean.astype('float64'),
                        vcd_mod.astype('float64'), cf_intp.astype('float64'),
                        vcd_sat.astype('float64')]).T
df.columns = ['Date_time', 'vcd_original', 'vcd_mod', 'cf', 'vcd_sat_cf']
df['Date_time'] = pd.to_datetime(df['Date_time'])
for cols in df.columns[1:]:
    df[cols] = pd.to_numeric(df[cols], errors='coerce')
#df = df.set_index('Date_time')
#df = df.resample('D').mean()
#df_sat = pd.DataFrame(data=[datetime_sat, np.array(cf)]).T
#df_sat.dtypes
#df_sat.columns = ['Date_time', 'cf']
#df_sat['cf'] = pd.to_numeric(df_sat['cf'], errors='coerce')
#df_sat = df_sat.set_index('Date_time')
#df['cf']=df_sat['cf']
#df.reset_index(inplace=True)
#df_clear = df[df['cf']<0.3]
#df_clear = df[df['cf']<0.5]
df_clear = df.copy()
df_clear = df_clear.set_index('Date_time')
df_clear['vcd_sat_cf'] = df_clear['vcd_sat_cf']*Inp.wf
# this might be a problem. As the cloud fraction is mean of several pixels. 
# while vcd_cf takes mean of allpixels at which cf is less than 0.5
# %% plotting
fig, ax = plt.subplots(figsize=[11, 4])
ax.scatter(df_clear.index, df_clear['vcd_original'], label='original', s=2)
ax.scatter(df_clear.index, df_clear['vcd_mod'], label=Inp.prod+'_smoothened', s=2)
ax.set_ylim(-0.25e16, 2e16)
ax.set_xlim(pd.to_datetime('11-15-2012'), pd.to_datetime('06-15-2017'))
ax.set_ylim(Inp.y_min, Inp.y_max)
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
## contribution from individual boxes at different grids
#bar2 = ax.bar(df_clear['Date_time'], vcd_mod_cum[idx,2], width=0.001, label='cum_z2')
#bar1 = ax.bar(df_clear['Date_time'], vcd_mod_cum[idx,1], width=0.001, label='cum_z1')
#bar0 = ax.bar(df_clear['Date_time'], vcd_mod_cum[idx,0], width=0.001, label='cum_z0')
#df.to_csv(r'D:\postdoc\DOAS\Mohali_2013_17\sf_0.8\doas_vcd_'+Inp.prod+'_ak.csv', sep=',', index=False)


# %% daily mean for doas data
mask = df_clear['vcd_original'].isnull() | df_clear['vcd_mod'].isnull() | df_clear['vcd_sat_cf'].isnull()
df_overlap_mnth = df_clear[~mask].resample('MS').mean()
df_overlap_std = df_clear[~mask].resample('MS').sem()
df_day = df_clear[~mask].resample('D').mean()
df_day.reset_index(inplace=True)
df_overlap_mnth.reset_index(inplace=True)
df_overlap_std.reset_index(inplace=True)

def plots_ts(ax):
    ax.scatter(df_day['Date_time'], df_day['vcd_original'],
               s=2, c='r', alpha=0.15, label=None)
    ax.scatter(df_day['Date_time'], df_day['vcd_mod'],
               c='k', s=2, alpha=0.15, label=None)
    ax.scatter(df_day['Date_time'], df_day['vcd_sat_cf'],
               c='b', s=2, alpha=0.15, label=None)
    ax.set_ylim(Inp.y_min, Inp.y_max)
    markers, caps, bars = ax.errorbar(df_overlap_mnth['Date_time'], df_overlap_mnth['vcd_original'],
                            df_overlap_std['vcd_original'],
                            c='r', label='MAX-DOAS (original)', fmt='-x', capsize=4)
    [bar.set_alpha(0.5) for bar in bars]
    markers, caps, bars = ax.errorbar(df_overlap_mnth['Date_time'], df_overlap_mnth['vcd_mod'],
                            df_overlap_std['vcd_mod'],
                            c='k', label='MAX-DOAS (modified)', fmt='-x', capsize=4)
    [bar.set_alpha(0.5) for bar in bars]
    markers, caps, bars = ax.errorbar(df_overlap_mnth['Date_time'], df_overlap_mnth['vcd_sat_cf'],
                            df_overlap_std['vcd_sat_cf'],
                            c='b', label='OMI ('+Inp.prod+')', fmt='-x', capsize=4)
    [bar.set_alpha(0.5) for bar in bars]
    ax.legend(loc='upper right')
    ax.set_xlim(pd.to_datetime('11-15-2012'), pd.to_datetime('06-15-2017'))  
    ax.minorticks_on()
    ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
    ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
    ax.grid(axis='y', alpha=0.5)
    ax.set_ylabel(Inp.spec_str)
fig, ax = plt.subplots(figsize=[11, 4])
plots_ts(ax)
plt.tight_layout()

plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\sf_'+Inp.sf, 'Overlap',
                         Inp.mapa_spec+'_'+Inp.inst+'_'+Inp.prod+'_TS_DOAS_correctedv5.png'),
                         format='png', dpi=300)
# %% scatter plot
def plot_scatter(ax2):
    mask_day = df_day['vcd_mod'].isnull() | df_day['vcd_sat_cf'].isnull()
    ax2.scatter(df_day['vcd_mod'][~mask_day],
                df_day['vcd_sat_cf'][~mask_day], s=2, c='b', alpha=0.5, label='Daily')
    
    ax2.set_ylabel(Inp.inst + ' (' +Inp.prod+ ') ' + Inp.spec_str)
    ax2.set_xlabel('DOAS (modified) ' + Inp.spec_str)
    ax2.set_xlim(Inp.y_min, Inp.y_max)
    ax2.set_ylim(Inp.y_min, Inp.y_max)
    # # add linear regression line
    fit_param = stats.linregress(df_day['vcd_mod'][~mask_day],
                df_day['vcd_sat_cf'][~mask_day])[0:3]
    fit_func = np.poly1d(fit_param[0:2])
    ax2.plot(df_day['vcd_mod'][~mask_day],
             fit_func(df_day['vcd_mod'][~mask_day]), color="b",
             ls="--", linewidth=1)    
    ax2.plot(ax2.get_xlim(), ax2.get_ylim(), ls=":", c=".3")
    ax2.annotate("y = " + str('%.2f' % fit_param[0]) + "x + " +
                str('%.2E' % Decimal(str(fit_param[1]))) + "\n r = " +
                str('%.2f' % fit_param[2]), xy=(0.02, 0.85),
                xycoords='axes fraction', color = 'b')
     
    # add monthly values
    mask_mnth = df_overlap_mnth['vcd_mod'].isnull() | df_overlap_mnth['vcd_sat_cf'].isnull()
    ax2.scatter(df_overlap_mnth['vcd_mod'][~mask_mnth], df_overlap_mnth['vcd_sat_cf'][~mask_mnth],
                s=20, marker='s', facecolors='none', edgecolors='k', label='Monthly')
    fit_param2 = stats.linregress(df_overlap_mnth['vcd_mod'][~mask_mnth],
                                  df_overlap_mnth['vcd_sat_cf'][~mask_mnth])[0:3]
    fit_func2 = np.poly1d(fit_param2[0:2])
    ax2.plot(df_overlap_mnth['vcd_mod'][~mask_mnth],
             fit_func2(df_overlap_mnth['vcd_mod'][~mask_mnth]),
             color="k", linewidth=2)
    ax2.annotate("y = " + str('%.2f' % fit_param2[0]) + "x + " +
                str('%.2E' % Decimal(str(fit_param2[1]))) + "\n r = " +
                str('%.2f' % fit_param2[2]), xy=(0.02, 0.70), xycoords='axes fraction', color = 'k')
    ax2.legend(handletextpad=0.2, loc=[0.63,0.76])
    ax2.minorticks_on()
    
fig2, ax2 = plt.subplots()
plot_scatter(ax2)