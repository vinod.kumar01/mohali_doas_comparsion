# -*- coding: utf-8 -*-

'''
Created on 12-Jul-2019

@author: Vinod
'''

# %% definitions and imports
import os
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import import_merge_mapa_out as mapa
from scipy import stats
from decimal import Decimal


def color_flag(flag):
    if flag == 0:
        color = 'g'
    elif flag == 1:
        color = "orange"
    else:
        color = "r"
    return color


def plot_sf(date_time, scaling_fac):
    fig, ax = plt.subplots()
    ax.scatter(date_time, scaling_fac, s=1, c=color)
    ax.axhline(y=1, c='k', ls='-.')
    ax.axhline(y=0.8, c='k', ls='--')
    ax.set_ylim(0, 5)
    ax.set_ylabel('O4 scaling factor')
    ax.set_xlim(pd.to_datetime('12-09-2013'), pd.to_datetime('06-15-2017'))
    plt.show()


def interpolate(ts, datetime_index):
    x = pd.concat([ts, pd.Series(index=datetime_index)])
    return x.groupby(x.index).first().sort_index().fillna(
        method="ffill", limit=5)[datetime_index]

# %% load DOAS Data

Inp = mapa.load_plot('no2', 'no2', 'NO$_{2}$ concentration (molecules cm$^{3}$)',
                     5e9, 9e11, 'Chemiluminescence', 'vcd_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v5'
#Inp = mapa.load_plot('hcho', 'hcho_alt', 'HCHO concentration (molecules cm$^{3}$)',
#                5e9, 7e11, 'PTR-MS', 'IISERM', 'vcd_weighted_mean', sf='0.8')
vcd_mean, z, flag, profile_mean, h, time2 = Inp.load_data(mapa_inpath, mapa_outpath)

profile_mean, flag = np.array(profile_mean), np.array(flag)
time2 = np.array(time2)

color = [color_flag(i) for i in flag]
mask = np.empty(profile_mean.shape, dtype=bool)
mask[:, :] = (flag[:] == 2)[:, np.newaxis]
# profile_mean = np.ma.MaskedArray(profile_mean, mask=mask)
profile_mean[mask] = np.nan  # masked values have problem in pandas
#
# # plot_sf(time2, sf)   #plot scaling factor

'''
flag 0 = good
flag 1 = warning
flag 2 = error
'''
'''
surface concentration
'''
conc_surf = profile_mean[:, 0]

time3 = time2.astype(datetime)
df2 = pd.DataFrame(np.column_stack((time3, conc_surf, vcd_mean, flag)),
                   columns=['Date_time', 'conc_surface',
                            'vcd_mean', 'flag'])
df2['Date_time'] = pd.to_datetime(df2['Date_time'],
                                  format='%Y-%m-%d %H:%M:%S')
for cols in df2.columns[1:]:
    df2[cols] = pd.to_numeric(df2[cols])
# df2['conc_surface'] = pd.to_numeric(df2['conc_surface'])
# df2['vcd_mean'] = pd.to_numeric(df2['vcd_mean'])
# df2['flag'] = pd.to_numeric(df2['flag'])
df2 = df2.set_index('Date_time')

# %% load insitu data
insitu_path = r'D:\postdoc\DOAS\Mohali_2013_17'
insitu_file = os.path.join(insitu_path, 'in_situ.csv')
colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
            'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
            'abs_humidity', 'hcho']
insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=colnames)
insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                         format='%d-%m-%Y %H:%M:%S')
insitu_data['desttime'] -= timedelta(minutes=330)  # offset for IST
for cols in colnames[1:]:
    insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')
insitu_data[Inp.tracer+'_conc'] = insitu_data[Inp.tracer]*6.023E23*1E-9*97000
insitu_data[Inp.tracer+'_conc'] /= (8.314*(insitu_data['Amb_temp']+273.16)*1E6)
df3 = insitu_data.set_index('desttime')
df4 = interpolate(df3[Inp.tracer+'_conc'], df2.index)
df2[Inp.tracer+'_insitu'] = df4

df2_dm = df2.resample('D').mean()    # to resample
df2_ds = df2.resample('D').std()    # to resample

df2_dm.reset_index(inplace=True)
df2_ds.reset_index(inplace=True)

df2 = df2.reset_index()
dt_intp = [d.to_pydatetime() for d in df2['Date_time']]

# %% time series
'''
Surface concentration
'''
# inst = 'PTR-MS' if Inp.tracer == 'hcho' else 'Chemiluminescence'
fig, ax = plt.subplots(figsize=[11, 5])

ax.scatter(dt_intp, df2['conc_surface'], c=color, s=2, alpha=0.1,
           label='MAX-DOAS')
ax.scatter(dt_intp, df2[Inp.tracer+'_insitu'], c='k', s=2, alpha=0.1,
           label=Inp.inst, marker="v")
ax.plot(df2_dm['Date_time'], df2_dm['conc_surface'],
        c='r', label='MAX-DOAS (daily)')
ax.plot(df2_dm['Date_time'], df2_dm[Inp.tracer+'_insitu'],
        c='k', label=Inp.inst+' (daily)')

# ax.errorbar(df2_dm['Date_time'], df2_dm['conc_surface'],
#             df2_ds['conc_surface'], c='g', label='MAX-DOAS (daily)')
# ax.errorbar(df2_dm['Date_time'], df2_dm[spec+'_insitu'],
#             df2_ds[spec+'_insitu'], c='b', label='MAX-DOAS (daily)')
ax.set_ylim(Inp.y_min, Inp.y_max)
ax.set_ylabel("Surface " + Inp.spec_str, fontsize=10)
plt.legend(loc='upper right', markerscale=2.5)
ax.set_xlabel("Date and Time (UTC)", fontsize=10)
ax.set_xlim(pd.to_datetime('12-09-2012'), pd.to_datetime('06-15-2017'))
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
plt.tight_layout()
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf,
#                         Inp.tracer+'_insitu.png'), format='png', dpi=300)

# %% Scatter plot
fig, ax = plt.subplots()
ax.scatter(df2_dm[Inp.tracer+'_insitu'], df2_dm['conc_surface'], s=1, c='k', label='Daily mean')
idx = np.isfinite(df2_dm[Inp.tracer+'_insitu']) & np.isfinite(df2_dm['conc_surface'])
fit_param = stats.linregress(df2_dm[Inp.tracer+'_insitu'][idx], df2_dm['conc_surface'][idx])[0:3]
fit_func = np.poly1d(fit_param[0:2])
ax.plot(df2_dm[Inp.tracer+'_insitu'][idx], fit_func(df2_dm[Inp.tracer+'_insitu'][idx]),
        color="b", ls="--", linewidth=1, label='linear fit')
ax.set_xlabel(Inp.spec_str+' ('+Inp.inst+')')
ax.set_ylabel(Inp.spec_str+' (DOAS)')
ax.set_ylim(Inp.y_min, Inp.y_max)
ax.set_xlim(Inp.y_min, Inp.y_max)
ax.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c=".3", label='1:1 line')
ax.legend(loc='upper right')
ax.minorticks_on()
ax.annotate("y = " + str('%.1f' % fit_param[0]) + "x + " +
            str('%.2E' % Decimal(str(fit_param[1]))) + "\n r = " +
            str('%.2f' % fit_param[2]), xy=(0.05, 0.9), xycoords='axes fraction', color = 'k')
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17', 'sf_'+Inp.sf, Inp.tracer+'_insitu_scatter.png'), format='png', dpi=300)
