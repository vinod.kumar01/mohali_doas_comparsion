# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 10:23:48 2019
Script to evaluate the effect of relaxing relative RMS criteria on NO2 uv
@author: Vinod
"""
# %% imports
import numpy as np
import import_merge_mapa_out as mapa
import matplotlib.pyplot as plt
from scipy import stats
from decimal import Decimal
import os
# %% load data
# NO2 VIS data as reference
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v4'
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
                     -0.25e16, 2e16, 'OMI', 'DOMINO', 'vcd_weighted_mean', 1e15, sf='0.8')
vcd_mean, z, flag, profile_mean, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean_vis, flag_vis = np.array(vcd_mean), np.array(flag)
h_vis = np.array(h)
valid_vis = len(flag_vis[flag_vis==0])
print("Number of valid retrievals for NO2 in vis are {}".format(valid_vis))
time2 = np.array(time2)
profile_mean_vis = np.array(profile_mean)
profile_mean_vis = profile_mean_vis[:, 0]
vcd_mean_vis[flag_vis != 0] = np.nan
profile_mean_vis[flag_vis != 0] = np.nan
h_vis[flag_vis != 0] = np.nan
# NO2 UV with original flag
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v4'
Inp = mapa.load_plot('no2', 'no2', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
                     -0.25e16, 2e16, 'OMI', 'DOMINO', 'vcd_weighted_mean', 1e15, sf='0.8')
vcd_mean, z, flag, profile_mean, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean_uv, flag_uv = np.array(vcd_mean), np.array(flag)
h_uv = np.array(h)
valid_uv = len(flag_uv[flag_uv==0])
print("Number of valid retrievals for NO2 in UV are {}".format(valid_uv))
time2 = np.array(time2)
profile_mean_uv = np.array(profile_mean)
profile_mean_uv = profile_mean_uv[:, 0]
vcd_mean_uv[flag_uv != 0] = np.nan
profile_mean_uv[flag_uv != 0] = np.nan
h_uv[flag_uv != 0] = np.nan

# %% Plotting
fig, ax = plt.subplots(2, 2, figsize=[11,8])
plt.suptitle('MAPA with Rn<0.05 (valid only)', x=0.3)
# plot VCD
idx = np.isfinite(vcd_mean_vis) & np.isfinite(vcd_mean_uv)
ax[0][0].scatter(vcd_mean_vis[idx], vcd_mean_uv[idx], s=2, label='VCD',
              c=h_vis[idx], vmin=0.02, vmax=1, cmap='Reds')
ax[0][0].set_ylabel('NO$_{2}$ VCD (molecules cm$^{-2}$) (MAPA UV)')
ax[0][0].set_xlabel('NO$_{2}$ VIS VCD (molecules cm$^{-2}$) (MAPA VIS)')
fit_param = stats.linregress(vcd_mean_vis[idx], vcd_mean_uv[idx])[0:3]
fit_func = np.poly1d(fit_param[0:2])
ax[0][0].set_ylim(0, 4e16)
ax[0][0].set_xlim(0, 4e16)
ax[0][0].plot(ax[0][0].get_xlim(), ax[0][0].get_ylim(), ls=":", alpha=0.3)
ax[0][0].plot(vcd_mean_vis[idx], fit_func(vcd_mean_vis[idx]), ls="--",
        linewidth=1, c='k')
if fit_param[1] >= 0:
    ax[0][0].annotate("n = "+str(len(vcd_mean_uv[idx]))+"\ny = " + str('%.2f' % fit_param[0]) + "x + " +
      str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
      str('%.2f' % fit_param[2]), xy=(0.1, 0.85), xycoords='axes fraction')
else:
    ax[0][0].annotate("n = "+str(len(vcd_mean_uv[idx]))+"\ny = " + str('%.2f' % fit_param[0]) + "x - " +
      str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
      str('%.2f' % fit_param[2]), xy=(0.1, 0.85), xycoords='axes fraction')
# histogram plot for VCD
rat_vcd = vcd_mean_vis[idx]/vcd_mean_uv[idx]
n_bins = np.arange(0.5,2.05,0.05)
count, bins = np.histogram(rat_vcd, n_bins)
ax[1][0].bar(bins[:-1], count, width=0.04, label='Ratio')
ax[1][0].axvline(x=1, alpha=0.5, c='r')
ax[1][0].set_xlabel('NO$_{2}$ VCD(vis)/VCD(uv)')
ax[1][0].set_ylabel('Frequency')
ax[1][0].minorticks_on()

# plot surface concentration
idx = np.isfinite(profile_mean_vis) & np.isfinite(profile_mean_uv)
scat = ax[0][1].scatter(profile_mean_vis[idx], profile_mean_uv[idx], s=2, alpha=0.5, label='Concentration',
              c=h_vis[idx], vmin=0.02, vmax=1, cmap='Reds')
ax[0][1].set_ylabel('NO$_{2}$ UV conc. (molecules cm$^{-3}$) (MAPA UV)')
ax[0][1].set_xlabel('NO$_{2}$ VIS conc. (molecules cm$^{-3}$) (MAPA VIS)')
fit_param = stats.linregress(profile_mean_vis[idx], profile_mean_uv[idx])[0:3]
fit_func = np.poly1d(fit_param[0:2])
ax[0][1].set_ylim(0, 2e12)
ax[0][1].set_xlim(0, 2e12)
ax[0][1].plot(ax[0][1].get_xlim(), ax[0][1].get_ylim(), ls=":", alpha=0.3)
ax[0][1].plot(profile_mean_vis[idx], fit_func(profile_mean_vis[idx]), ls="--", c='k', 
        linewidth=1)
if fit_param[1] >= 0:
    ax[0][1].annotate("n = "+str(len(vcd_mean_uv[idx]))+"\ny = " + str('%.2f' % fit_param[0]) + "x + " +
      str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
      str('%.2f' % fit_param[2]), xy=(0.1, 0.85), xycoords='axes fraction')
else:
    ax[0][1].annotate("n = "+str(len(vcd_mean_uv[idx]))+"\ny = " + str('%.2f' % fit_param[0]) + "x - " +
      str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
      str('%.2f' % fit_param[2]), xy=(0.1, 0.85), xycoords='axes fraction')


# histogram plot for surface conc
rat_conc = profile_mean_vis[idx]/profile_mean_uv[idx]
rat_conc = rat_conc[np.isfinite(rat_conc)]
n_bins = np.arange(0.5,2.05,0.05)
count, bins = np.histogram(rat_conc, n_bins)
ax[1][1].bar(bins[:-1], count, width=0.04, label='Ratio')
ax[1][1].axvline(x=1, alpha=0.5, c='r')
ax[1][1].set_xlabel('NO$_{2}$ conc. (vis)/VCD(uv)')
ax[1][1].set_ylabel('Frequency')
ax[1][1].minorticks_on()

plt.tight_layout()
fig.subplots_adjust(right=0.88, wspace = 0.2, hspace = 0.2)
cbar_ax=fig.add_axes([0.9, 0.55, 0.02, 0.4])
cbar=fig.colorbar(scat, cax = cbar_ax, extend='max')
cbar.set_label('Layer height (km)', fontsize=12)
plt.savefig(os.path.join(mapa_outpath, 'scat_uv_vis_rn_015.png'), format='png', dpi=300)