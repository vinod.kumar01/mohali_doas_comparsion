# -*- coding: utf-8 -*-
"""
Created on Wed Oct  2 10:20:47 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import sys
from datetime import date, datetime
import pandas as pd
lat_bound = [30.4, 30.9]
lon_bound = [76.4, 77.0]
profile_dir = 'D:\postdoc\Sat\OMI\TS_mohali'
filename_profile = 'QA4ECV_hcho_profile_mohali_2015_dom_temp.nc'
f_profile = netCDF4.Dataset(os.path.join(profile_dir, filename_profile), 'r')
meas_profile = f_profile.variables['hcho_sub_col_mod'][:]
meas_rel_profile = meas_profile[:, 0:16]
for days in range(meas_profile.shape[0]):
    meas_rel_profile[days, :] = meas_rel_profile[days, :]/np.ma.sum(meas_rel_profile[days, :])
#lat_bound = [30.57, 30.77]
#lon_bound = [76.63, 76.83]
basedir = r"M:\DATASETS\OMI_HCHO\QA4ECV\l2"
vcd_ts = []
vcd_corr_ts = []
vcd_ts_std = []
vcd_ts_cf_30 = []
vcd_corr_ts_cf_30 = []
vcd_ts_cf_30_std = []
vcd_ts_cf_60 = []
vcd_ts_cf_50 = []
vcd_ts_cf_40 = []
vcd_ts_cf_35 = []
vcd_ts_cf_25 = []
vcd_ts_cf_20 = []
vcd_ts_cf_15 = []
vcd_ts_cf_10 = []
date_time = []

start_date = date(2013, 1, 1)
#end_date = date(2013, 1, 6)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(basedir,
                                        single_date.strftime("%Y\%m\%d"),
                                        'QA4ECV_L2_HCHO_OMI_*.nc'))

    vcd_day, vcd_day_corr = [], []
    vcd_day_cf_30, vcd_day_cf_50 , vcd_day_cf_60 = [], [], []
    vcd_day_cf_40, vcd_day_cf_35, vcd_day_cf_25 = [], [], []
    vcd_day_cf_20, vcd_day_cf_15, vcd_day_cf_10 = [], [], []
    for path_sat in f_name_sat:
        AK_orb = []
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        tempdate = path_sat.split('\\')[-1].split('_')[4]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[4:6])
        temp_day = int(tempdate[6:8])
        temp_hour = int(tempdate[9:11])
        temp_min = int(tempdate[11:13])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        daynum = min(temp_dt.timetuple().tm_yday, 365)
        Data_fields = f.groups['PRODUCT']
        lats = Data_fields.variables['latitude'][0, :]
        lons = Data_fields.variables['longitude'][0, :]
        HCHO_tropcol = Data_fields.variables['tropospheric_hcho_vertical_column']
        INPUT_fields = f.groups['PRODUCT'].groups['SUPPORT_DATA'].groups['INPUT_DATA']
        AMF_trop = Data_fields.variables['amf_trop'][0, :]
        AK = Data_fields.variables['averaging_kernel'][0, :]
        cf = INPUT_fields.variables['cloud_fraction'][0, :]
        flag = Data_fields.variables['processing_error_flag'][0, :]
        row_flag = INPUT_fields.variables['omi_xtrack_flags'][0, :]
        fv = HCHO_tropcol._FillValue
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        keep &= (flag==0) & (row_flag==0)
        HCHO_tropcol = HCHO_tropcol[0, :]
        HCHO_tropcol = HCHO_tropcol[keep]
        cf = cf[keep]
        HCHO_tropcol_cf_30 = HCHO_tropcol[(cf < 0.3)]
        HCHO_tropcol_cf_50 = HCHO_tropcol[(cf < 0.5)]
        HCHO_tropcol_cf_60 = HCHO_tropcol[(cf < 0.6)]
        HCHO_tropcol_cf_40 = HCHO_tropcol[(cf < 0.4)]
        HCHO_tropcol_cf_35 = HCHO_tropcol[(cf < 0.35)]
        HCHO_tropcol_cf_25 = HCHO_tropcol[(cf < 0.25)]
        HCHO_tropcol_cf_20 = HCHO_tropcol[(cf < 0.2)]
        HCHO_tropcol_cf_15 = HCHO_tropcol[(cf < 0.15)]
        HCHO_tropcol_cf_10 = HCHO_tropcol[(cf < 0.10)]
        HCHO_tropcol_cf = HCHO_tropcol[(cf < 0.3)]
        if len(HCHO_tropcol) > 0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                + ' : ' + str(orbit_num))
            trop_SCD = HCHO_tropcol/AMF_trop[:][keep]
#            AMF_tot = AMF_tot[:][keep]
            trop_amf_scale = np.zeros(len(AMF_trop[:][keep]), dtype='float')
            # filter averaging kernels for pixels within spatial boundary
            for lev in range(34):
                AK_orb.append(AK[keep, :][:, lev])
            AK_orb = np.array(AK_orb)
            for i in range(len(AMF_trop[:][keep])):
                trop_amf_scale[i] = np.ma.divide(np.ma.sum(meas_rel_profile[daynum-1]*AK_orb[0:16,i]),
                                                 np.ma.sum(meas_rel_profile[daynum-1]))
            trop_amf_corr = AMF_trop[:][keep]*trop_amf_scale
            HCHO_tropcol_corr = trop_SCD/trop_amf_corr        
          
            vcd_day_corr.extend(HCHO_tropcol_corr)
            vcd_day.extend(HCHO_tropcol)
        if len(HCHO_tropcol_cf_60 > 0):
            vcd_day_cf_30.extend(HCHO_tropcol_cf_30)
            vcd_day_cf_40.extend(HCHO_tropcol_cf_40)
            vcd_day_cf_50.extend(HCHO_tropcol_cf_50)
            vcd_day_cf_60.extend(HCHO_tropcol_cf_60)
            vcd_day_cf_35.extend(HCHO_tropcol_cf_35)
            vcd_day_cf_25.extend(HCHO_tropcol_cf_25)
            vcd_day_cf_20.extend(HCHO_tropcol_cf_20)
            vcd_day_cf_15.extend(HCHO_tropcol_cf_15)
            vcd_day_cf_10.extend(HCHO_tropcol_cf_10)
        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_corr_daymean = np.nanmean(vcd_day_corr)
        vcd_daystd = np.nanstd(vcd_day)
    else:
        vcd_daymean = np.nan
        vcd_corr_daymean = np.nan
        vcd_daystd = np.nan
    if len(vcd_day_cf_60) > 0:
        vcd_daymean_cf_30 = np.nanmean(vcd_day_cf_30)
        vcd_daystd_cf_30 = np.nanstd(vcd_day_cf_30)
        vcd_daymean_cf_40 = np.nanmean(vcd_day_cf_40)
        vcd_daymean_cf_50 = np.nanmean(vcd_day_cf_50)
        vcd_daymean_cf_60 = np.nanmean(vcd_day_cf_60)
        vcd_daymean_cf_35 = np.nanmean(vcd_day_cf_35)
        vcd_daymean_cf_25 = np.nanmean(vcd_day_cf_25)
        vcd_daymean_cf_20 = np.nanmean(vcd_day_cf_20)
        vcd_daymean_cf_15 = np.nanmean(vcd_day_cf_15)
        vcd_daymean_cf_10 = np.nanmean(vcd_day_cf_10)
    else:
        vcd_daymean_cf_30 = np.nan
        vcd_daystd_cf_30 = np.nan
        vcd_daymean_cf_50 = np.nan
        vcd_daymean_cf_60 = np.nan
        vcd_daymean_cf_40 = np.nan
        vcd_daymean_cf_35 = np.nan
        vcd_daymean_cf_25 = np.nan
        vcd_daymean_cf_20 = np.nan
        vcd_daymean_cf_15 = np.nan
        vcd_daymean_cf_10 = np.nan
        
    vcd_ts.append(vcd_daymean)
    vcd_corr_ts.append(vcd_corr_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf_30.append(vcd_daymean_cf_30)
    vcd_ts_cf_30_std.append(vcd_daystd_cf_30)
    vcd_ts_cf_60.append(vcd_daymean_cf_60)
    vcd_ts_cf_50.append(vcd_daymean_cf_50)
    vcd_ts_cf_40.append(vcd_daymean_cf_40)
    vcd_ts_cf_35.append(vcd_daymean_cf_35)
    vcd_ts_cf_25.append(vcd_daymean_cf_25)
    vcd_ts_cf_20.append(vcd_daymean_cf_20)
    vcd_ts_cf_15.append(vcd_daymean_cf_15)
    vcd_ts_cf_10.append(vcd_daymean_cf_10)
    date_time.append(single_date.strftime("%Y-%m-%d"))
    print("day " + single_date.strftime("%Y-%m-%d"))
omi_ts = np.vstack((date_time, vcd_ts, vcd_corr_ts, vcd_ts_std, vcd_ts_cf_30,
                    vcd_ts_cf_30_std, vcd_ts_cf_60, vcd_ts_cf_50, vcd_ts_cf_40, 
                    vcd_ts_cf_35, vcd_ts_cf_25, vcd_ts_cf_20, vcd_ts_cf_15, 
                    vcd_ts_cf_10)).T
np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\hcho_OMI_QA4ECV_corr_50_cf_all.csv", omi_ts,
           delimiter=",", header = 'date_time, vcd, vcd_corr, vcd_std,'
                     'vcd_cf, vcd_cf_std, vcd_cf_60, vcd_cf_50, vcd_cf_40, vcd_cf_35,'
                     'vcd_cf_25,vcd_cf_20,vcd_cf_15,vcd_cf_10',
           fmt="%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log1.txt', 'w')