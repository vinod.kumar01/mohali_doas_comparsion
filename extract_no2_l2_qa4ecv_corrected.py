# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 12:12:40 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import pandas as pd
import sys
from datetime import date, datetime
# %% define limits and path
lat_bound = [30.4, 30.9]
lon_bound = [76.4, 77.0]

#lat_bound = [30.57, 30.77]
#lon_bound = [76.63, 76.83]
profile_dir = 'D:\postdoc\Sat\OMI\TS_mohali'
filename_profile = r'DOMINO_profile_mohali_2015_corrsf_08.nc'
f_profile = netCDF4.Dataset(os.path.join(profile_dir, filename_profile), 'r')
meas_profile = f_profile.variables['no2_sub_col_mod'][:]
meas_rel_profile = meas_profile[:, 0:16]
for days in range(meas_profile.shape[0]):
    meas_rel_profile[days, :] = meas_rel_profile[days, :]/np.ma.sum(meas_rel_profile[days, :])
    
dirname = r"M:\nobackup\vinod\QA4ECV\NO2"
vcd_ts = []
vcd_ts_std = []
vcd_ts_cf_30 = []
vcd_corr_ts_cf_30 = []
vcd_ts_cf_30_std = []
vcd_ts_cf_60 = []
vcd_ts_cf_50 = []
vcd_ts_cf_40 = []
vcd_ts_cf_35 = []
vcd_ts_cf_25 = []
vcd_ts_cf_20 = []
vcd_ts_cf_15 = []
vcd_ts_cf_10 = []
date_time = []

start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(dirname,
                                        single_date.strftime("%Y\%m\%d"),
                                        'QA4ECV_L2_NO2_' + '*.nc'))
    vcd_day = []
    vcd_day_cf_30, vcd_day_cf_50 , vcd_day_cf_60 = [], [], []
    vcd_day_cf_40, vcd_day_cf_35, vcd_day_cf_25 = [], [], []
    vcd_day_cf_20, vcd_day_cf_15, vcd_day_cf_10 = [], [], []
    for path_sat in f_name_sat:
        AK_orb = []
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        tempdate = path_sat.split('\\')[-1].split('_')[4]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[4:6])
        temp_day = int(tempdate[6:8])
        temp_hour = int(tempdate[9:11])
        temp_min = int(tempdate[11:13])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        daynum = min(temp_dt.timetuple().tm_yday, 365)
        swath = f.groups['PRODUCT']
        lats = swath.variables['latitude'][0, :]
        lons = swath.variables['longitude'][0, :]
        NO2_tropcol = swath.variables['tropospheric_no2_vertical_column']
#        AMF_trop = swath.variables['amf_trop'][0, :]
#        AMF_tot = swath.variables['amf_total'][0, :]
#        AK = swath.variables['averaging_kernel'][0, :]
#        add_data =swath.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS']
        add_data =swath.groups['SUPPORT_DATA'].groups['INPUT_DATA']
        flag = swath.variables['processing_error_flag'][0, :]
        cf = add_data.variables['cloud_fraction'][0, :]
        fv = NO2_tropcol._FillValue
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        keep &= flag==0
        NO2_tropcol = NO2_tropcol[0, :]
        NO2_tropcol = NO2_tropcol[keep]
        NO2_tropcol = 1E-15*NO2_tropcol
        cf = cf[keep]
        NO2_tropcol_cf_30 = NO2_tropcol[(cf < 0.3)]
        NO2_tropcol_cf_50 = NO2_tropcol[(cf < 0.5)]
        NO2_tropcol_cf_60 = NO2_tropcol[(cf < 0.6)]
        NO2_tropcol_cf_40 = NO2_tropcol[(cf < 0.4)]
        NO2_tropcol_cf_35 = NO2_tropcol[(cf < 0.35)]
        NO2_tropcol_cf_25 = NO2_tropcol[(cf < 0.25)]
        NO2_tropcol_cf_20 = NO2_tropcol[(cf < 0.2)]
        NO2_tropcol_cf_15 = NO2_tropcol[(cf < 0.15)]
        NO2_tropcol_cf_10 = NO2_tropcol[(cf < 0.10)]
        # further processing of AMF correction on the relevant orbits
        if len(NO2_tropcol) > 0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                + ' : ' + str(orbit_num))
            vcd_day.extend(NO2_tropcol)
#            NO2_tropcol_corr_cf = NO2_tropcol_corr[(cf < 0.5)]
        if len(NO2_tropcol_cf_60 > 0):
            vcd_day_cf_30.extend(NO2_tropcol_cf_30)
            vcd_day_cf_40.extend(NO2_tropcol_cf_40)
            vcd_day_cf_50.extend(NO2_tropcol_cf_50)
            vcd_day_cf_60.extend(NO2_tropcol_cf_60)
            vcd_day_cf_35.extend(NO2_tropcol_cf_35)
            vcd_day_cf_25.extend(NO2_tropcol_cf_25)
            vcd_day_cf_20.extend(NO2_tropcol_cf_20)
            vcd_day_cf_15.extend(NO2_tropcol_cf_15)
            vcd_day_cf_10.extend(NO2_tropcol_cf_10)

        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)

        vcd_daystd = np.nanstd(vcd_day)
    else:
        vcd_daymean = np.nan
#        vcd_corr_daymean = np.nan
        vcd_daystd = np.nan
        
    if len(vcd_day_cf_60) > 0:
        vcd_daymean_cf_30 = np.nanmean(vcd_day_cf_30)
        vcd_daystd_cf_30 = np.nanstd(vcd_day_cf_30)
        vcd_daymean_cf_40 = np.nanmean(vcd_day_cf_40)
        vcd_daymean_cf_50 = np.nanmean(vcd_day_cf_50)
        vcd_daymean_cf_60 = np.nanmean(vcd_day_cf_60)
        vcd_daymean_cf_35 = np.nanmean(vcd_day_cf_35)
        vcd_daymean_cf_25 = np.nanmean(vcd_day_cf_25)
        vcd_daymean_cf_20 = np.nanmean(vcd_day_cf_20)
        vcd_daymean_cf_15 = np.nanmean(vcd_day_cf_15)
        vcd_daymean_cf_10 = np.nanmean(vcd_day_cf_10)
    else:
        vcd_daymean_cf_30 = np.nan
        vcd_daystd_cf_30 = np.nan
        vcd_daymean_cf_50 = np.nan
        vcd_daymean_cf_60 = np.nan
        vcd_daymean_cf_40 = np.nan
        vcd_daymean_cf_35 = np.nan
        vcd_daymean_cf_25 = np.nan
        vcd_daymean_cf_20 = np.nan
        vcd_daymean_cf_15 = np.nan
        vcd_daymean_cf_10 = np.nan
        

    vcd_ts.append(vcd_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf_30.append(vcd_daymean_cf_30)
    vcd_ts_cf_30_std.append(vcd_daystd_cf_30)
    vcd_ts_cf_60.append(vcd_daymean_cf_60)
    vcd_ts_cf_50.append(vcd_daymean_cf_50)
    vcd_ts_cf_40.append(vcd_daymean_cf_40)
    vcd_ts_cf_35.append(vcd_daymean_cf_35)
    vcd_ts_cf_25.append(vcd_daymean_cf_25)
    vcd_ts_cf_20.append(vcd_daymean_cf_20)
    vcd_ts_cf_15.append(vcd_daymean_cf_15)
    vcd_ts_cf_10.append(vcd_daymean_cf_10)
    date_time.append(single_date.strftime("%Y-%m-%d"))
omi_ts = np.vstack((date_time, vcd_ts, vcd_ts_std, vcd_ts_cf_30,
                    vcd_ts_cf_30_std, vcd_ts_cf_60, vcd_ts_cf_50, vcd_ts_cf_40, 
                    vcd_ts_cf_35, vcd_ts_cf_25, vcd_ts_cf_20, vcd_ts_cf_15, 
                    vcd_ts_cf_10)).T
np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\no2_OMI_QA4ECV_50_cf_all.csv", omi_ts,
           delimiter=",", header = 'date_time, vcd, vcd_std,'
                     'vcd_cf, vcd_cf_std, vcd_cf_60, vcd_cf_50, vcd_cf_40, vcd_cf_35,'
                     'vcd_cf_25,vcd_cf_20,vcd_cf_15,vcd_cf_10',
           fmt="%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log1.txt', 'w')