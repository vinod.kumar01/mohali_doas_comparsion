# -*- coding: utf-8 -*-
"""
Created on Tue Dec 17 18:11:31 2019

@author: Vinod
"""
import numpy as np
from datetime import datetime
import netCDF4
import os
import pandas as pd
import matplotlib.pyplot as plt
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def get_scd(tracer,mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v3',
              mapa_outpath=r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v3',
              geo_angle=2):
    amf = 1/np.sin(geo_angle*np.pi/180)-1
    scd, geo_vcd, sza, time2 = [], [], [], []
    for nc_dir in ['analysis_2012_1_auto', 'analysis_2012_2_auto',
                   'analysis_2013_1_auto', 'analysis_2013_2_auto',
                   'analysis_2013_3_auto', 'analysis_2013_4_auto',
                   'analysis_2014_1_auto', 'analysis_2014_2_auto',
                   'analysis_2015_1_auto', 'analysis_2015_2_auto',
                   'analysis_2015_3_auto', 'analysis_2015_4_auto',
                   'analysis_2015_5_auto', 'analysis_2015_6_auto',
                   'analysis_2016_1_auto', 'analysis_2016_2_auto',
                   'analysis_2017_1_auto']:
        mapa_infile = os.path.join(mapa_inpath, nc_dir+".nc")
        datafile = netCDF4.Dataset(mapa_infile, 'r')
        eas = datafile.variables['ea'][:]
        idx_elev = np.argwhere(eas==geo_angle)
        idx_elev = idx_elev[0][0]
        tracer_dscd = datafile.variables[tracer+'_dscd'][:]
        scd.extend(tracer_dscd[:, idx_elev])
        tracer_vcd = tracer_dscd[:, idx_elev]/amf
        sza_file = datafile.variables['sza'][:, idx_elev]
        dat = datafile.variables['time'][:]
        timegrid = np.max(dat, axis=1)
        time2.extend(np.array(
            [np.datetime64(datetime.strptime(str(int(t)), "%Y%m%d%H%M%S"))
             for t in timegrid]))
        geo_vcd.extend(tracer_vcd)   
        sza.extend(sza_file)
        print(nc_dir)
    return scd, geo_vcd, sza, time2


def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])
# %% data import
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v0'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v0'
dscd, geo_vcd, sza, time2 = get_scd('o4', mapa_inpath, mapa_outpath, geo_angle=30)
time2 = pd.to_datetime(time2)
dscd, geo_vcd = np.array(dscd), np.array(geo_vcd)

# %% filter out cloudy data
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad_new.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
dscd[OT_cloud==1] = np.nan
dscd[fog==1] = np.nan

df_old = pd.DataFrame(data=[time2, dscd, cloud_type, sza]).T
df_old.columns = ['Date_time', 'dSCD', 'cloud_type', 'sza']
df_old['Date_time'] = pd.to_datetime(df_old['Date_time'])
df_old['dSCD'] = pd.to_numeric(df_old['dSCD'], errors='coerce')
df_old['cloud_type'] = pd.to_numeric(df_old['cloud_type'], errors='coerce')
df_old['sza'] = pd.to_numeric(df_old['sza'], errors='coerce')
df_old = df_old[df_old['sza']<70]
df_old = df_old.set_index('Date_time')
df_old_day = df_old.resample('D').mean()
# %% plotting
#fig,ax = plt.subplots(figsize=[11, 4])
#ax.scatter(time2[cloud_type<3], dscd[cloud_type<3], s=1, label='new_retrieval' )
#ax.set_ylim(0, 5e43)