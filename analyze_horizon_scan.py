'''
Created on 03-Dec-2018

@author: Vinod
'''
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import os
import pandas as pd
import handleMFC
from datetime import date
# sets the font and text style in the plots
mpl.rcParams.update({
    'text.usetex': False,
    'font.family': 'Times New Roman',
    'font.size': 10,
    'legend.fontsize': 8,
    'mathtext.default': 'regular'})


# define model function for the Gaussian fit later
def fitFunc(x, A, mu, sigma):
    return A*np.exp(-((x-mu)**2/(2.*sigma**2)))


# define paths for the different input files
start_date = date(2014, 9, 16)
end_date = date(2014, 9, 18)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
base_path = r'D:\wat\minimax\doas_2014\horizon_scan'  # my project path
offset_file = 'offset_osdc000419'
dc_file = 'dc_osdc000420'
calib_file = 'calib_mfc_12543.clb'

calib_data = np.genfromtxt(os.path.join(base_path, calib_file), skip_header=6)
calib = calib_data[:, 0]

# read Offset and DC and normalise it to one scan (OS) and one TIMEUNIT
# (check this!!!)
Offset_data = handleMFC.readMFC(os.path.join(base_path, offset_file))
Offset = Offset_data['spectrum']/Offset_data['iNoScans']
DC_data = handleMFC.readMFC(os.path.join(base_path, dc_file))
DC = ((DC_data['spectrum']-Offset)/DC_data['fExposureTime'])  # *1000
fit_params = []
for single_date in daterange:
    fit_params_now = []
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    datenow = single_date.strftime("%Y_%m_%d")
    # create empty lists
    elevs = []
    int_340 = []
    int_404 = []
    int_440 = []
    # create a list of all horizon scan spectra
    try:
        file_list = np.sort(os.listdir(os.path.join(base_path, datenow)))
    # read all spectra in file_list and write intensities to the list defined above
    except FileNotFoundError:
        fit_params_now = [i*np.nan for i in np.arange(9)]
        fit_params.append(fit_params_now)
        continue
    if len(file_list) < 20:
        fit_params_now = [i*np.nan for i in np.arange(9)]
        fit_params.append(fit_params_now)
        continue
    for idx in range(len(file_list)):
        # reading part
        data_path = os.path.join(base_path, datenow, file_list[idx])
        mfc_data = handleMFC.readMFC(data_path)
        cur_elev = float(mfc_data['szSpecName'])
        cur_spec = mfc_data['spectrum']
        cur_spec = cur_spec - (mfc_data['fExposureTime'] * DC)
        cur_spec = cur_spec - (mfc_data['iNoScans'] * Offset)
        # interpolate intensity to specific wavelengths
        cur_int_340 = np.interp(340., calib, cur_spec)/mfc_data['fExposureTime']
        cur_int_404 = np.interp(404.8, calib, cur_spec)/mfc_data['fExposureTime']
        cur_int_440 = np.interp(440., calib, cur_spec)/mfc_data['fExposureTime']
        int_340.append(cur_int_340)
        int_404.append(cur_int_404)
        int_440.append(cur_int_440)
        elevs.append(cur_elev)

    # PLOTTING
    # plot the intensities
    fig = plt.figure(figsize=(5.511811024, 4.133858268))
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(elevs, int_340, '.-', color='b', label='@ 340 nm')
    ax1.plot(elevs, int_404, '.-', color='m', label='@ 404.8 nm')
    ax1.plot(elevs, int_440, '.-', color='r', label='@ 440 nm')
    ax1.set_xlabel('Elevation [$^\circ$]')
    ax1.set_ylabel('Counts per s')
    # ax1.set_xticks(np.arange(-2., 3.5, 0.5))
    # ax1.set_xlim(-2.25, 3.25)
    ax1.grid(ls=':', lw=0.75)
    plt.legend(loc=2, numpoints=1)
    plt.tight_layout()
    # define outpath for the intensiy plot
    savepath = os.path.join(base_path, 'results')
    fig.savefig(os.path.join(savepath, 'intensity_{}_.png'.format(datenow)),
                format='png', dpi=300)
    plt.close()

    # plot the derivative of the intensity curve
    fig = plt.figure(figsize=(5.511811024, 4.133858268))
    ax1 = fig.add_subplot(1, 1, 1)
    # calculate the derivatives (simple Eulerian approach)and plot them
    # first for 340 nm
    int_change_340 = [(int_340[index+1] - int_340[index])/(elevs[index+1] - elevs[index])
                      for index in range(len(int_340)-1)]
    # fit Gaussian function to the derivatives
    fitParams_340, fitCovariances_340 = curve_fit(fitFunc,
                                                  elevs[0:len(int_change_340)],
                                                  int_change_340,
                                                  p0=(2.e5, 1.5, -0.2))
    fit_params_now.extend(fitParams_340)
    plot_elevs = np.arange(-3., 3.025, 0.025)
    fit_line_340 = fitFunc(plot_elevs, fitParams_340[0], fitParams_340[1],
                           fitParams_340[2])
    ax1.plot(plot_elevs, fit_line_340, '-', color='b', label='Fit @ 340 nm')
    ax1.plot(elevs[0:len(int_change_340)], int_change_340, '.', color='b',
             label='Data @ 340 nm')
    # same as above for 404 nm
    int_change_404 = [(int_404[index+1] - int_404[index])/(elevs[index+1] - elevs[index])
                      for index in range(len(int_404)-1)]
    fitParams_404, fitCovariances_404 = curve_fit(fitFunc, elevs[0:len(int_change_404)],
                                                  int_change_404, p0=(2.e5, 1.5, -0.2))
    fit_params_now.extend(fitParams_404)
    fit_line_404 = fitFunc(plot_elevs, fitParams_404[0], fitParams_404[1],
                           fitParams_404[2])
    ax1.plot(plot_elevs, fit_line_404, '-', color='m', label='Fit @ 404.8 nm')
    ax1.plot(elevs[0:len(int_change_404)], int_change_404, '.', color='m',
             label='Data @ 404.8 nm')
    # finally for 440 nm
    int_change_440 = [(int_440[index+1] - int_440[index])/(elevs[index+1] - elevs[index])
                      for index in range(len(int_440)-1)]
    fitParams_440, fitCovariances_440 = curve_fit(fitFunc, elevs[0:len(int_change_440)],
                                                  int_change_440, p0=(2.e5, 1.5, -0.2))
    fit_params_now.extend(fitParams_440)
    fit_line_440 = fitFunc(plot_elevs, fitParams_440[0], fitParams_440[1],
                           fitParams_440[2])
    ax1.plot(plot_elevs, fit_line_440, '-', color='r', label='Fit @ 440 nm')
    ax1.plot(elevs[0:len(int_change_440)], int_change_440, '.', color='r',
             label='Data @ 440 nm')
    
    # write the information of the centers into the plot
    ax1.text(0.1, 0.5, 'Center = %1.2f$^\circ$' % fitParams_340[1],
             transform=ax1.transAxes, fontsize=8, color='blue')
    ax1.text(0.1, 0.45, 'Center = %1.2f$^\circ$' % fitParams_404[1],
             transform=ax1.transAxes, fontsize=8, color='m')
    ax1.text(0.1, 0.4, 'Center = %1.2f$^\circ$' % fitParams_440[1],
             transform=ax1.transAxes, fontsize=8, color='red')
    ax1.annotate(single_date.strftime("%d-%m-%Y"), xy=(0.75, 0.9),
                 xycoords="axes fraction")
    ax1.set_xlabel('Elevation [$^\circ$]')
    ax1.set_ylabel('Counts per s')
    # ax1.set_xticks(np.arange(-2., 3.5, 0.5))
    # ax1.set_xlim(-2.25, 3.25)
    plt.legend(loc=2, numpoints=1)
    ax1.grid(ls=':', lw=0.75)
    # define the outpath for the derivative plot
    plt.tight_layout()
    fig.savefig(os.path.join(savepath, 'derivative_{}_.png'.format(datenow)),
                format='png', dpi=300)
    plt.close()
    fit_params.append(fit_params_now)

df = pd.DataFrame(columns=['Date', 'A_340', 'mu_340', 'sigma_340',
                           'A_404', 'mu_404', 'sigma_404',
                           'A_440', 'mu_440', 'sigma_440'])
df['Date'] = daterange
for i, col in enumerate(df.columns[1:]):
    df[col] = np.array(fit_params)[:, i]
#df.to_csv(os.path.join(base_path, 'summary_horizon_scan.csv'), index=False,
#          float_format='%.3f')
