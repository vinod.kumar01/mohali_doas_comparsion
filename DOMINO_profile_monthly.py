# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 11:44:52 2019
Script to get the mean monthly profile from DOMINO OMI data
@author: Vinod
"""
# %% imports
import os
import glob
import netCDF4
from datetime import datetime, timedelta
import numpy as np
from datetime import date
import pandas as pd
import matplotlib.pyplot as plt
import calendar
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# %% Function to get profiles
def get_profile(folder_name, foldername_cloud):
    f_name_sat = glob.glob(os.path.join(folder_name, 'OMI-Aura_L2-OMDOPROFILE_*.he5'))
    if f_name_sat==[]:
        tempdate=tempdate=[int(i) for i in folder_name.split('\\')[-3:]]
        temp_dt = datetime(tempdate[0], tempdate[1], tempdate[2], 6, 30)
        profile_ap = np.full(34, np.nan)
        Temp_ap = np.full(34, np.nan)
        AK_ap = np.full(34, np.nan)
        cf_ap = np.nan
        ch_ap = np.nan  
    profile_ap=[]
    Temp_ap = []
    AK_ap = []
    cf_ap = []
    ch_ap = []
    for filename in f_name_sat:
        profile_orb = []        # NO2 a priori profie
        AK_orb = []        # Averaging Kernel
        Temp_orb = []        # Temperature profie
        orbit_num = int(filename.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(filename, 'r')
        tempdate = filename.split('\\')[-1].split('_')[2]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[5:7])
        temp_day = int(tempdate[7:9])
        temp_hour = int(tempdate[10:12])
        temp_min = int(tempdate[12:14])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        lat_bound = [30.4, 30.9]
        lon_bound = [76.4, 77.0]
        try:
            swath = f.groups['HDFEOS'].groups['SWATHS'].groups['DominoNO2']
            Geoloc_fields = swath.groups['Geolocation Fields']
            lats = Geoloc_fields.variables['Latitude'][:]
            lons = Geoloc_fields.variables['Longitude'][:]
            Data_fields = swath.groups['Data Fields']
            TM4profile = Data_fields.variables['TM4profile']
            Tempprofile = Data_fields.variables['TemperatureProfile']
            keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
            keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
            if len(lats[keep])>0:
                print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                        + ' : ' + str(orbit_num))
                # get cloud informaion for data file of same orbit number
                filename_cloud = glob.glob(os.path.join(foldername_cloud,
                                                        '*OMI*'+str(orbit_num)+'*.he5'))
                try:
                    f_cloud = netCDF4.Dataset(filename_cloud[0], 'r')
                    swath_cloud = f_cloud.groups['HDFEOS'].groups['SWATHS']
                    Data_fields_cloud = swath_cloud.groups['DominoNO2'].groups['Data Fields']
                    cf_orb = Data_fields_cloud.variables['CloudFraction'][:]*0.001
                    AK = Data_fields_cloud.variables['AveragingKernel'][:]
                    cp = Data_fields_cloud.variables['CloudPressure'][:]
                    cf_orb = cf_orb[keep]
                    cp = cp[keep].astype('float32')
                    cp[cp<0] = np.nan
                    ch_orb = 8.5*np.log(970/cp)
                    f_cloud.close()
                except:
                    cf_orb = np.nan
                    ch_orb = np.nan
                for lev in range(34):
                    profile_orb.append(TM4profile[lev][keep])
                    AK_orb.append(AK[lev][keep])
                    Temp_orb.append(Tempprofile[lev][keep]*0.01)
                profile_orb = np.array(profile_orb)
                Temp_orb = np.array(Temp_orb)
                AK_orb = np.array(AK_orb)
                try:
                    profile_ap = np.concatenate((profile_ap,profile_orb), axis=1)
                    Temp_ap = np.concatenate((Temp_ap,Temp_orb), axis=1)
                    AK_ap = np.concatenate((AK_ap,AK_orb), axis=1)
                    cf_ap = np.concatenate((cf_ap,cf_orb), axis=1)
                    ch_ap = np.concatenate((ch_ap,ch_orb), axis=1)
                except:
                    profile_ap = profile_orb
                    Temp_ap = Temp_orb
                    AK_ap = AK_orb
                    cf_ap = cf_orb
                    ch_ap = ch_orb
    #        mean_profile = np.ma.mean(profile, 1)
            f.close()
        except(KeyError):
            print('no profile for relevant area for '+ temp_dt.strftime("%Y-%m-%d"))
            profile_ap = np.full(34, np.nan)
            Temp_ap = np.full(34, np.nan)
            AK_ap = np.full(34, np.nan)
            cf_ap = np.nan
            ch_ap = np.nan
    if len(profile_ap)==0:
        profile_ap = np.full(34, np.nan)
        Temp_ap = np.full(34, np.nan)
        AK_ap = np.full(34, np.nan)
        cf_ap = np.nan
        ch_ap = np.nan
    return(temp_dt, profile_ap, Temp_ap, AK_ap, cf_ap, ch_ap)

# %% loop through days and get an array of profiles
dirname = r'M:\nobackup\vinod\omi_profiles\extract'
dirname_cloud = r'M:\DATASETS\www.temis.nl\airpollution\no2col\data\omi\data_v2'
start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)
days = (end_date-start_date).days+1
profile_ts = []
AK_ts = []
date_time = []
temp_ts = []
cf_ts = []
ch_ts = []

daterange = pd.date_range(start_date, end_date)
for single_date in daterange:
    print('processing '+ single_date.strftime("%Y\%m\%d"))
    folder_name = os.path.join(dirname, single_date.strftime("%Y\%m\%d"))
    foldername_cloud = os.path.join(dirname_cloud, single_date.strftime("%Y\%m\%d"))
    single_day, profile_day, temp_day, AK_day, cf_day, ch_day = get_profile(folder_name, foldername_cloud)
    i=len(profile_day.shape)
    while i>1:
        profile_day = np.ma.mean(profile_day,1)
        temp_day = np.ma.mean(temp_day,1)
        AK_day = np.ma.mean(AK_day,1)
        cf_day = np.ma.mean(cf_day,0)
        ch_day = np.ma.mean(ch_day,0)
        i -= 1

    profile_ts.append(profile_day)
    temp_ts.append(temp_day)
    AK_ts.append(AK_day)
    cf_ts.append(cf_day)
    ch_ts.append(ch_day)
    date_time.append(single_day.date())

#profile_ts = np.ma.masked_array(profile_ts)

# %% save the daily profiles and temperatures as netCDF 
# Domino vertical grid definitios from a separate file
vlevs = netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\omi_domino_hy.nc',
                        'r') 
filename_out = 'DOMINO_profile_mohali_2013_17_inc_cloud_ak.nc'
out_dir = r'D:\postdoc\Sat\OMI\TS_mohali'
f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'w', format='NETCDF4')
f_out.createDimension('time', None)
f_out.createDimension('lev', 34)
f_out.createDimension('ilev', 35)
lev_out = f_out.createVariable('lev', 'int32', ('lev',))
ilev_out = f_out.createVariable('ilev', 'int32', ('ilev',))
hyam_out = f_out.createVariable('hyam', 'f4', ('lev',))
hybm_out = f_out.createVariable('hybm', 'f4', ('lev',))
hyai_out = f_out.createVariable('hyai', 'f4', ('ilev',))
hybi_out = f_out.createVariable('hybi', 'f4', ('ilev',))
time_out = f_out.createVariable('time', 'f4', ('time',))
NO2_out = f_out.createVariable('NO2',   'f4', ('time', 'lev'))
temp_out = f_out.createVariable('Temperature',   'f4', ('time', 'lev'))
AK_out = f_out.createVariable('Averaging_Kernel',   'f4', ('time', 'lev'))
cf_out = f_out.createVariable('CloudFraction',   'f4', ('time'))
ch_out = f_out.createVariable('CloudHeight',   'f4', ('time'))

lev_out[:] = np.arange(1, 35)
ilev_out[:] = np.arange(1, 36)
hyam_out[:], hyai_out[:] = vlevs.variables['hyam'][:], vlevs.variables['hyai'][:]
hyam_out.units = vlevs.variables['hyam'].units
hyai_out.units = vlevs.variables['hyai'].units
hyam_out.long_name = vlevs.variables['hyam'].long_name
hyai_out.long_name = vlevs.variables['hyai'].long_name
hybm_out[:], hybi_out[:] = vlevs.variables['hybm'][:], vlevs.variables['hybi'][:]
hybm_out.units = vlevs.variables['hybm'].units
hybi_out.units = vlevs.variables['hybi'].units
hybm_out.long_name = vlevs.variables['hybm'].long_name
hybi_out.long_name = vlevs.variables['hybi'].long_name
NO2_out[:] = np.array(profile_ts)
NO2_out.long_name='NO2 a priori sub Column'
NO2_out.units='molecules cm-2'
NO2_out.ScaleFactor=1E15
AK_out[:] = np.array(AK_ts)
AK_out.long_name='Averaging kernels'
AK_out.units='1'
NO2_out.ScaleFactor=0.001
temp_out[:] = np.array(temp_ts)
temp_out.long_name='Air Temperature'
temp_out.units='K'
cf_out[:] = np.array(cf_ts)
cf_out.long_name='Cloud Fraction'
cf_out.units='1'
ch_out[:] = np.array(ch_ts)
ch_out.long_name='Cloud Height'
ch_out.units='Km'
time_out[:] = np.arange(0, days, 1); time_out.long_name='time';time_out.units='days since 2013-01-01 00:00:00'
f_out.close()

# %% plotting
f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'r')
date_time = f_out.variables['time'][:]
date_time = [date(2015,1,1)+timedelta(days=float(i)) for i in date_time]
month = np.array([i.month for i in date_time])
prs_i = np.array([f_out.variables['hyai'][i]+(97000*f_out.variables['hybi'][i]) for i in range(0,35)])
temp = f_out.variables['Temperature'][:]
prs_i[prs_i<0]=np.nan
h_interface=np.array([7.4*np.log(97000/prs_i[i]) for i in range(0,35)])*1000 
h=np.array([h_interface[i]-h_interface[i-1] for i in range(1,35)])
h=h*temp[100,:]/250
z_centre = (h_interface[1:]+h_interface[:-1])/2 
z_centre=z_centre*temp[100,:]/250
no2_bvcd = f_out.variables['NO2'][:]*1E15
no2_conc = no2_bvcd/h/100
#cf = f_out.variables['Cloud Fraction'][:]
fig,ax = plt.subplots()
def plot_profile(ax):
    ax.set_color_cycle(['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
                        'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])
    for i in range(1,13):
        idx = (month==i)
        abs_profile = np.nanmean(no2_conc[idx, :16], axis=0)
        rel_profile = abs_profile/np.sum(abs_profile)
        ax.plot(rel_profile, z_centre[:16], '-+',
            label=calendar.month_abbr[i])
    ax.set_ylabel('Altitude (meters)')
    #ax.set_xlabel('NO$_{2}$ concentration (molecules cm$^{-3}$)')
    ax.set_xlabel('Fraction of NO$_{2}$ concentration')
    ax.legend(loc='upper right', ncol=2)
    ax.minorticks_on()
    ax.grid(alpha=0.4)
    ax.set_xlim(-0.01, 1)
    ax.set_ylim(0, 4000)
plot_profile(ax)
plt.tight_layout()
plt.savefig(os.path.join(out_dir, 'profile_cloud', 'montly_no2_profile_rel.png'), format='png')
