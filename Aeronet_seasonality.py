# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 08:29:42 2020

@author: Vinod
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decimal import Decimal
from scipy import stats


def add_fit_param(fit_param, pos_x, pos_y, ax='ax', color='k', showr=True):
    corr_coeff = "\n r = " + str('%.2f' % fit_param[2]) if showr else ''
    if fit_param[1] >= 0:
       ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x + " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color=color, size=12)
    else:
      ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x - " +
            str('%.2f' % Decimal(str(abs(fit_param[1])))) + corr_coeff,
            xy=(pos_x, pos_y), xycoords='axes fraction', color=color, size=12)


# %% load data
dirname = r'D:\postdoc\DOAS\Mohali_2013_17\Aeronet'
site = "Lahore"  # Lahore, New_Delhi
filename = site+"_le_20.dat"

df = pd.read_csv(os.path.join(dirname, site, filename),
                 sep=",", header=6)
df['date_time'] = df['Date(dd:mm:yyyy)'] + df['Time(hh:mm:ss)']
df['date_time'] = pd.to_datetime(df['date_time'], format="%d:%m:%Y%H:%M:%S")
date_min = df['date_time'][0].strftime('%b-%Y')
date_max = df['date_time'][df.index[-1]].strftime('%b-%Y')
df['month'] = [d.strftime('%b') for d in df['date_time']]
df['year'] = df['date_time'].dt.year
df = df.set_index('date_time')
df[df == -999] = np.nan

# %%plotting
#fig, axes = plt.subplots(figsize=[7, 4])
#order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
#         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
#
#sns.boxplot(x='month', y='AOD_440nm', data=df, showfliers=False, whis=[5, 95],
#            color="k", showmeans=True, order=order)
#for i, artist in enumerate(axes.artists):
#    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
#    col = artist.get_facecolor()
#    artist.set_edgecolor(col)
#    artist.set_facecolor('None')
#axes.set_ylabel('AOD (440nm)')
#axes.set_xlabel('Month of the year')
#axes.set_ylim(0, 2.5)
#axes.grid(axis='y', alpha=0.4)
#if len(list(set(df["year"]))) > 3:
#    axes.annotate("B) "+site+"\n"+date_min+" - "+date_max, xy=(0.05, 0.9),
#                  xycoords='axes fraction', color='k')
#else:
#    axes.annotate("B) " + site+"\n" + str(np.sort(list(set(df["year"])))),
#                  xy=(0.05, 0.9), xycoords='axes fraction', color='k')
#plt.tight_layout()
#plt.savefig(os.path.join(dirname, site+'.png'), format='png', dpi=300)
# %% load and compare MODIS AOD
dirname_sat = r'D:\postdoc\Sat\MODIS\TS_mohali'
f_name = 'aod_MODIS_360_TS_dm_1km_{}_cf.csv'.format(site)
df_sat = pd.read_csv(os.path.join(dirname_sat, f_name),
                     delimiter=',', header=0, escapechar='#')
df_sat.columns = df_sat.columns.str.strip()
df_sat['date_time'] = pd.to_datetime(df_sat['date_time'])
for cols in df_sat.columns[1:]:
    df_sat[cols] = pd.to_numeric(df_sat[cols], errors='coerce')
df_sat = df_sat.set_index('date_time')
df_sat['month'] = [d.strftime('%b') for d in df_sat.index]

# %% plotting
df1 = df[['AOD_440nm', 'month']]
df1 = df1[df1.index.hour.isin([7,8,4,5])]
df1['kind'] = 'Aeronet'
df1.reset_index(inplace=True)
df2 = df_sat[['vcd_440cf', 'month']]
df2.columns = ['AOD_440nm', 'month']
df2['kind'] = 'MODIS'
df2.reset_index(inplace=True)
df_merge = pd.concat((df1, df2), sort=False)
fig, axes = plt.subplots(figsize=[8, 4])
order = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
sns.boxplot(x='month', y='AOD_440nm', hue='kind', data=df_merge,
            showfliers=False, whis=[5, 95],
            palette=sns.color_palette(('r', 'k')),
            showmeans=True, order=order)
num_artists = len(axes.artists)
num_lines = len(axes.lines)
lines_per_artist = num_lines // num_artists
for i, artist in enumerate(axes.artists):
    # Set the linecolor on the artist to the facecolor, and set the facecolor to None
    col = artist.get_facecolor()
    artist.set_edgecolor(col)
    artist.set_facecolor('None')
    # set the marker colors of the corresponding "lines" to the same color
    for j in range(lines_per_artist):
        axes.lines[i * lines_per_artist + j].set_markerfacecolor(col)
        axes.lines[i * lines_per_artist + j].set_markeredgecolor(col)
axes.set_ylabel('AOD (440nm)')
axes.set_xlabel('Month of the year')
axes.set_ylim(0, 2.5)
axes.legend(loc=[0.25, 0.85])
axes.grid(axis='y', alpha=0.4)
if len(list(set(df["year"]))) > 3:
    axes.annotate("B) "+site+"\n"+date_min+" - "+date_max, xy=(0.02, 0.9),
                  xycoords='axes fraction', color='k')
else:
    axes.annotate("B) " + site+"\n" + str(np.sort(list(set(df["year"])))),
                  xy=(0.05, 0.9), xycoords='axes fraction', color='k')
# Also fix the legend
for legpatch in axes.get_legend().get_patches():
    col = legpatch.get_facecolor()
    legpatch.set_edgecolor(col)
    legpatch.set_facecolor('None')
plt.tight_layout()
#plt.savefig(os.path.join(dirname, site+'_inc_modis.png'), format='png', dpi=300)

# %% scatter plot
df1_day = df1.set_index('date_time').resample('1D').mean()
df2_day = df2.set_index('date_time').resample('1D').mean()
df_merge = df1_day.reindex(df2_day.index)
df_merge['MODIS_AOD'] = df2_day['AOD_440nm']
fig, ax = plt.subplots(figsize=[8, 4])
ax.scatter(df_merge['MODIS_AOD'], df_merge['AOD_440nm'])
ax.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c=".3")
ax.set_ylim(0, 3)
ax.set_xlim(0, 3)
ax.grid(alpha=0.3)
mask = df_merge['MODIS_AOD'].isnull() | df_merge['AOD_440nm'].isnull()
fit_param = stats.linregress(df_merge['MODIS_AOD'][~mask],
                             df_merge['AOD_440nm'][~mask])[0:3]
add_fit_param(fit_param, pos_x=0.02, pos_y=0.7, ax=ax,
              color='k', showr=True)
ax.set_ylabel('MODIS AOD (440nm)')
ax.set_xlabel('AERONET AOD (440nm)')
ax.annotate(site, xy=(0.02, 0.9), size=14,
                  xycoords='axes fraction', color='k')
plt.tight_layout()
#plt.savefig(os.path.join(dirname, site+'_modis_aeronet_scatter.png'),
#            format='png', dpi=300)
