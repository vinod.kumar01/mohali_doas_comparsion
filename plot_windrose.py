# -*- coding: utf-8 -*-
"""
Created on Mon Dec 16 14:13:24 2019

@author: Vinod
"""

from windrose import WindroseAxes
import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd
from datetime import timedelta
import calendar


def windrose(ws, wd):
    bins_range = np.arange(0, 13, 3)
    wax = WindroseAxes.from_ax()
    wax.bar(wd, ws, normed=True, bins=bins_range, cmap=plt.get_cmap('cividis'))
    wax.set_ylim(0, 30)
    wax.set_yticks(np.arange(10, 30, step=5))
    wax.set_yticklabels(np.arange(10, 30, step=5), size=16, weight='bold')
    wax.set_xticklabels(["E", "N-E", "N", "N-W", "W", "S-W", "S", "S-E"],
                        size=16, weight='bold')
    L = wax.legend(loc=[-0.12, -0.12], fontsize='xx-large', fancybox=True)
    L.set_title('Wind speed (m s$^{-1}$)', prop={'size': 16})
    bins = wax._info['bins']
    fracs = wax._info['table']
    L.get_texts()[-1].set_text('> %.1f' % (bins[-2]))
    return wax, bins, fracs


# %% Import data 
insitu_path = r'D:\postdoc\DOAS\Mohali_2013_17'
insitu_file = os.path.join(insitu_path, 'in_situ.csv')
colnames = ['desttime', 'm31ptc', 'no', 'no2', 'nox', 'o3', 'Wind_Speed',
            'Wind_Direction', 'Amb_temp', 'Humidity', 'Solar_Radiation',
            'abs_humidity', 'hcho']
insitu_data = pd.read_csv(insitu_file, dtype=None, delimiter=',', header=0,
                          keep_default_na=False, names=colnames)
insitu_data['desttime'] = pd.to_datetime(insitu_data['desttime'],
                                         format='%d-%m-%Y %H:%M:%S')
insitu_data['desttime'] -= timedelta(minutes=330)  # offset for IST
insitu_data['year'] = insitu_data['desttime'].dt.year
insitu_data['month'] = insitu_data['desttime'].dt.month
for cols in colnames[1:]:
    insitu_data[cols] = pd.to_numeric(insitu_data[cols], errors='coerce')
# %%plot wind rose
#wind_dir = np.array([30,45,90,43,180])
#wind_sd = np.arange(1,wind_dir.shape[0]+1)
#bins_range = np.arange(1,6,1) # this sets the legend scale

year = [2012,2013,2014,2015,2016,2017]
month = [11, 12, 1, 2]
sel = (insitu_data['month'].isin(month)) & (insitu_data['year'].isin(year))
sel &= (insitu_data['Solar_Radiation']>100)
df = insitu_data[sel]
#print('Daytime mean tempearture of '+calendar.month_abbr[month]+ ' ' + str(year) + ' is '+ str(df['Amb_temp'].mean()))
#
#sel = (insitu_data['month']==month) & (insitu_data['year']==year) & (insitu_data['Solar_Radiation']<40)
#df = insitu_data[sel]
#print('Nighttime mean tempearture of '+calendar.month_abbr[month]+ ' ' + str(year) + ' is '+ str(df['Amb_temp'].mean()))

wax,bins,fracs = windrose(df['Wind_Speed'], df['Wind_Direction'])
#bins_range = np.arange(0,13,3)
#wax = WindroseAxes.from_ax()
#wax.bar(df['Wind_Direction'],df['Wind_Speed'],normed=True,bins=bins_range)
#wax.set_ylim(0, 50)
#wax.set_yticks(np.arange(10, 50, step=10))
#wax.set_yticklabels(np.arange(10, 50, step=10))
#wax.legend(loc=[-0.1, -0.1])
#wax.text(0.9, 0.9, 'November\n'+str(year), transform=wax.transAxes, size=12)