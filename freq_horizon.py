# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 18:48:58 2020

@author: Vinod
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd



df1 = pd.read_csv(r'D:\wat\minimax\doas_2014\horizon_scan\summary_horizon_scan.csv')
df2 = pd.read_csv(r'D:\wat\minimax\doas_2015\horizon_scan\summary_horizon_scan.csv')
df3 = pd.read_csv(r'D:\wat\minimax\doas_2015_2\horizon_scan\summary_horizon_scan.csv')
df4 = pd.read_csv(r'D:\wat\minimax\doas_2015_3\horizon_scan\summary_horizon_scan.csv')
df5 = pd.read_csv(r'D:\wat\minimax\DOAS_2016\horizon_scan\summary_horizon_scan.csv')
df6 = pd.read_csv(r'D:\wat\minimax\DOAS_2017\horizon_scan\summary_horizon_scan.csv')
df = pd.concat([df1, df2, df3, df4, df5, df6])
df['Date'] = pd.to_datetime(df['Date'], format='%d-%m-%Y')
n_bins = np.arange(-0.4,0.41,0.1) 
count_340, bins = np.histogram(df['mu_340'].dropna(), n_bins)
count_404, bins = np.histogram(df['mu_404'].dropna(), n_bins)
count_440, bins = np.histogram(df['mu_440'].dropna(), n_bins)

fig, ax = plt.subplots(figsize=[9, 4])
ax.plot(df['Date'], df['mu_340'], label='340nm')
ax.plot(df['Date'], df['mu_404'], label='404nm')
ax.plot(df['Date'], df['mu_440'], label='440nm')
ax.set_ylabel('Horizon position [$^\circ$]', size=14)
ax.legend(prop={'size': 14})
ax.grid(alpha=0.4)
plt.tight_layout()
#plt.savefig(r'D:\postdoc\DOAS\Mohali_2013_17\horizon_scan_ts.png',
#            format='png', dpi=300)

fig, ax = plt.subplots(figsize=[8, 5])
ax.bar(n_bins[1:]-0.02, count_340, width=0.02, label='340 nm')
ax.bar(n_bins[1:], count_404, width=0.02, label='404 nm')
ax.bar(n_bins[1:]+0.02, count_440, width=0.02, label='440 nm')
ax.legend(loc='upper left', prop={'size': 14})
ax.set_ylabel('Frequency', size=14)
ax.set_xlabel('Horizon position [$^\circ$]', size=14)
ax.grid(alpha=0.4)
plt.tight_layout()
#plt.savefig(r'D:\postdoc\DOAS\Mohali_2013_17\horizon_scan_freq_new.png',
#            format='png', dpi=300)
