# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 14:27:01 2020

@author: Vinod
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

dirname_sat = r'D:\postdoc\Sat\MODIS\TS_mohali'
f_name = "aod_MODIS_360_TS_dm_1km_cf.csv"
df_sat = pd.read_csv(os.path.join(dirname_sat, f_name),
                     delimiter=',', header=0, escapechar='#')
# remove excess space from headers
df_sat.columns = df_sat.columns.str.strip()
# get the data formats right
df_sat['date_time'] = pd.to_datetime(df_sat['date_time'])
for cols in df_sat.columns[1:]:
    df_sat[cols] = pd.to_numeric(df_sat[cols], errors='coerce')
# Resample at different frequencies : Daily/Monthly
df_sat['month'] = [d.strftime('%b') for d in df_sat['date_time']]
df_sat = df_sat.set_index('date_time')

df_sat['AE'] = -np.log(df_sat['vcd470_cf']/df_sat['vcd550_cf'])/np.log(470/550)
df_sat['AE_old'] = df_sat['vcd470_cf']/df_sat['vcd550_cf']



#df_sat2 = pd.read_csv(os.path.join(dirname_sat, f_name2),
#                     delimiter=',', header=0, escapechar='#')
## remove excess space from headers
#df_sat2.columns = df_sat2.columns.str.strip()
## get the data formats right
#df_sat2['date_time'] = pd.to_datetime(df_sat2['date_time'])
#for cols in df_sat2.columns[1:]:
#    df_sat2[cols] = pd.to_numeric(df_sat2[cols], errors='coerce')
## Resample at different frequencies : Daily/Monthly
#df_sat2 = df_sat2.set_index('date_time')



fig, ax = plt.subplots(figsize=[9,4])
sns.boxplot(x='month', y='AE', data=df_sat, showfliers = False, whis=[5, 95],
            color="w", showmeans=True)
ax.set_ylabel(r'Ångström Exponent')
ax.set_xlabel("Month of the year")
ax.grid(axis='y', alpha=0.4)
plt.tight_layout()
#plt.savefig(os.path.join(dirname_sat, "AE_seasonality.png"), format="png", dpi=300)

#axes[0].set_title('Year-wise Box Plot\n(The Trend)', fontsize=18)
#plt.show()
#axes[1].set_title('Month-wise Box Plot\n(The Seasonality)', fontsize=18)