# -*- coding: utf-8 -*-
"""
Created on Sun Aug 25 16:28:04 2019

@author: Vinod
"""

import netCDF4
from datetime import date
import glob
import os
from datetime import datetime
import numpy as np
import pandas as pd

def AOD_TS(filename):
    f = netCDF4.Dataset(filename, 'r')
    tempdate = filename.split('\\')[-1].split('_')[2]
    temp_year = int(tempdate[0:4])
    temp_month = int(tempdate[5:7])
    temp_day = int(tempdate[7:9])
    temp_hour = int(tempdate[10:12])
    temp_min = int(tempdate[12:14])
    temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
    lat_bound = [30.4, 30.9]
    lon_bound = [76.4, 77.0]
    try:
        swath = f.groups['HDFEOS'].groups['SWATHS'].groups['Aerosol NearUV Swath']
        Geoloc_fields = swath.groups['Geolocation Fields']
        lats = Geoloc_fields.variables['Latitude'][:]
        lons = Geoloc_fields.variables['Longitude'][:]
        Data_fields = swath.groups['Data Fields']
        AOD_354 = Data_fields.variables['FinalAerosolOpticalDepth']
        flag = Data_fields.variables['PixelQualityFlags'][:][:,:,0]
    #    fv = AOD_354._FillValue  
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        AOD_354 = AOD_354[:][:,:,0]
        AOD_354 = AOD_354[keep]
    #    AOD_354[AOD_354 == fv] =  np.nan
        flag = flag[keep]
        AOD_354 = np.ma.masked_where(flag!=0, AOD_354)
        tempaod = np.ma.mean(AOD_354)
        f.close()
    except(KeyError):
        tempaod = np.nan
    return(temp_dt, tempaod)


dirname = r'M:\nobackup\vinod\OMI_AOD\raw'

f_name_sat = glob.glob(os.path.join(dirname,
                                    'OMI-Aura_L2-OMAERUV_' +  '*.he5'))
f_name_sat.sort(key=os.path.basename)

counter = 0
date_time = np.empty(len(f_name_sat), dtype='object')
AOD_Loc = np.empty(len(f_name_sat), dtype='float')
for filename in f_name_sat:
    tempts = AOD_TS(filename)
    date_time[counter] = tempts[0]
    AOD_Loc[counter] = tempts[1]
    counter = counter+1
    
df = pd.DataFrame(np.column_stack((date_time, AOD_Loc)),
                   columns=['Date_time', 'OMI_AOD'])
df.to_csv(os.path.join(dirname, "AOD_OMI_ts.csv"), index=False)