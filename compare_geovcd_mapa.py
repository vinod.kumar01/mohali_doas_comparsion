# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 13:19:54 2019

@author: Vinod
"""

# %% definitions and imports
import numpy as np
import os
from scipy import stats
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import import_merge_mapa_out as mapa
from scipy.interpolate import interpn
from decimal import Decimal

def interpolate_cloud(ref_time, cloud_inp):
    cloud_inp = cloud_inp.set_index('Date_time')
    cloud_data_intp = cloud_inp.reindex(ref_time, method='nearest', limit=1)
    return(cloud_data_intp['cloud_type'], cloud_data_intp['fog'],
           cloud_data_intp['Thick clouds_rad'])

def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs )   :
    """
    Scatter plot colored by 2d histogram
    """
    if ax is None :
        fig , ax = plt.subplots()
    data, x_e, y_e = np.histogram2d(x, y, bins = bins)
    z = interpn((0.5*(x_e[1:] + x_e[:-1]), 0.5*(y_e[1:]+y_e[:-1])), data,
                np.vstack([x,y]).T, method="splinef2d", bounds_error=False )

    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    scat = ax.scatter( x, y, c=z, **kwargs )
    return scat, ax, z
# %% load DOAS mapa data
# #NO2
Inp = mapa.load_plot('no2', 'no2_vis', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
                     -0.25e16, 2e16, 'OMI', 'DOMINO', 'vcd_weighted_mean', 1e15, sf='0.8')
#Inp = mapa.load_plot('no2', 'no2', 'NO$_{2}$ VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 2e16, 'OMI', 'DOMINO', 'vcd_weighted_mean', 1e15, sf='0.8')
#Inp = mapa.load_plot('hcho', 'hcho', 'HCHO VCD (molecules cm$^{-2}$)',
#                     -0.25e16, 6e16, 'OMI', 'QA4ECV', 'vcd_weighted_mean', sf='0.8')
mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v4'
mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v4'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\no2_uv_modflag_v4'
vcd_mean, z, flag, profile_mean, h, s, time2 = Inp.load_data(mapa_inpath, mapa_outpath)
print('loaded MAPA data')
vcd_mean, flag = np.array(vcd_mean), np.array(flag)
time2 = np.array(time2)
vcd_geo, sza, time_geo = Inp.get_geo_vcd(mapa_inpath, mapa_outpath, geo_angle=15)
print('loaded DOAS geometric VCD data')
vcd_geo = np.array(vcd_geo)
# %% Load external cloud classification data
#  and remove data affected by thick clouds and fig
cloud_inp = os.path.join(r'D:\wat\minimax\Mohali_cloud_classification\results',
                         'cloud_classified_rad.csv')
cloud_dat = pd.read_csv(cloud_inp, header=0, sep=',')
cloud_dat['Date_time'] = pd.to_datetime(cloud_dat['Date_time'], format='%Y%m%d%H%M%S')
cloud_type, fog, OT_cloud = interpolate_cloud(time2, cloud_dat)
vcd_mean[OT_cloud==1] = np.nan
vcd_mean[fog==1] = np.nan
#vcd_mean[cloud_type>3] = np.nan
## %% data filers 
##flag_remove = (flag!=0) & (OT_cloud==1) &  (fog==1)#flag!=0 # flag==2
#flag_remove = (flag!=0) | (OT_cloud==1) | (fog==1) | (cloud_type!=1)
#
#vcd_mean[flag_remove] = np.nan
#vcd_geo[flag_remove] = np.nan

# %% plots
#fig,ax = plt.subplots(figsize=[12,4])
#ax.scatter(time2, vcd_mean, s=2, label='MAPA sf='+Inp.sf, c='k')
#ax.scatter(time2, vcd_geo, s=2, label='Geometric (vis)', c='r')
#ax.set_xlim(pd.to_datetime('11-15-2012'), pd.to_datetime('06-15-2017'))
#ax.set_ylim(Inp.y_min, Inp.y_max)
#ax.xaxis.set_major_locator(mdates.MonthLocator(interval=6))
#ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=1))
#ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m"))
#plt.tight_layout()
#ax.legend(loc='upper right')

# %% scatter plot as frequency distribution
flag_remove = (flag!=0) | (OT_cloud==1) | (fog==1) | (cloud_type!=1)
x = vcd_mean[~flag_remove]
# cloud classification has not been applied on 
y = vcd_geo[~flag_remove]
idx = np.isfinite(x) & np.isfinite(y)
fig, ax = plt.subplots()
def plot_scatter(ax):
    scat = ax.scatter(x[idx], y[idx], s=2)
    ax.set_ylim(-2E15, 3e16)
    ax.set_xlim(-2E15, 3e16)
    ax.set_ylabel('NO$_{2}$ Geometric VCD (VIS) (molecules cm$^{-2}$)')
    ax.set_xlabel('NO$_{2}$ VIS VCD (MAPA) (molecules cm$^{-2}$)')
    fit_param = stats.linregress(x[idx], y[idx])[0:3]
    fit_func = np.poly1d(fit_param[0:2])
    ax.plot(x[idx], fit_func(x[idx]), color="k", ls="--",
            linewidth=1)
    ax.plot(ax.get_xlim(), ax.get_ylim(), ls=":", c=".3")
    if fit_param[1] >= 0:
       ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x + " +
            str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
            str('%.2f' % fit_param[2]), xy=(0.1, 0.8), xycoords='axes fraction', color = 'k')
    else:
      ax.annotate("y = " + str('%.2f' % fit_param[0]) + "x - " +
            str('%.2E' % Decimal(str(abs(fit_param[1])))) + "\n r = " +
            str('%.2f' % fit_param[2]), xy=(0.1, 0.8), xycoords='axes fraction', color = 'k')
    return scat
scat = plot_scatter(ax)
plt.tight_layout()
#scat = ax.hexbin(x[idx], y[idx], mincnt=1, bins=100)
#scat, ax, z = density_scatter(x[idx], y[idx], ax=ax, sort=True, bins=20, s=2)
#fig.subplots_adjust(right=0.8)
#cbar_ax = fig.add_axes([0.85, 0.15, 0.02, 0.7])
#cbar = fig.colorbar(scat, cax=cbar_ax, extend="max")
#cbar.set_label('Frequency', labelpad=2, size=15)
#plt.savefig(os.path.join(r'D:\postdoc\DOAS\Mohali_2013_17\geometric', 'scat_sf'+
#                         Inp.sf+ Inp.tracer+'_vis_mapa_clear_warning.png'), format='png', dpi=300)