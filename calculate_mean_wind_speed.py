# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 12:12:55 2019

@author: Vinod
"""

import numpy as np


def windmean(wind_speed, wind_direction):
    wd = np.deg2rad(wind_direction)
    x = -1*np.sin(wd)*wind_speed
    y = -1*np.cos(wd)*wind_speed
    mean_x = np.nanmean(x)
    mean_y = np.nanmean(y)
    mean_ws = np.sqrt(mean_x**2+mean_y**2)
    mean_wd = np.rad2deg(np.arctan(mean_x/mean_y))  
#    mean_wd = np.rad2deg(np.arctan2(mean_x, mean_y))
    mean_wd = (360 + mean_wd) % 360
    return mean_ws, mean_wd



        