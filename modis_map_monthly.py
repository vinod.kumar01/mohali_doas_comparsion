# -*- coding: utf-8 -*-
"""
Created on Sat Oct  5 23:37:53 2019

@author: Vinod
"""

import os
import glob
import numpy as np
import netCDF4
import cartopy as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import calendar
basedir = r'M:\nobackup\vinod\MODIS_AOD\raw\l2_1km\Mohali\2013_17\month_mean'

def mean_aod(month):
    f_names = glob.glob(os.path.join(basedir, 'MODIS_AOD*'+str(month).zfill(2)+'_cf.nc'))
    aod_mean = []
    for files in f_names:
        with netCDF4.Dataset(files, 'r') as f:
            aod_mean.append(f.variables['AOD_470'][:])
            lats = f.variables['lat'][:]
            lons = f.variables['lon'][:]
    aod_mean = np.ma.masked_array(aod_mean)
    aod_mean = np.ma.mean(aod_mean, 0)
    return(aod_mean, lats, lons)


plt.ioff()
fig,ax = plt.subplots(3,4, subplot_kw=dict(projection=ccrs.crs.PlateCarree()),
                      figsize=[22,13], sharex=True, sharey=True)
loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'},
       {'lon': 75.86, 'lat': 30.90, 'site': 'Ludhiana'},
       {'lon': 76.93, 'lat': 30.83, 'site': 'Kalka'},
       {'lon': 76.79, 'lat': 30.96, 'site': 'Baddi'},
       {'lon': 76.37, 'lat': 31.38, 'site': 'Nangal'}]
for i in range(0,3):
    for j in range(0,4):
        month = (4*i)+j+1
        aod, lat, lon = mean_aod(month)
        ax[i,j].set_extent([75.6, 77.6, 29.7, 31.7])
        gl = ax[i,j].gridlines(crs=ccrs.crs.PlateCarree(), draw_labels=True,
                            linewidth=1, color='gray', alpha=0.5, linestyle='--')
        gl.xlabels_top = False
        gl.ylabels_right = False
        ax[i,j].coastlines(color='black', linewidth=1, resolution='50m')
        ax[i,j].add_feature(cfeature.BORDERS.with_scale('50m'),
                         linestyle='-', alpha=.5)
        ax[i][j].text(0.05, 0.9, calendar.month_abbr[month],
                         transform=ax[i][j].transAxes)
        cs = ax[i,j].scatter(lon, lat, c=aod, cmap='jet',
                            vmin = 0, vmax = 1.5,
                            transform=ccrs.crs.PlateCarree())
        ax[i,j].add_patch(Rectangle((76.479,30.417), 0.5, 0.5, fill=False, color = 'k'))
#        ax[i,j].add_patch(Rectangle((76.63,30.57), 0.2, 0.2, fill=False, color = 'k'))
        print(calendar.month_abbr[month])
        for point in loc:
            lo, la = point['lon'], point['lat']
            if point['site']=='Mohali':
                ax[i,j].plot(lo, la, 'ko')
            elif point['site']=='Ludhiana':
                ax[i,j].plot(lo, la, 'k*')
            else:
                ax[i,j].plot(lo, la, 'kx')

plt.tight_layout()           
fig.subplots_adjust(right=0.88, wspace = 0.02)
cbar_ax=fig.add_axes([0.9, 0.15, 0.02, 0.7])
cbar=fig.colorbar(cs, cmap=plt.cm.get_cmap('jet'), cax = cbar_ax, extend='max')
cbar.set_label('Aerosol optical thickness (470nm)', fontsize=14)
plt.style.use('ggplot')
plt.savefig(r'D:\postdoc\DOAS\Mohali_2013_17\aod_470_MODIS_season_map_patch.png', format='png', dpi=300)
plt.ion()
#plt.close()

