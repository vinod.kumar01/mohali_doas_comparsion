# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 11:16:03 2020

@author: Vinod
"""

import pandas as pd
import os
import glob
import numpy as np

dirname = r'M:\nobackup\vinod\Mohali_DOAS_results\Auto\NO2'
fnames = glob.glob(os.path.join(dirname, '*.ASC'))
counter = 0
for fname in fnames:
    print(os.path.basename(fname))
    if counter == 0:
        df = pd.read_csv(fname, sep='\t')
    else:
        df = df.append(pd.read_csv(fname, sep='\t'))
    counter += 1
df = df.loc[~df['Name'].isin([0, 1, 90])]
df = df.loc[df['SZA'] < 85]
df[df == df['HCHO.RMS'].max()] = np.nan
df.hist(column='HCHO.RMS',
        bins=[3e-4,4e-4,5e-4,6e-4,7e-4,8e-4,9e-4,1e-3,1.1e-3,1.2e-3,1.3e-3])