# -*- coding: utf-8 -*-
"""
Created on Tue Nov 26 16:10:00 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import pandas as pd
import sys
from datetime import date, datetime
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# %% define limits and path
pos = Point(30.667, 76.729)

lat_bound = [30.57, 30.77]
lon_bound = [76.63, 76.83]
    
basedir = r"M:\DATASETS\OMI_HCHO\QA4ECV\l2"
vcd_ts = []
vcd_ts_std = []
vcd_ts_cf = []
cf_ts = []
date_time = []

start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(basedir,
                                        single_date.strftime("%Y\%m\%d"),
                                        'QA4ECV_L2_HCHO_OMI_*.nc'))
    vcd_day = []
    vcd_day_cf = []
    cf_day = []
    for path_sat in f_name_sat:
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        tempdate = path_sat.split('\\')[-1].split('_')[4]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[4:6])
        temp_day = int(tempdate[6:8])
        temp_hour = int(tempdate[9:11])
        temp_min = int(tempdate[11:13])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        Data_fields = f.groups['PRODUCT']
        lats = Data_fields.variables['latitude'][0, :]
        lons = Data_fields.variables['longitude'][0, :]
        HCHO_tropcol = Data_fields.variables['tropospheric_hcho_vertical_column']
        INPUT_fields = f.groups['PRODUCT'].groups['SUPPORT_DATA'].groups['INPUT_DATA']
        cf = INPUT_fields.variables['cloud_fraction'][0, :]
        flag = Data_fields.variables['processing_error_flag'][0, :]
        row_flag = INPUT_fields.variables['omi_xtrack_flags'][0, :]
        fv = HCHO_tropcol._FillValue
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        keep &= (flag==0) & (row_flag==0)
        geoloc = Data_fields.groups['SUPPORT_DATA'].groups['GEOLOCATIONS']
        lat_bnds = geoloc.variables['latitude_bounds'][0, :]
        lon_bnds = geoloc.variables['longitude_bounds'][0, :]

        # filter along a larger area to reduction further calculation
        if len(lats[keep]) > 0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                + ' : ' + str(orbit_num))
            lat_bnds = lat_bnds[keep, :]
            lon_bnds = lon_bnds[keep, :]
            HCHO_tropcol = HCHO_tropcol[0, :]
            HCHO_tropcol[HCHO_tropcol==fv] = np.nan
            HCHO_tropcol = HCHO_tropcol[keep]
            cf = cf[keep]
            HCHO_tropcol_cf = HCHO_tropcol.copy()
            HCHO_tropcol_cf[(cf >= 0.3)] = np.nan
#            break
            for idx in range(len(lat_bnds)):
                polygon = Polygon([(lat_bnds[idx][0], lon_bnds[idx][0]),
                                   (lat_bnds[idx][1], lon_bnds[idx][1]),
                                   (lat_bnds[idx][2], lon_bnds[idx][2]),
                                   (lat_bnds[idx][3], lon_bnds[idx][3])])
                if(polygon.contains(pos)):
                    vcd_day.append(HCHO_tropcol[idx])
                    vcd_day_cf.append(HCHO_tropcol_cf[idx])
                    cf_day.append(cf[idx])
#                    print(idx)

        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_daymean_cf = np.nanmean(vcd_day_cf)
        vcd_daystd = np.nanstd(vcd_day)
        cf_daymean = np.nanmean(cf_day)
    else:
        vcd_daymean = np.nan
        vcd_daymean_cf = np.nan
        vcd_daystd = np.nan
        cf_daymean = np.nan

    vcd_ts.append(vcd_daymean)
#    vcd_corr_ts.append(vcd_corr_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf.append(vcd_daymean_cf)
    cf_ts.append(cf_daymean)
    date_time.append(single_date.strftime("%Y-%m-%d"))
omi_ts = np.vstack((date_time, vcd_ts, vcd_ts_std, vcd_ts_cf,  cf_ts)).T
np.savetxt(r"D:\postdoc\Sat\OMI\hcho_OMI_QA4ECV_coloc.csv", omi_ts,
           delimiter=",", header = 'date_time, vcd, vcd_std, vcd_cf, vcd_cf_std, cloud_fraction',
           fmt="%s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log.txt', 'w')