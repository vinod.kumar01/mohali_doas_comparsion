# -*- coding: utf-8 -*-
'''
Created on 13-Nov-2018

@author: Vinod
'''
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import cartopy as ccrs
import cartopy.feature as cfeature


path = r'D:\postdoc\DOAS\Mohali_2013_17\firecount\DL_FIRE_M6_92682'
filename = 'fire_archive_M6_92682.csv'
in_file = os.path.join(path, filename)
df = pd.read_csv(in_file, dtype=None, delimiter=',', header=0)
df['acq_date'] = pd.to_datetime(df['acq_date'], format='%Y-%m-%d')

def get_count(daf):
    param = 'firecount'   # brightness,frp,firecount,bright_t31
    if "fire_archive_V1" in filename:
        daf = daf[daf['confidence'] == "h"]
    else:
        confidence = 80
        daf = daf[daf['confidence'] >= confidence]
    lat_st = int(np.min(daf['latitude'][:]))
    lat_end = int(np.max(daf['latitude'][:]))
    lat_end = 32
    lon_st = int(np.min(daf['longitude'][:]))
    lon_end = int(np.max(daf['longitude'][:]))
    resol_lat = 0.25  # degrees
    resol_lon = 0.25  # degrees
    lat_length = (lat_end-lat_st)/resol_lat
    lon_length = (lon_end-lon_st)/resol_lon
    rast = np.zeros((int(lat_length), int(lon_length)))
    lat = np.zeros((int(lat_length)))
    lon = np.zeros((int(lon_length)))
    counter_lat = 0
    counter_lon = 0
    for lats in np.arange(lat_st, lat_end, resol_lat):
        for lons in np.arange(lon_st, lon_end, resol_lon):
            cond = (daf['latitude'] >= lats)
            cond &= (daf['latitude'] < (lats + resol_lat))
            cond &= (daf['longitude'] >= lons)
            cond &= (daf['longitude'] < (lons + resol_lon))
            if param == 'firecount':
                rast[counter_lat, counter_lon] = len(daf[cond])
            else:
                rast[counter_lat, counter_lon] = np.nanmean(daf[cond][param])
            lon[counter_lon] = lons
            counter_lon = counter_lon+1
        lat[counter_lat] = lats
        counter_lat = counter_lat+1
        counter_lon = 0
    return lat,lon,rast
loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'},
       {'lon': 75.86, 'lat': 30.90, 'site': 'Ludhiana'},
       {'lon': 76.93, 'lat': 30.83, 'site': 'Kalka'},
       {'lon': 76.79, 'lat': 30.96, 'site': 'Baddi'},
       {'lon': 76.37, 'lat': 31.38, 'site': 'Nangal'}]
# %% plotting
fig,ax = plt.subplots(2,2, subplot_kw=dict(projection=ccrs.crs.PlateCarree()),
                      figsize=[8,6],sharex=True, sharey=True)
def plot_fire(ax):
    ax.set_extent([73, 79, 27, 32])
    gl = ax.gridlines(crs=ccrs.crs.PlateCarree(), draw_labels=True,
                        linewidth=1, color='gray', alpha=0.5, linestyle='--')
    gl.xlabels_top = False
    gl.ylabels_right = False
    ax.coastlines(color='black', linewidth=1, resolution='50m')
    ax.add_feature(cfeature.BORDERS.with_scale('50m'),
                     linestyle='-', alpha=.5)
    ax.text(0.72, 0.9, "October\n"+str(year), transform=ax.transAxes)
    llat,llon = np.meshgrid(lat,lon)
    cs = ax.pcolormesh(llon, llat, rast.T, cmap='Reds',
                        vmin = 0, vmax = 50,
                        transform=ccrs.crs.PlateCarree())
    for point in loc:
        lo, la = point['lon'], point['lat']
        if point['site']=='Mohali':
            ax.plot(lo, la, 'ko')
    return gl,cs
years = [2013, 2014, 2015, 2016]
axs = [ax[0,0], ax[0,1], ax[1,0], ax[1,1]]
for year,ax in zip(years,axs):
    sel = (df['acq_date'].dt.year==year) & (df['acq_date'].dt.month==10)
    lat,lon,rast = get_count(df[sel])
    gl,cs=plot_fire(ax)
plt.tight_layout()
fig.subplots_adjust(right=0.88, wspace = 0.02)
cbar_ax=fig.add_axes([0.9, 0.15, 0.02, 0.7])
cbar=fig.colorbar(cs, cmap=plt.cm.get_cmap('jet'), cax = cbar_ax, extend='max')
cbar.set_label('Fire counts', fontsize=12)