# -*- coding: utf-8 -*-
"""
Created on Tue Sep 24 09:40:24 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
from datetime import date
import pandas as pd
import multiprocessing as mp
from multiprocessing import Process

def extract_day(single_date):
    f_name_sat = glob.glob(os.path.join(basedir,
                                        single_date.strftime("%Y\%m\%d"),
                                        'QA4ECV_L2_HCHO_OMI_*.nc'))
    vcd_day = []
    vcd_day_cf = []
    for path_sat in f_name_sat:
        f = netCDF4.Dataset(path_sat, 'r')
        Data_fields = f.groups['PRODUCT']
        lats = Data_fields.variables['latitude'][:]
        lons = Data_fields.variables['longitude'][:]
        HCHO_tropcol = Data_fields.variables['tropospheric_hcho_vertical_column']
        INPUT_fields = f.groups['PRODUCT'].groups['SUPPORT_DATA'].groups['INPUT_DATA']
        cf = INPUT_fields.variables['cloud_fraction'][:]
        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        HCHO_tropcol = HCHO_tropcol[:]
        HCHO_tropcol = HCHO_tropcol[keep]
        cf = cf[keep]
        HCHO_tropcol_cf = HCHO_tropcol[(cf < 0.3)]
        if len(HCHO_tropcol > 0):
            vcd_day.extend(HCHO_tropcol)
        if len(HCHO_tropcol_cf > 0):
            vcd_day_cf.extend(HCHO_tropcol_cf)
        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))

    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_daystd = np.nanstd(vcd_day)
    else:
        vcd_daymean = np.nan
        vcd_daystd = np.nan
    if len(vcd_day_cf) > 0:
        vcd_daymean_cf = np.nanmean(vcd_day_cf)
        vcd_daystd_cf = np.nanstd(vcd_day_cf)
    else:
        vcd_daymean_cf = np.nan
        vcd_daystd_cf = np.nan
    return(vcd_daymean, vcd_daystd, vcd_daymean_cf, vcd_daystd_cf,
           single_date.strftime("%Y-%m-%d") )


# lat_bound = [30.4, 30.9]
# lon_bound = [76.4, 77.0]
lat_bound = [30.57, 30.77]
lon_bound = [76.63, 76.83]
basedir = r"M:\DATASETS\OMI_HCHO\QA4ECV\l2"



def func2017():
    year = 2017
    start_date = date(year, 1, 1)
    end_date = date(year, 1, 31)
    numdays = (end_date-start_date).days+1
    vcd_2017 = np.zeros(numdays, dtype=float)
    vcd_2017_std = np.zeros(numdays, dtype=float)
    vcd_2017_cf = np.zeros(numdays, dtype=float)
    vcd_2017_cf_std = np.zeros(numdays, dtype=float)
    date_time_2017 = np.array(['1900-01-01' for x in range(numdays)])
    daterange = pd.date_range(start_date, end_date)
    i = 0
    for single_date in daterange:
        vcd_daymean, vcd_daystd, vcd_daymean_cf, vcd_daystd_cf, tempdt = extract_day(single_date)
        vcd_2017[i] = vcd_daymean
        vcd_2017_std[i] = vcd_daystd
        vcd_2017_cf[i] = vcd_daymean_cf
        vcd_2017_cf_std[i] = vcd_daystd_cf
        date_time_2017[i] = tempdt
        print("day " + tempdt)
        i += 1
    omi_2017 = np.vstack((date_time_2017, vcd_2017, vcd_2017_std, vcd_2017_cf, vcd_2017_cf_std)).T
    np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\test_2017_HCHO_OMI_TS_20.csv", omi_2017,
               delimiter=",", fmt="%s, %s, %s, %s, %s")
        
def func2016():
    year = 2016
    start_date = date(year, 1, 1)
    end_date = date(year, 1, 31)
    numdays = (end_date-start_date).days+1
    vcd_2016 = np.zeros(numdays, dtype=float)
    vcd_2016_std = np.zeros(numdays, dtype=float)
    vcd_2016_cf = np.zeros(numdays, dtype=float)
    vcd_2016_cf_std = np.zeros(numdays, dtype=float)
    date_time_2016 = np.array(['1900-01-01' for x in range(numdays)])
    daterange = pd.date_range(start_date, end_date)
    i = 0
    for single_date in daterange:
        vcd_daymean, vcd_daystd, vcd_daymean_cf, vcd_daystd_cf, tempdt = extract_day(single_date)
        vcd_2016[i] = vcd_daymean
        vcd_2016_std[i] = vcd_daystd
        vcd_2016_cf[i] = vcd_daymean_cf
        vcd_2016_cf_std[i] = vcd_daystd_cf
        date_time_2016[i] = tempdt
        print("day " + tempdt)
        i += 1
    omi_2016 = np.vstack((date_time_2016, vcd_2016, vcd_2016_std, vcd_2016_cf, vcd_2016_cf_std)).T
    np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\test_2016_HCHO_OMI_TS_20.csv", omi_2016,
                          delimiter=",", fmt="%s, %s, %s, %s, %s")
if __name__ == '__main__':
  p1 = Process(target=func2016)
  p1.start()
  p2 = Process(target=func2017)
  p2.start()
  p1.join()
  p2.join()

##Set processor###
#process = mp.Pool(processes=4)
#a=[]
#result  = process.map(extract_day, pd.date_range(start_date, end_date)) ####Range of data ##can be len(data1)
#a.append(result)
#process.close()
#process.join()


#daterange = pd.date_range(start_date, end_date)
#i = 0
#for single_date in daterange:
#    vcd_daymean, vcd_daystd, vcd_daymean_cf, vcd_daystd_cf, tempdt = extract_day(single_date)
#    vcd_ts[i] = vcd_daymean
#    vcd_ts_std[i] = vcd_daystd
#    vcd_ts_cf[i] = vcd_daymean_cf
#    vcd_ts_cf_std[i] = vcd_daystd_cf
#    date_time[i] = tempdt
#    print("day " + tempdt)
#    i += 1
    
    
    
#    vcd_ts.append(vcd_daymean)
#    vcd_ts_std.append(vcd_daystd)
#    vcd_ts_cf.append(vcd_daymean_cf)
#    vcd_ts_cf_std.append(vcd_daystd_cf)
#    date_time.append(single_date.strftime("%Y-%m-%d"))
#    print("day " + single_date.strftime("%Y-%m-%d"))
#omi_ts = np.vstack((date_time, vcd_ts, vcd_ts_std, vcd_ts_cf, vcd_ts_cf_std)).T
#np.savetxt(r"D:\postdoc\Sat\OMI\TS_mohali\2017_HCHO_OMI_TS_20.csv", omi_ts,
#           delimiter=",", fmt="%s, %s, %s, %s, %s")