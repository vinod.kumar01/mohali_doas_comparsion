# -*- coding: utf-8 -*-
"""
Created on Tue Dec 24 17:00:03 2019

@author: Vinod
"""

import cartopy as ccrs
import matplotlib.pyplot as plt
import cartopy.io.img_tiles as cimgt
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import numpy as np
import netCDF4
import os
from matplotlib.patches import Rectangle
from mod_colormap import man_cm, add_white

cm = man_cm("cmap_matlab")
cm = add_white("jet", shrink_colormap=False, replace_frac=0.2, start="min")
fig, ax = plt.subplots(subplot_kw=dict(projection=ccrs.crs.PlateCarree()))
stamen_terrain = cimgt.Stamen('terrain-background')
#ax.set_extent([73, 79,27, 33])
#ax.set_extent([76.479, 76.979, 30.417, 30.917])
ax.set_extent([75.7, 77.7, 29.7, 31.7])
ax.add_image(stamen_terrain, 8)
gl = ax.gridlines(crs=ccrs.crs.PlateCarree(), draw_labels=True,
              linewidth=1, color='gray', alpha=0.5, linestyle='--')
gl.xformatter = LONGITUDE_FORMATTER
gl.yformatter = LATITUDE_FORMATTER
gl.xlabel_style = {'size': 14}
gl.ylabel_style = {'size': 14}
#ax.add_patch(Rectangle((76.479,30.417), 0.5, 0.5, fill=False, color = 'k', alpha=0.5))
gl.xlabels_bottom = False
gl.ylabels_right = False
loc = [{'lon': 76.729, 'lat': 30.667, 'site': 'Mohali'},
       {'lon': 75.86, 'lat': 30.90, 'site': 'Ludhiana'},
       {'lon': 76.78, 'lat': 30.37, 'site': 'Ambala'},
       {'lon': 77.21, 'lat': 28.61, 'site': 'New Delhi'},
       {'lon': 77.17, 'lat': 31.10, 'site': 'Shimla'},
       {'lon': 76.96, 'lat': 29.39, 'site': 'Panipat'},
       {'lon': 74.36, 'lat': 31.52, 'site': 'Lahore'},
       {'lon': 76.5845, 'lat': 31.0418, 'site': 'PP1'},
       {'lon': 76.577, 'lat': 30.557, 'site': 'PP2'}]

for point in loc:
    lo, la = point['lon'], point['lat']
    if point['site']=='Mohali':
        ax.plot(lo, la, 'r^')
    elif 'PP' in point['site']:
        ax.plot(lo, la, 'bo')
    else:
        ax.plot(lo, la, 'kx')
    ax.annotate(point['site'], xy=(lo, la),  xycoords='data', size=14,
                 xytext=(lo, la), textcoords='data', color='white', weight='bold',
                 horizontalalignment='left', verticalalignment='bottom',
                 arrowprops=dict(facecolor='white', shrink=0.05))
plt.tight_layout()

# %% ad tropomi map
#
dirname = r'M:\home\beirle\ProjectsA\2018_Divergence\data\Mean_Fluxes\India\hires'
filename = 'means_layer12_allmonths.hdf'
f = netCDF4.Dataset(os.path.join(dirname, filename))

lons = f.variables['lon'][:]
lats = f.variables['lat'][:]
lats = np.flip(lats,0)
VCD = f.variables['VCD'][:]
llons,llats = np.meshgrid(lons, lats)
cs = ax.pcolormesh(llons, llats, VCD/1E15, cmap=cm,
                    vmin = 0.1, vmax = 4,
                    transform=ccrs.crs.PlateCarree())
##fig.subplots_adjust(right=0.88, wspace = 0.02)
fig.subplots_adjust(bottom=0.12, wspace = 0.02)
##cbar_ax=fig.add_axes([0.9, 0.15, 0.02, 0.7])
cbar_ax=fig.add_axes([0.2, 0.07, 0.6, 0.02])
cbar=fig.colorbar(cs, cmap=plt.cm.get_cmap('jet'), cax = cbar_ax, extend='max',
                  orientation="horizontal")
cbar.set_label('NO$_{2}$ VCD (10$^{15}$ molecules cm$^{-2}$)', fontsize=12)