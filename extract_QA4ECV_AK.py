# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 23:47:21 2019

@author: Vinod
"""

# %% imports
import os
import glob
import netCDF4
from datetime import datetime
import numpy as np
from datetime import date
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# %% Function to get profiles
def get_AK(folder_name):
    f_name_sat = glob.glob(os.path.join(dirname,
                                        single_date.strftime("%Y\%m\%d"),
                                        'QA4ECV_L2_NO2_' + '*.nc'))
    cf_ap = []
    ch_ap = []
    AK_ap = []
    AMF_trop_ap = []
    AMF_tot_ap = []
    for filename in f_name_sat:
        AK_orb = []
        orbit_num = int(filename.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(filename, 'r')
        tempdate = filename.split('\\')[-1].split('_')[4]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[4:6])
        temp_day = int(tempdate[6:8])
        temp_hour = int(tempdate[9:11])
        temp_min = int(tempdate[11:13])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
#        daynum = min(temp_dt.timetuple().tm_yday, 365)
        swath = f.groups['PRODUCT']
        lats = swath.variables['latitude'][0, :]
        lons = swath.variables['longitude'][0, :]
        AK = swath.variables['averaging_kernel']
        add_data =swath.groups['SUPPORT_DATA'].groups['DETAILED_RESULTS']
        inp_data = swath.groups['SUPPORT_DATA'].groups['INPUT_DATA']
        AMF_trop = swath.variables['amf_trop']
        AMF_tot = swath.variables['amf_total']       
        cf = add_data.variables['cloud_radiance_fraction_no2']
#        cf = inp_data.variables['cloud_fraction']
        cp = inp_data.variables['cloud_pressure']
        lat_bound = [30.4, 30.9]
        lon_bound = [76.4, 77.0]

        keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
        keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
        if len(lats[keep])>0:
            print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                    + ' : ' + str(orbit_num))
            cf = cf[0, :][keep]
            AMF_trop = AMF_trop[0, :][keep]
            AMF_tot = AMF_tot[0, :][keep]
            cp = cp[0, :][keep]
            cp[cp<0] = np.nan
            ch = 8.5*np.log(970/cp)

            for lev in range(34):
                AK_orb.append(AK[0, :, :, lev][keep])

            AK_orb = np.array(AK_orb)
            try:
                AK_ap = np.concatenate((AK_ap,AK_orb), axis=1)
                cf_ap = np.concatenate((cf_ap,cf), axis=1)
                ch_ap = np.concatenate((ch_ap,ch), axis=1)
                AMF_trop_ap = np.concatenate((AMF_trop_ap, AMF_trop), axis=1)
                AMF_tot_ap = np.concatenate((AMF_tot_ap,AMF_tot), axis=1)
            except:
                AK_ap = AK_orb
                cf_ap = cf
                ch_ap = ch
                AMF_trop_ap = AMF_trop
                AMF_tot_ap = AMF_tot                
    #        mean_profile = np.ma.mean(profile, 1)
        try:
            f.close()
        except NameError:
            print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
            AK_ap = np.full(34, np.nan)
            cf_ap = np.nan
            ch_ap = np.nan
            AMF_trop_ap = np.nan
            AMF_tot_ap = np.nan
    if len(AK_ap)==0:
        AK_ap = np.full(34, np.nan)
        cf_ap = np.nan
        ch_ap = np.nan
        AMF_trop_ap = np.nan
        AMF_tot_ap = np.nan
        temp_year, temp_month, temp_day = [int(i) for i in folder_name.split('\\')[-3:]]
        temp_dt = datetime(temp_year, temp_month, temp_day)
    return(temp_dt, AK_ap, cf_ap, ch_ap, AMF_trop_ap, AMF_tot_ap)

# %% loop through days and get an array of profiles
dirname = r'M:\nobackup\vinod\QA4ECV\NO2'
start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)
#start_date = date(2016, 1, 1)
#end_date = date(2017, 1, 3)
days = (end_date-start_date).days+1
AK_ts = []
date_time = []
cf_ts = []
ch_ts = []
AMF_TROP_ts = []
AMF_TOT_ts = []

daterange = pd.date_range(start_date, end_date)
for single_date in daterange:
    print('processing '+ single_date.strftime("%Y\%m\%d"))
    folder_name = os.path.join(dirname, single_date.strftime("%Y\%m\%d"))
    single_day, AK_day, cf_day, ch_day, AMF_trop_day, AMF_tot_day = get_AK(folder_name)
    i=len(AK_day.shape)
    while i>1:
#        AK_day = np.ma.mean(AK_day,1)
#        cf_day = np.ma.mean(cf_day,0)
#        ch_day = np.ma.mean(ch_day,0)
#        AMF_trop_day = np.ma.mean(AMF_trop_day,0)
#        AMF_tot_day = np.ma.mean(AMF_tot_day,0)
        AK_day = np.nanmean(AK_day,1)
        cf_day = np.nanmean(cf_day,0)
        ch_day = np.nanmean(ch_day,0)
        AMF_trop_day = np.nanmean(AMF_trop_day,0)
        AMF_tot_day = np.nanmean(AMF_tot_day,0)
        i -= 1

    AK_ts.append(AK_day)
    cf_ts.append(cf_day)
    ch_ts.append(ch_day)
    AMF_TROP_ts.append(AMF_trop_day)
    AMF_TOT_ts.append(AMF_tot_day)
    date_time.append(single_day.date())

# Reality checks
AK_ts = np.array(AK_ts)
AK_ts[AK_ts>100] = np.nan
AK_ts[AK_ts<0] = np.nan
cf_ts = np.array(cf_ts)
cf_ts[cf_ts>100] = np.nan
cf_ts[cf_ts<0] = np.nan
ch_ts = np.array(ch_ts)
ch_ts[ch_ts>100] = np.nan
ch_ts[ch_ts<0] = np.nan
AMF_TROP_ts = np.array(AMF_TROP_ts)
AMF_TROP_ts[AMF_TROP_ts>100] = np.nan
AMF_TROP_ts[AMF_TROP_ts<0] = np.nan
AMF_TOT_ts = np.array(AMF_TOT_ts)
AMF_TOT_ts[AMF_TOT_ts>100] = np.nan
AMF_TOT_ts[AMF_TOT_ts<0] = np.nan

#profile_ts = np.ma.masked_array(profile_ts)

# %% save the daily profiles and temperatures as netCDF 
# Domino vertical grid definitios from a separate file
vlevs = netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\omi_qa4ecv_hy.nc',
                        'r') 
filename_out = 'QA4ECV_AK_amf_mohali_2013_17_new2.nc'
out_dir = r'D:\postdoc\Sat\OMI\TS_mohali'
f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'w', format='NETCDF4')
f_out.createDimension('time', None)
f_out.createDimension('lev', 34)
f_out.createDimension('ilev', 35)
lev_out = f_out.createVariable('lev', 'int8', ('lev',))
ilev_out = f_out.createVariable('ilev', 'int8', ('ilev',))
hyam_out = f_out.createVariable('hyam', 'f4', ('lev',))
hybm_out = f_out.createVariable('hybm', 'f4', ('lev',))
hyai_out = f_out.createVariable('hyai', 'f4', ('ilev',))
hybi_out = f_out.createVariable('hybi', 'f4', ('ilev',))
time_out = f_out.createVariable('time', 'int16', ('time',))
AK_out = f_out.createVariable('AveragingKernel', 'f4', ('time', 'lev'))
cf_out = f_out.createVariable('CloudFraction',   'f4', ('time'))
ch_out = f_out.createVariable('CloudHeight', 'f4', ('time'))
AMF_TROP_out = f_out.createVariable('AMF_TROP', 'f4', ('time'))
AMF_TOT_out = f_out.createVariable('AMF_TOT', 'f4', ('time'))
lev_out[:] = np.arange(1, 35)
ilev_out[:] = np.arange(1, 36)
hyam_out[:], hyai_out[:] = vlevs.variables['hyam'][:], vlevs.variables['hyai'][:]
hyam_out.units = vlevs.variables['hyam'].units
hyai_out.units = vlevs.variables['hyai'].units
hyam_out.long_name = vlevs.variables['hyam'].long_name
hyai_out.long_name = vlevs.variables['hyai'].long_name
hybm_out[:], hybi_out[:] = vlevs.variables['hybm'][:], vlevs.variables['hybi'][:]
hybm_out.units = vlevs.variables['hybm'].units
hybi_out.units = vlevs.variables['hybi'].units
hybm_out.long_name = vlevs.variables['hybm'].long_name
hybi_out.long_name = vlevs.variables['hybi'].long_name
AK_out[:] = AK_ts
AK_out.long_name='Averaging Kernel'
AK_out.units='1'
cf_out[:] = cf_ts
cf_out.long_name='Cloud Fraction'
cf_out.units='1'
ch_out[:] = ch_ts
ch_out.long_name='Cloud Height'
ch_out.units='Km'

AMF_TOT_out[:] = AMF_TOT_ts
AMF_TOT_out.long_name='Tot Airmass Factor'
AMF_TOT_out.units='1'
AMF_TROP_out[:] = AMF_TROP_ts
AMF_TROP_out.long_name='Trop Airmass Factor'
AMF_TROP_out.units='1'
time_out[:] = np.arange(0, days, 1); time_out.long_name='time'
time_out.units='days since 2013-01-01 00:00:00'
f_out.close()