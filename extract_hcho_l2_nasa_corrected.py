# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 14:14:42 2019

@author: Vinod
"""

import netCDF4
import numpy as np
import os
import glob
import pandas as pd
import sys
from datetime import date, datetime
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
# %% define limits and path
lat_bound = [30.4, 30.9]
lon_bound = [76.4, 77.0]

#lat_bound = [30.57, 30.77]
#lon_bound = [76.63, 76.83]

    
dirname = r"M:\nobackup\vinod\OMI_HCHO\NASA"
vcd_ts = []
vcd_ts_std = []
vcd_ts_cf = []
vcd_ts_cf_std = []
date_time = []

start_date = date(2013, 1, 1)
end_date = date(2017, 8, 1)

daterange = pd.date_range(start_date, end_date)
month = 1
# %% loading and processing data
for single_date in daterange:
    print("Processing day " + single_date.strftime("%Y-%m-%d"))
    f_name_sat = glob.glob(os.path.join(dirname, 'OMI-Aura_L2-OMHCHO_'+
                                        single_date.strftime("%Ym%m%d") + '*.he5'))
    vcd_day = []
    vcd_month = []
    vcd_day_corr = []
    vcd_day_cf = []
    vcd_day_corr_cf = []
    for path_sat in f_name_sat:
#        AK_orb = []
        orbit_num = int(path_sat.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(path_sat, 'r')
        print("Processing day " + single_date.strftime("%Y-%m-%d") + ' orbit ' + str(orbit_num))
        tempdate = path_sat.split('\\')[-1].split('_')[2]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[5:7])
        temp_day = int(tempdate[7:9])
        temp_hour = int(tempdate[10:12])
        temp_min = int(tempdate[12:14])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
#        daynum = min(temp_dt.timetuple().tm_yday, 365)
        try:
            swath = f.groups['HDFEOS'].groups['SWATHS'].groups['OMI Total Column Amount HCHO']
            data_fields = swath.groups['Data Fields']
            geo_fields = swath.groups['Geolocation Fields']
            lats = geo_fields.variables['Latitude'][:]
            lons = geo_fields.variables['Longitude'][:]
            hcho_tropcol = data_fields.variables['ColumnAmount']
    #        AMF_trop = swath.variables['amf_trop'][0, :]
    #        AMF_tot = swath.variables['amf_total'][0, :]
    #        AK = swath.variables['averaging_kernel'][0, :]
            cf = data_fields.variables['AMFCloudFraction'][:]
            flag = data_fields.variables['MainDataQualityFlag']
   #        row_anam_flag = geo_fields.variables['XtrackQualityFlags'][:]
            fv = hcho_tropcol._FillValue
            keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
            keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
            keep &= (flag[:]==0)
            hcho_tropcol = hcho_tropcol[:][keep]
            hcho_tropcol[hcho_tropcol>15E16] = np.nan
            hcho_tropcol[hcho_tropcol<-5E16] = np.nan
            cf = cf[keep]
            hcho_tropcol_cf = hcho_tropcol[(cf < 0.3)]
            # further processing of AMF correction on the relevant orbits
            if len(hcho_tropcol > 0):
                print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                    + ' : ' + str(orbit_num))
                vcd_day.extend(hcho_tropcol)
            if len(hcho_tropcol_cf > 0):
                vcd_day_cf.extend(hcho_tropcol_cf)
            try:
                f.close()
            except NameError:
                print('no omi L2 data for' + single_date.strftime("%Y-%m-%d"))
        except KeyError:
            print('file too short for' + single_date.strftime("%Y-%m-%d") +
                  ' orbit ' + str(orbit_num))
    if len(vcd_day) > 0:
        vcd_daymean = np.nanmean(vcd_day)
        vcd_daystd = np.nanstd(vcd_day)
    else:
        print('no VCD for ' + single_date.strftime("%Y-%m-%d"))
        vcd_daymean = np.nan
        vcd_daystd = np.nan
    if len(vcd_day_cf) > 0:
        vcd_daymean_cf = np.nanmean(vcd_day_cf)
        vcd_daystd_cf = np.nanstd(vcd_day_cf)
    else:
        vcd_daymean_cf = np.nan
        vcd_daystd_cf = np.nan
    vcd_ts.append(vcd_daymean)
#    vcd_corr_ts.append(vcd_corr_daymean)
    vcd_ts_std.append(vcd_daystd)
    vcd_ts_cf.append(vcd_daymean_cf)
    vcd_ts_cf_std.append(vcd_daystd_cf)
    date_time.append(single_date.strftime("%Y-%m-%d"))
omi_ts = np.vstack((date_time, vcd_ts, vcd_ts_std, vcd_ts_cf, vcd_ts_cf_std)).T
np.savetxt(r"D:\postdoc\Sat\OMI\ts_hcho_nasa_l2.csv", omi_ts,
           delimiter=",", header = 'date_time, vcd, vcd_std, vcd_cf, vcd_cf_std',
           fmt="%s, %s, %s, %s, %s")
sys.stdout = open(r'D:\postdoc\Sat\OMI\log2.txt', 'w')