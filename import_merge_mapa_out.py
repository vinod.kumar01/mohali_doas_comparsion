# -*- coding: utf-8 -*-
'''
Created on 29-Jul-2019

@author: Vinod
'''
import netCDF4
import os
import numpy as np
from datetime import datetime


class load_plot():
    def __init__(self, tracer, mapa_spec, spec_str, y_min, y_max,
                 inst, prod, vcd_var, wf=1, sf='0.8'):
        '''
        load_plot initialized by
        @tracer species in question no2, hcho or aod
        @mapa_spec relevant species from which it is derived e.g. O4 for aod
            or no2 for no2 or hcho for hcho
        @wf scale factor while importing satellite data for comparison
        @spec_str y axis label in case of plot
        @ymin y axis lower limit
        @ymax y axis max limit
        @inst instrument e.g. OMI, MODIS, PTR-MS, Chemiluminescence
        @product Data product e.g. QA4ECV, DOMINO, NASA
        @vcd_var mapa variable to be plotted
        @sf O4 scaling factor
        '''
        self.tracer = tracer
        self.mapa_spec = mapa_spec
        self.wf = wf
        self.spec_str = spec_str
        self.y_min = y_min
        self.y_max = y_max
        self.inst = inst
        self.prod = prod
        self.vcd_var = vcd_var
        self.data = None
        self.wf = wf
        self.sf = sf
#        self.load_data()
#        self.plot_data()

    def load_data(self, mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v1',
                  mapa_outpath=r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v1'):
        hdf_filename = self.mapa_spec + '_o4sf'+self.sf+'.hdf'
        vcd_mean, z, flag, profile_mean, time2 = [], [], [], [], []
#        sza = []
        o4sf = []
        s = []
        h = []
        for nc_dir in ['analysis_2012_1_auto', 'analysis_2012_2_auto',
                       'analysis_2013_1_auto', 'analysis_2013_2_auto',
                       'analysis_2013_3_auto', 'analysis_2013_4_auto',
                       'analysis_2014_1_auto', 'analysis_2014_2_auto',
                       'analysis_2015_1_auto', 'analysis_2015_2_auto',
                       'analysis_2015_3_auto', 'analysis_2015_4_auto',
                       'analysis_2015_5_auto', 'analysis_2015_6_auto',
                       'analysis_2016_1_auto', 'analysis_2016_2_auto',
                       'analysis_2017_1_auto']:
            full_path = os.path.join(mapa_outpath, nc_dir, self.mapa_spec,
                                     hdf_filename)
            f = netCDF4.Dataset(full_path, 'r')
            vcd_mean.extend(f.variables[self.vcd_var][:])
            z = f.variables['z_center'][:]
            try:
                o4sf.extend(f.variables['scaling_factor_weighted_mean'][:])
            except KeyError:
                o4sf.extend(f.variables['scaling_factor_aerosol'][:])
#            sza.extend(f.variables['sza'][:])
            h.extend(f.variables['h_weighted_mean'][:])
            s.extend(f.variables['s_weighted_mean'][:])
            flag.extend(f.variables['flag_total'][:])
            profile_mean.extend(f.variables['profile_weighted_mean'][:])
            f.close()

            '''
            getting the time right from the input file
            '''
            mapa_infile = os.path.join(mapa_inpath, nc_dir+".nc")
            datefile = netCDF4.Dataset(mapa_infile, 'r')
            dat = datefile.variables['time'][:]
            timegrid = np.max(dat, axis=1)
            time2.extend(np.array(
                [np.datetime64(datetime.strptime(str(int(t)), "%Y%m%d%H%M%S"))
                 for t in timegrid]))
            datefile.close()
            print(nc_dir)
        return vcd_mean, z, flag, profile_mean, o4sf, h, s, time2
    def get_geo_vcd(self, mapa_inpath=r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v3',
                    geo_angle=15):
        amf = 1/np.sin(geo_angle*np.pi/180)-1
        geo_vcd, sza, time2 = [], [], []
        for nc_dir in ['analysis_2012_1_auto', 'analysis_2012_2_auto',
                       'analysis_2013_1_auto', 'analysis_2013_2_auto',
                       'analysis_2013_3_auto', 'analysis_2013_4_auto',
                       'analysis_2014_1_auto', 'analysis_2014_2_auto',
                       'analysis_2015_1_auto', 'analysis_2015_2_auto',
                       'analysis_2015_3_auto', 'analysis_2015_4_auto',
                       'analysis_2015_5_auto', 'analysis_2015_6_auto',
                       'analysis_2016_1_auto', 'analysis_2016_2_auto',
                       'analysis_2017_1_auto']:
            mapa_infile = os.path.join(mapa_inpath, nc_dir+".nc")
            datafile = netCDF4.Dataset(mapa_infile, 'r')
            eas = datafile.variables['ea'][:]
            idx_elev = np.argwhere(eas==geo_angle)
            idx_elev = idx_elev[0][0]
            tracer_dscd = datafile.variables[self.mapa_spec+'_dscd'][:]
            tracer_vcd = tracer_dscd[:, idx_elev]/amf
            sza_file = datafile.variables['sza'][:, idx_elev]
            dat = datafile.variables['time'][:]
            timegrid = np.max(dat, axis=1)
            time2.extend(np.array(
                [np.datetime64(datetime.strptime(str(int(t)), "%Y%m%d%H%M%S"))
                 for t in timegrid]))
            geo_vcd.extend(tracer_vcd)   
            sza.extend(sza_file)
            print(nc_dir)
        return geo_vcd, sza, time2
        
#mapa_inpath = r'D:\postdoc\DOAS\MAPA\Input\Vinod\NO2_v1'
#mapa_outpath = r'D:\postdoc\DOAS\MAPA\Results\Vinod\mapa_v098_cnf_098_v1'
# mapa_outpath = r'M:\nobackup\vinod\MAPA\Results\Vinod\mapa_v097_cnf_097\hdf'

