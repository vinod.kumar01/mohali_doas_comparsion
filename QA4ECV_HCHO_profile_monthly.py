# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 20:40:55 2019
Script to get the mean monthly profile from QA4ECV OMI data of HCHO
@author: Vinod
"""

import os
import glob
import netCDF4
from datetime import datetime, date, timedelta
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import calendar
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def get_profile(folder_name):
    f_name_sat = glob.glob(os.path.join(folder_name, 'QA4ECV_L2_HCHO_OMI_*.nc'))
    profile_ap=[]
    for filename in f_name_sat:
        profile_orb = []
        orbit_num = int(filename.split('\\')[-1].split('o')[-1][0:5])
        f = netCDF4.Dataset(filename, 'r')
        tempdate = filename.split('\\')[-1].split('_')[4]
        temp_year = int(tempdate[0:4])
        temp_month = int(tempdate[4:6])
        temp_day = int(tempdate[6:8])
        temp_hour = int(tempdate[9:11])
        temp_min = int(tempdate[11:13])
        temp_dt = datetime(temp_year, temp_month, temp_day, temp_hour, temp_min)
        lat_bound = [30.4, 30.9]
        lon_bound = [76.4, 77.0]
        try:
            swath = f.groups['PRODUCT']
            lats = swath.variables['latitude'][0, :]
            lons = swath.variables['longitude'][0, :]
            Data_fields = swath.groups['SUPPORT_DATA'].groups['INPUT_DATA']
            TM5profile = Data_fields.variables['hcho_profile_apriori']
            keep = (lats > lat_bound[0]) & (lats < lat_bound[1])
            keep &= (lons > lon_bound[0]) & (lons < lon_bound[1])
            if len(lats[keep])>0:
                print('Relevant orbit number for ' + temp_dt.strftime("%Y-%m-%d")
                        + ' : ' + str(orbit_num))
                for lev in range(34):
                    profile_orb.append(TM5profile[0,:, :, lev][keep])
                profile_orb = np.array(profile_orb)
                try:
                    profile_ap = np.concatenate((profile_ap,profile_orb), axis=1)
                except:
                    profile_ap = profile_orb
    #        mean_profile = np.ma.mean(profile, 1)
            f.close()
        except(KeyError):
            print('no profile for relevant area for '+ temp_dt.strftime("%Y-%m-%d"))
            profile_ap = np.full(34, np.nan)
    if len(profile_ap)==0:
        profile_ap = np.full(34, np.nan)
    return(temp_dt, profile_ap)

dirname = r'M:\DATASETS\OMI_HCHO\QA4ECV\l2'
start_date = date(2015, 1, 1)
end_date = date(2015, 12, 31)
days = (end_date-start_date).days+1
profile_ts = []
date_time = []
temp_ts = []
daterange = pd.date_range(start_date, end_date)
for single_date in daterange:
    print('processing '+ single_date.strftime("%Y\%m\%d"))
    folder_name = os.path.join(dirname, single_date.strftime("%Y\%m\%d"))
    single_day, profile_day = get_profile(folder_name)
    i=len(profile_day.shape)
    while i>1:
        profile_day = np.ma.mean(profile_day,1)
        i -= 1

    profile_ts.append(profile_day)
    date_time.append(single_day.date())

#profile_ts = np.ma.masked_array(profile_ts)

vlevs = netCDF4.Dataset(r'M:\nobackup\vinod\model_work\MECO\omi_qa4ecv_hy.nc', 'r')    
filename_out = 'QA4ECV_hcho_profile_mohali_2015.nc'
out_dir = r'D:\postdoc\Sat\OMI\TS_mohali'
f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'w', format='NETCDF4')
f_out.createDimension('time', None)
f_out.createDimension('lev', 34)
f_out.createDimension('ilev', 35)
lev_out = f_out.createVariable('lev', 'int32', ('lev',))
ilev_out = f_out.createVariable('ilev', 'int32', ('ilev',))
hyam_out = f_out.createVariable('hyam', 'f4', ('lev',))
hybm_out = f_out.createVariable('hybm', 'f4', ('lev',))
hyai_out = f_out.createVariable('hyai', 'f4', ('ilev',))
hybi_out = f_out.createVariable('hybi', 'f4', ('ilev',))
time_out = f_out.createVariable('time', 'f4', ('time',))
HCHO_out = f_out.createVariable('HCHO',   'f4', ('time', 'lev'))

lev_out[:] = np.arange(1, 35)
ilev_out[:] = np.arange(1, 36)
hyam_out[:], hyai_out[:] = vlevs.variables['hyam'][:], vlevs.variables['hyai'][:]
hyam_out.units = vlevs.variables['hyam'].units
hyai_out.units = vlevs.variables['hyai'].units
hyam_out.long_name = vlevs.variables['hyam'].long_name
hyai_out.long_name = vlevs.variables['hyai'].long_name
hybm_out[:], hybi_out[:] = vlevs.variables['hybm'][:], vlevs.variables['hybi'][:]
hybm_out.units = vlevs.variables['hybm'].units
hybi_out.units = vlevs.variables['hybi'].units
hybm_out.long_name = vlevs.variables['hybm'].long_name
hybi_out.long_name = vlevs.variables['hybi'].long_name
HCHO_out[:] = np.array(profile_ts)
HCHO_out.long_name='HCHO a priori mixing ratio'
HCHO_out.units='mol mol-1'
time_out[:] = np.arange(0, days, 1); time_out.long_name='time';time_out.units='days since 2015-01-01 00:00:00'
f_out.close()


#%% QA4ECV doesnot have tempearure profile. Copy it from DOMINO profile
#from scipy.interpolate import interp1d
#filename_no2 = 'DOMINO_profile_mohali_2015.nc'
#out_dir = r'D:\postdoc\Sat\OMI\TS_mohali'
#f_no2 = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'r')
#prs = np.array([f_out.variables['hyam'][i]+(97000*f_out.variables['hybm'][i]) for i in range(0,34)])
#temp = f_out.variables['Temperature'][:]
#filename_out = 'QA4ECV_hcho_profile_mohali_2015_dom_temp.nc'
#f2 = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'r+')
#prs2 = np.array([f2.variables['hyam'][i]+(97000*f2.variables['hybm'][i]) for i in range(0,34)])
#temp2 = temp.copy()
#for day in range(365):
#    fn_temp = interp1d(prs, temp[day,:])
#    temp2[day, :] = fn_temp(prs2)
#temp2_out = f2.createVariable('Temperature',   'f4', ('time', 'lev'))
#temp2_out[:] = temp2
#temp2_out.long_name='Air Temperature'
#temp2_out.units='K'
#f2.close()
#f_no2.close()

#%% plotting
f_out = netCDF4.Dataset(os.path.join(out_dir, filename_out), 'r')
date_time = f_out.variables['time'][:]
date_time = [date(2015,1,1)+timedelta(days=float(i)) for i in date_time]
month = np.array([i.month for i in date_time])
prs_i = np.array([f_out.variables['hyai'][i]+(97000*f_out.variables['hybi'][i]) for i in range(0,35)])
temp = f_out.variables['Temperature'][:]
prs_i[prs_i<0]=np.nan
h_interface=np.array([7.4*np.log(97000/prs_i[i]) for i in range(0,35)])*1000 
h=np.array([h_interface[i]-h_interface[i-1] for i in range(1,35)])
h=h*temp[100,:]/250
z_centre = (h_interface[1:]+h_interface[:-1])/2 
z_centre=z_centre*temp[100,:]/250
hcho_mr = f_out.variables['HCHO'][:]
hcho_conc = hcho_mr*6.023E23*97000
hcho_conc /= (8.314*temp*1E6)
hcho_bvcd = hcho_conc*h*100
fig,ax = plt.subplots()
ax.set_color_cycle(['darkgrey', 'k', 'lawngreen', 'gold', 'violet', 'r',
                    'b', 'cyan', 'g', 'maroon', 'indigo', 'rosybrown'])
for i in range(1,13):
    idx = month==i
    abs_profile = np.nanmean(hcho_conc[idx, :16], axis=0)
    rel_profile = abs_profile/np.sum(abs_profile)
    ax.plot(abs_profile, z_centre[:16], '-+',
        label=calendar.month_abbr[i])
ax.set_ylabel('Altitude (meters)')
ax.set_xlabel('HCHO concentration (molecules cm$^{-3}$)')
#ax.set_xlabel('HCHO relative concentration')
ax.legend(loc='upper right', ncol=2)
ax.minorticks_on()
ax.grid(alpha=0.4)
ax.set_ylim(0, 4000)
ax.set_xlim(-0.01, 1)
#plt.savefig(os.path.join(out_dir, 'monthly_hcho_qa4ecv_profile_abs.png'), format='png')

